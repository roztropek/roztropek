<?php

namespace Roztropek\Image\Factory;

use Roztropek\Image\Text as TextImage;
use Roztropek\Utils\Helper\ArrayHelper;

final class Text {

    public static function create(array $data) {
        $width = ArrayHelper::getKey('width', $data, 10, 'natural');
        $height = ArrayHelper::getKey('height', $data, 10, 'natural');
        $text = ArrayHelper::getKey('text', $data, '', 'string');
        $transparent = ArrayHelper::getKey('transparent', $data, true, 'boolean');
        $padding = ArrayHelper::getKey('padding', $data, 10, 'natural');
        $fontSize = ArrayHelper::getKey('fontSize', $data, 10, 'natural');
        
        $textImage = new TextImage($width, $height, $text);
        $textImage->setTransparent($transparent);
        $textImage->setFontSize($fontSize);
        $textImage->setPadding($padding);
        
        return $textImage;
    }

}
