<?php

namespace Roztropek\Image;

class Text {

//    private static $defaultFont = '/font/HelveticaNeue.ttf';
    //    private static $defaultFont = '/font/YesevaOne-Regular.ttf';
//    private static $defaultFont = '/font/RobotoCondensed-Regular.ttf';
    private static $defaultFont = '/font/Roboto-Regular.ttf';
    private static $watermarkImg = '/watermark/watermark.png';
    public $width;
    public $height;
    public $font;
    public $text;
    public $fontsize;
    public $padding;
    public $rowSpacing;
    public $fontColor;
    public $transparent;
    private $image;
    private $backgroundColor;
    private $background;
    private $fontC;

    public function __construct($width = 10, $height = 10, $text = '') {
        $this->font = __DIR__ . self::$defaultFont;
        $this->width = (int) $width;
        $this->height = (int) $height;
        $this->setText($text);
        $this->backgroundColor = array(255, 255, 255);
        $this->fontColor = array(0, 0, 0);
        $this->padding = 10;
        $this->rowSpacing = 10;
        $this->transparent = false;
    }

    public function setRowSpacing($rowSpacing) {
        $this->rowSpacing = (int) $rowSpacing;
    }

    public function setPadding($padding) {
        $this->padding = (int) $padding;
    }

    public function setFontSize($fontSize) {
        $this->fontsize = (int) $fontSize;
    }

    public function setWidth($width) {
        $this->width = (int) $width;
    }

    public function setHeight($height) {
        $this->height = (int) $height;
    }

    public function setFont($font) {
        $this->font = $font;
    }

    public function setText($text) {
        if (is_array($text)) {
            $this->text = $text;
        } else {
            $this->text = array($text);
        }
    }

    /**
     * @param boolean $transparent
     */
    public function setTransparent($transparent) {
        $this->transparent = $transparent;
    }

    /**
     * 
     * @param mixed $color
     */
    public function setBackgroundColor($color) {
        if (is_array($color)) {
            $this->backgroundColor = $color;
        } else {
            if (strpos($color, ',') !== false) {
                $color = explode(',', $color);
                $this->backgroundColor = $color;
            } else if (strpos($color, '#') !== false) {
                $color = str_replace('#', '', $color);
                $color = str_split($color, 2);
                foreach ($color as $key => $hex) {
                    $color[$key] = hexdec($hex);
                }
                $this->backgroundColor = $color;
            }
        }
    }

    public function setFontColor($color) {
        if (is_array($color)) {
            $this->fontColor = $color;
        } else {
            if (strpos($color, ',') !== false) {
                $color = explode(',', $color);
                $this->fontColor = $color;
            } else if (strpos($color, '#') !== false) {
                $color = str_replace('#', '', $color);
                $color = str_split($color, 2);
                foreach ($color as $key => $hex) {
                    $color[$key] = hexdec($hex);
                }
                $this->fontColor = $color;
            }
        }
    }

    public function createImage() {

        $multipler = count($this->text);

        $this->image = imagecreatetruecolor($this->width, $this->height * $multipler);
        
        if ($this->transparent == true) {
            $this->background = imagecolorallocatealpha($this->image, $this->backgroundColor[0], $this->backgroundColor[1], $this->backgroundColor[2], 127);
            imagesavealpha($this->image, true);
        } else {
            $this->background = imagecolorallocate($this->image, $this->backgroundColor[0], $this->backgroundColor[1], $this->backgroundColor[2]);
        }
        $this->fontC = imagecolorallocate($this->image, $this->fontColor[0], $this->fontColor[1], $this->fontColor[2]);
        imagefill($this->image, 0, 0, $this->background);
        
        $this->putTextIntoImage();
        $this->addWatermarkIntoImage();
    }

    public function putImage($output = false) {
        $this->createImage();
        imagealphablending($this->image, true);
        if ($output === true) {
            ImagePng($this->image);
            return;
        }
        ob_start();
        ImagePng($this->image);
        $imagedata = ob_get_contents();
        ob_end_clean();
        return base64_encode($imagedata);
    }

    private function putTextIntoImage() {
        $positionMultipler = 0;
        foreach ($this->text as $text) {
            $words = explode(' ', $text);
            $rows = array();
            $dlugosc = null;
            $tmpText = null;
            $textRow = null;
            $wysokoscSum = 0;
            foreach ($words as $word) {
                $tmpText .= $word . ' ';

                $bbox = imagettfbbox($this->fontsize, 0, $this->font, $tmpText);

                $dlugoscTmp = $bbox[0] + $bbox[2];
                $wysokosc = abs($bbox[1] + $bbox[7]);

                if ($dlugoscTmp > $this->width - $this->padding) {
                    $rows[] = array('text' => trim($textRow),
                        'width' => $dlugosc,
                        'height' => $wysokosc);
                    $wysokoscSum+=$wysokosc;
                    $tmpText = $word . ' ';
                    $textRow = '';
                }
                $textRow .= $word . ' ';
                $bbox = imagettfbbox($this->fontsize, 0, $this->font, $tmpText);
                $dlugosc = $bbox[0] + $bbox[2];
            }

            if (strlen($tmpText) > 0) {
                $rows[] = array('text' => trim($tmpText),
                    'width' => $dlugosc,
                    'height' => $wysokosc);
                $wysokoscSum+=$wysokosc;
            }

            $y = ($this->height - ($wysokoscSum + count($rows) * $this->rowSpacing) / 2) / 2 + $positionMultipler * $this->height + (($wysokosc + $this->rowSpacing) / 2);

            foreach ($rows as $row) {
                $x = ($this->width - $row['width']) / 2;
                imagettftext($this->image, $this->fontsize, 0, $x, $y, $this->fontC, $this->font, $row['text']);
                $y = $y + $row['height'] + $this->rowSpacing;
            }
            $positionMultipler++;
        }
    }

    protected function addWatermarkIntoImage() {
        $watermark = imagecreatefrompng(__DIR__ . self::$watermarkImg);

        $width = imagesx($watermark);
        $height = imagesy($watermark);

        $maxMarginLeft = imagesx($this->image) - $width;
        $maxMarginTop = imagesy($this->image) - $height;

        if (($maxMarginLeft - 2) < 0 || ($maxMarginTop - 2) < 0) {
            throw new \Exception('nie moge dodac watermarka');
        }

        $marginLeft = rand(20, 100);
        while ($marginLeft < $maxMarginLeft) {
            $marginTop = rand(2, $maxMarginTop);
            imagecopy($this->image, $watermark, $marginLeft, $marginTop, 0, 0, imagesx($watermark), imagesy($watermark));
            $marginLeft += imagesx($watermark)+rand(20, 100);
        }
        $this->image = $this->image;
    }

}
