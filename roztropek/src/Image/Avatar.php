<?php

namespace Roztropek\Image;

class Avatar {

    public static function getFilename($id) {
        return $id . '.png';
    }

}
