<?php

namespace Roztropek\Filter;

/**
 * Description of ValidatorChain
 *
 * @author kefas
 */
class FilterChain extends \Zend\Filter\FilterChain {

    /**
     * @param array $filters
     * @return \Roztropek\Validator\FilterChain
     * @throws \InvalidArgumentException
     */
    public function attachByNameArray(array $filters) {
        foreach ($filters as $filter) {
            if (!is_array($filter)) {
                throw new \InvalidArgumentException('validator config must be an array!');
            }
            if (!array_key_exists('name', $filter)) {
                throw new \InvalidArgumentException('name key doesent exists');
            }
            $name = strtolower($filter['name']);
            $options = array_key_exists('options', $filter) ? $filter['options'] : [];
            $priority = array_key_exists('priority', $filter) ? $filter['priority'] : self::DEFAULT_PRIORITY;
            parent::attachByName($name, $options, $priority);
        }
        return $this;
    }

}
