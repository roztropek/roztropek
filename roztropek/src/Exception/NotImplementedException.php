<?php

namespace Roztropek\Exception;

class NotImplementedException extends BadMethodCallException
{}