<?php

namespace Roztropek\Exception;

class ActionFailedException extends \Exception {

    private $reason = '';
    private $messages = [];

    public function getMessages(): array {
        return $this->messages();
    }

    public function setMessages(array $messages) {
        $this->messages = $messages;
    }

    public function setReason(string $reason) {
        $this->reason = $reason;
    }

    public function getReason(): string {
        return $this->reason;
    }

}
