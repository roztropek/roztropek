<?php

namespace Roztropek\Exception;

class IsNotValidException extends \InvalidArgumentException {

    protected $messages = [];

    public function setMessages(array $messages) {
        $this->messages = $messages;
    }

    public function getMessages(): array {
        return $this->messages;
    }

    public function getPlainMessages(): array {
        $messages = $this->getMessages();
        return array_reduce($messages, function($carry, $messageFilters) {
            $mergedCarry = array_merge($carry, $messageFilters);
            return $mergedCarry;
        }, []);
    }

    public function getMessagesAsString(): string {
        return implode('|', $this->getPlainMessages());
    }

}
