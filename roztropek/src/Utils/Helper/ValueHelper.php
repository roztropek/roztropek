<?php

namespace Roztropek\Utils\Helper;

 class ValueHelper{
     
     public static $NUMERIC = 'numeric';
     public static $NATURAL = 'natural';
     public static $INT = 'int';
     public static $STRING = 'string';
     public static $SCALAR = 'scalar';
     public static $ARRAY = 'array';
     public static $OBJECT = 'object';
     
     /**
      * @param mixed $value
      * @param string $type
      * @return boolean
      */
     public static function checkValue($value, $type) {
        switch ($type) {
            case self::$NUMERIC:
                return is_numeric($value);
            case self::$NATURAL:
                if(is_string($value)){
                    return ctype_digit($value) && (int) $value > 0;
                }else if(is_int($value)){
                    return $value > 0;
                }else{
                    return false;
                }
                break;
            case self::$INT:
                return ctype_digit($value);
            case self::$STRING:
                return is_string($value);
                break;
            case self::$SCALAR:
                return is_scalar($value);
                break;
            case self::$ARRAY:
                return is_array($value);
                break;
            case self::$OBJECT:
            default:
                return $value instanceof $type;
                break;
        }
    }
 }

