<?php

namespace Roztropek\Utils\Helper;

/**
 * @author kefas
 */
class ArrayHelper {

    /**
     * @param string $param
     * @param array $data
     * @param mixed $defaultValue
     * @param string $type
     */
    public static function getValue($param, $data, $defaultValue = null, $type = null, $arrayFilter = null) {
        if (!is_array($data)) {
            return $defaultValue;
        }
        $returnValue = $defaultValue;
        if (array_key_exists($param, $data)) {
            if ($type !== null && is_string($type)) {
                $returnValue = ValueHelper::checkValue($data[$param], $type) ? $data[$param] : $defaultValue;
                switch ($type) {
                    case 'array':
                        if (ValueHelper::checkValue($arrayFilter, 'string')) {
                            self::filterByType($data[$param], $arrayFilter);
                        }
                        break;
                }
            } else {
                $returnValue = $data[$param];
            }
        }
        return $returnValue;
    }

    public static function getKey($param, $data, $defaultValue = null, $type = null, $arrayFilter = null) {
        return static::getValue($param, $data, $defaultValue, $type, $arrayFilter);
    }

    /**
     * @param array $data
     * @param string $type
     * @return mixed
     */
    public static function filterByType(array $data, $type) {
        return array_filter($data, function ($value) use($type) {
            return ValueHelper::checkValue($value, $type);
        });
    }

    /**
     * @param array $data
     * @param array $keys
     * @return array
     */
    public static function filterByKEys(array $data, array $keys) {
        return array_intersect_key($data, array_flip($keys));
    }

    /**
     * @param array $keys
     * @param array $array
     * @param mixed $defaultValue
     * @return mixed
     */
    public static function getDeepValue(array $keys, array $array, $defaultValue = null) {
        if (empty($keys)) {
            return $defaultValue;
        }
        $key = array_shift($keys);
        if (array_key_exists($key, $array)) {
            $value = $array[$key];
            if (count($keys) > 0) {
                return static::getDeepValue($keys, $value, $defaultValue);
            } else {
                return $value;
            }
        } else {
            return $defaultValue;
        }
    }

}
