<?php

namespace Roztropek\Utils\Object;

class Object {

    public static function getShortClassName($object) {
        $reflect = new \ReflectionClass($object);
        return $reflect->getShortName();
    }

}
