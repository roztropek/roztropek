<?php

namespace Roztropek\Utils\Result;

/**
 * Description of Result
 *
 * @author kefas
 */
class Result {

    const RESOURCE_NOT_FOUND = 404;
    const IS_NOT_VALID = 422;
    const CREATED = 201;
    const OK = 200;
    const FORBIDDEN = 403;
    const BAD_REQUEST = 400;
    const INTERNAL_SERVER_ERROR = 500;
    
    /**
     * @param int $success
     * @param mixed $result
     * @param string|array $messages
     */
    public function __construct($success = null, $result = null, $messages = null){
        if($success !== null){
            $this->setSuccess($success);
        }
        if($result !== null){
            $this->setResult($result);
        }
        if($messages !== null){
            $this->setMessages($messages);
        }
    }
    /**
     * @var array | string
     */
    protected $messages = [];
    
    /**
     * @var mixed 
     */
    protected $result;
    
    /**
     * @var int 
     */
    protected $success = self::OK;
    
    /**
     * @return array
     */
    public function getMessages() {
        return $this->messages;
    }

    /**
     * @return mixed
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @return int
     */
    public function getSuccess() {
        return $this->success;
    }

    /**
     * @param array | string $messages
     * @return \Roztropek\Utils\Result\Result
     */
    public function setMessages($messages) {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @param mixed $result
     * @return \Roztropek\Utils\Result\Result
     */
    public function setResult($result) {
        $this->result = $result;
        return $this;
    }

    /**
     * @param int $success
     * @return \Roztropek\Utils\Result\Result
     */
    public function setSuccess($success) {
        $this->success = $success;
        return $this;
    }
    
    public function isValid(){
        return in_array($this->success, [self::CREATED, self::OK], true);
    }
}
