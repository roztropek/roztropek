<?php

namespace Roztropek\Utils\Result;

/**
 * Description of Result
 *
 * @author kefas
 */
class SimpleResult {

    /**
     * @var mixed
     */
    protected $message;

    /**
     * @var int 
     */
    protected $success = false;
    
    public function __construct($success = false, $message = '') {
        $this->setSuccess($success);
        $this->setMessage($message);
    }

    /**
     * @return mixed
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * @return boolean
     */
    public function getSuccess() {
        return $this->success;
    }

    /**
     * @param mixed $message
     * @return \Roztropek\Utils\Result\SimpleResult
     */
    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }

    /**
     * @param boolean $success
     * @return \Roztropek\Utils\Result\SimpleResult
     */
    public function setSuccess($success) {
        $this->success = $success;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isValid() {
        return $this->success === true;
    }

}
