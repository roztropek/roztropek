<?php

namespace Roztropek\Utils\Hash;

use Roztropek\Utils\Hash\Algorithm;

class Transformer {

    protected static function getFullClassName($name) {
        return '\Roztropek\Utils\Hash\Algorithm\\' . ucfirst($name);
    }

    public static function encode($value, $type) {
        $fullClassName = self::getFullClassName($type);
        if (!class_exists($fullClassName)) {
            throw new \Exception('Nie mam takiego algorytmu szyfrowania!');
        }
        return call_user_func(array($fullClassName, 'encode'), $value);
    }

    public static function decode($value, $type) {
        $fullClassName = self::getFullClassName($type);
        if (!class_exists($fullClassName)) {
            throw new \Exception('Nie mam takiego algorytmu szyfrowania!');
        }
        return call_user_func(array($fullClassName, 'decode'), $value);
    }

}
