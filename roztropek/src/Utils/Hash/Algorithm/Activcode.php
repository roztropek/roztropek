<?php

namespace Roztropek\Utils\Hash\Algorithm;
use Roztropek\Utils\Helper\ValueHelper;

class Activcode{
    private static $saltMultipler = 73819;
    
    public static function encode($value){
        return base64_encode($value);
    }
    
    public static function decode($value){
        return base64_decode($value);
    }
}

