<?php

namespace Roztropek\Utils\Hash\Algorithm;
use Roztropek\Utils\Helper\ValueHelper;

class Question{
    private static $saltMultipler = 75353;
    
    public static function encode($value){
        if(!ValueHelper::checkValue($value, 'array')){
            throw new \Exception($value." musi być tablicą!");
        }
        $idQuiz = \Roztropek\Utils\Helper\ArrayHelper::getKey('idQuiz', $value, null, 'natural');
        $idQuestion = \Roztropek\Utils\Helper\ArrayHelper::getKey('idQuestion', $value, null, 'natural');
        $randomHashMultipler = rand(1000,999999);
        
        $idQuizHash = base_convert($randomHashMultipler*$idQuiz,10,35);
        $idQuestionHash = base_convert($randomHashMultipler*$idQuestion,10,35);
        
        $randomHashMultiplerHash = base_convert($randomHashMultipler*self::$saltMultipler, 10, 35);
        
        $delimiter = self::getDelimiter($randomHashMultiplerHash);
        
        return $idQuestionHash.$delimiter.$idQuizHash.'z'.$randomHashMultiplerHash;
    }
    
    public static function decode($value){
        if(!ValueHelper::checkValue($value, 'string')){
            throw new \Exception($value." musi być ciągiem znaków!");
        }
        
        $start = strrpos($value, 'z');
        if($start === false){
            return false;
        }
        
        $randomHashMultiplerHash = substr($value, $start+1);
        $hashedValue = substr($value,0,$start);
        
        if(!$hashedValue || !$randomHashMultiplerHash){
            return false;
        }
        
        $delimiter = self::getDelimiter($randomHashMultiplerHash);
        $hashedValues = explode($delimiter, $hashedValue);
        
        if(count($hashedValues) !== 2){
            return false;
        }
        
        $randomHashMultiplerHashNatural = base_convert($randomHashMultiplerHash, 35, 10);
        if(!ValueHelper::checkValue($randomHashMultiplerHashNatural, 'natural')){
            return false;
        }
        $hashMultiplerHashNatural = $randomHashMultiplerHashNatural/self::$saltMultipler;
        if(!ValueHelper::checkValue($hashMultiplerHashNatural, 'natural')){
            return false;
        }
        
        $hashedIdQuiz = $hashedValues[1];
        $hashedIdQuestion = $hashedValues[0];
        
        $hashedIdQuizNatural = base_convert($hashedIdQuiz, 35, 10);
        $hashedIdQuestionNatural = base_convert($hashedIdQuestion, 35, 10);
        
        if(!ValueHelper::checkValue($hashedIdQuizNatural, 'natural') || !ValueHelper::checkValue($hashedIdQuestionNatural, 'natural')){
            return false;
        }
        
        $idQuiz = $hashedIdQuizNatural/$hashMultiplerHashNatural;
        $idQuestion = $hashedIdQuestionNatural/$hashMultiplerHashNatural;
        
        if(!ValueHelper::checkValue($idQuiz, 'natural') || !ValueHelper::checkValue($idQuestion, 'natural')){
            return false;
        }
        
        return [
            'idQuiz' => $idQuiz,
            'idQuestion' => $idQuestion
        ];
    }
    
    protected static function getDelimiter($hashValue){
        return substr($hashValue,2,1).'z'.substr($hashValue,-3,2);
    }
}

