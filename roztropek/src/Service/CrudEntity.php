<?php

namespace Roztropek\Service;

use Roztropek\Utils\Result\Result;

class CrudEntity extends Crud {

    use \Roztropek\Service\Traits\Doctrinable;

    public function edit($entity, array $data, array $fieldsToValidate = []) {
        $entityToEdit = $this->getEntityFromParam($entity);
        if (!$entityToEdit) {
            return $this->createResult(Result::RESOURCE_NOT_FOUND);
        }
        
        $entityToEdit->exchangeData($data);
        return $this->createResult(Result::OK, $entityToEdit);
    }

    public function create(array $data) {
        $entityClass = $this->getEntityClass();
        $entity = new $entityClass($data);
        $this->getEntityManager()->persist($entity);
        return $entity;
    }

    public function delete($entity) {
        $entityToDelete = $this->getEntityFromParam($entity);
        if (!$entityToDelete) {
            return $this->createResult(Result::RESOURCE_NOT_FOUND);
        }
        $this->getEntityManager()->remove($entityToDelete);
        return $this->createResult(Result::OK);
    }

    public function get($id) {
        $entity = $this->getEntityRepository()->find($id);
        $returnCode = $entity ? Result::OK : Result::RESOURCE_NOT_FOUND;
        return $this->createResult($returnCode, $entity);
    }

    public function getList(array $params = []) {
        return $this->createResult(Result::INTERNAL_SERVER_ERROR);
    }

    protected function getEntityFromParam($entity) {
        $entityClass = $this->getEntityClass();
        if (!$entity instanceof $entityClass) {
            $entity = $this->getEntityRepository()->find($entity);
        }
        return $entity;
    }

}

?>
