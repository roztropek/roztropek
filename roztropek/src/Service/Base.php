<?php

namespace Roztropek\Service;

use \Zend\ServiceManager\FactoryInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Roztropek\Utils\Result\Result;

class Base implements FactoryInterface {

    public $serviceLocator;

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }
    
    /**
     * @param int $success
     * @param mixed $result
     * @param string|array $messages
     * @return \Roztropek\Utils\Result\Result
     */
    protected function createResult($success, $result = null, $messages = []){
        return new Result($success, $result, $messages);
    }
    
    protected function getService($name){
        return $this->serviceLocator->get($name);
    }
}

?>
