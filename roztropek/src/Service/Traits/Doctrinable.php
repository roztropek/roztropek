<?php

namespace Roztropek\Service\Traits;

trait Doctrinable{
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var * @return \Doctrine\ORM\EntityRepository
     */
    protected $entityRepository;

    /*
     * @return string
     * @throws Exception
     */

    protected function getEntityClass() {
        if (!$this->entityClass) {
            throw new Exception('Brakuje klasy entity!');
        }
        return $this->entityClass;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager() {
        if (!$this->entityManager) {
            $this->setEntityManager($this->serviceLocator->get('doctrine.entitymanager.orm_default'));
        }
        return $this->entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getEntityRepository() {
        if (!$this->entityRepository) {
            $this->setEntityRepository($this->getRepository($this->getEntityClass()));
        }
        return $this->entityRepository;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    protected function setEntityManager(\Doctrine\ORM\EntityManager $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Doctrine\ORM\EntityRepository $repository
     */
    protected function setEntityRepository(\Doctrine\ORM\EntityRepository $entityRepository) {
        $this->entityRepository = $entityRepository;
    }

    /**
     * @param string $entityName
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository($entityName) {
        return $this->getEntityManager()->getRepository($entityName);
    }
    
    protected function persist($entity){
        $this->getEntityManager()->persist($entity);
    }
    
    public function find($id) {
        return $this->getEntityRepository()->find($id);
    }
    
    public function findBy(array $criteria, $orderBy = null, $limit = null, $offset = null) {
        return $this->getEntityRepository()->findBy($criteroa, $orderBy, $limit, $offset);
    }
    
    public function findOneBy(array $criteria, $orderBy = null) {
        return $this->getEntityRepository()->findOneBy($criteria, $orderBy);
    }

}
