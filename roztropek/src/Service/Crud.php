<?php

namespace Roztropek\Service;

use \Roztropek\Exception\NotImplementedException;

abstract class Crud extends Base {
    
    /**
     * @param int $id
     * @param array $data
     * @return \Roztropek\Utils\Result\Result;
     */
    abstract public function edit($id, array $data, array $fieldsToValidate = []);
    
    /**
     * @param array $data
     * @return \Roztropek\Utils\Result\Result;
     */    
    abstract public function create(array $data);
    
    /**
     * @return \Roztropek\Utils\Result\Result;
     */    
    abstract public function delete($id);
    
    /**
     * @param int $id
     * @return \Roztropek\Utils\Result\Result;
     */    
    abstract public function get($id);
    
    
    /**
     * @param array $params
     * @return \Roztropek\Utils\Result\Result;
     */    
    abstract public function getList(array $params = []);
}

?>
