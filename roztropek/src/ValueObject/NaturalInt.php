<?php

namespace Roztropek\ValueObject;

final class NaturalInteger{
    private $intValue = null;
    
    public function __construct($value) {
        $intValue = $this->convertToInt($value);
        if($intValue < 0){
            throw new \InvalidArgumentException("$value to nie jest liczba naturalna!");
        }
        $this->intValue = $intValue;
    }
   private function convertToInt(&$value){
       if(is_string($value) && ctype_digit($value) || is_int($value)){
            return (int)$value;    
       }
       return -1;
   }
}
