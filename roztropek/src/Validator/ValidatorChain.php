<?php

namespace Roztropek\Validator;

/**
 * Description of ValidatorChain
 *
 * @author kefas
 */
class ValidatorChain extends \Zend\Validator\ValidatorChain {

    /**
     * @param array $validators
     * @return \Roztropek\Validator\ValidatorChain
     * @throws \InvalidArgumentException
     */
    public function attachByNameArray(array $validators) {
        foreach ($validators as $validator) {
            if (!is_array($validator)) {
                throw new \InvalidArgumentException('validator config must be an array!');
            }
            if (!array_key_exists('name', $validator)) {
                throw new \InvalidArgumentException('name key doesent exists');
            }
            $name = strtolower($validator['name']);
            $options = array_key_exists('options', $validator) ? $validator['options'] : [];
            $breakChainOnFailure = array_key_exists('breakChainOnFailure', $validator) ? $validator['breakChainOnFailure'] : false;
            parent::attachByName($name, $options, $breakChainOnFailure);
        }
        return $this;
    }
    
    public function setPluginManager(\Zend\Validator\ValidatorPluginManager $plugins) {
        $plugins->setInvokableClass('CategoryParent', '\Roztropek\Validator\Question\Category\Ancestor');
        parent::setPluginManager($plugins);
    }

}
