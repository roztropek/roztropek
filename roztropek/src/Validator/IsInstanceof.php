<?php

namespace Roztropek\Validator;

use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;

class IsInstanceof extends AbstractValidator {

    const NOT_INSTANCEOF = 'notInstanceof';

    protected $classname;
    protected $messageTemplates = array(
        self::NOT_INSTANCEOF => 'The input is not instanceof %classname%'
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'classname' => 'classname'
    );

    /**
     * Sets validator options
     *
     * @param  array|Traversable $options
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($options = null) {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        if (!array_key_exists('classname', $options)) {
            throw new Exception\InvalidArgumentException("Missing option 'classname'");
        }

        $this->setClassname($options['classname']);

        parent::__construct($options);
    }

    public function isValid($value) {
        $classname = $this->getClassname();
        $result = $value instanceof $classname;
        if (!$result) {
            $this->error(self::NOT_INSTANCEOF);
            return false;
        }
        return true;
    }

    public function getClassname() {
        return $this->classname;
    }

    public function setClassname($classname) {
        $this->classname = $classname;
    }

}
