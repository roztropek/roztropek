<?php

namespace Roztropek\Validator;

use Zend\Validator\AbstractValidator;

class Count extends AbstractValidator {

    const IS_LOWER = 'isLower';
    const IS_GREATER = 'isGreater';
    const IS_NOT_EQUAL = 'isNotEqual';

    protected $messageTemplates = array(
        self::IS_GREATER => "%value% is greater than %min%",
        self::IS_LOWER => "%value% is lower than %max%",
        self::IS_NOT_EQUAL => "%value% is not equal %equals%"
    );

    /**
     * @var array
     */
    protected $messageVariables = array(
        'min' => 'min',
        'max' => 'max',
        'equals' => 'equals'
    );
    protected $min;
    protected $max;
    protected $equals;

    /**
     * Sets validator options
     *
     * @param  array|Traversable $options
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($options = null) {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        if (!array_key_exists('min', $options) &&
                !array_key_exists('max', $options) &&
                !array_key_exists('equals', $options)) {
            throw new Exception\InvalidArgumentException("Missing option 'min', 'max', or 'equals'");
        }

        parent::__construct($options);
    }

    public function isValid($value) {
        $value = count($value);
        $isValid = true;
        if ($this->getMin() && $value <= $this->getMin()) {
            $this->error(self::IS_LOWER, $value);
            $isValid = false;
        }
        if ($this->getMax() && $value >= $this->getMax()) {
            $this->error(self::IS_GREATER, $value);
            $isValid = false;
        }
        if ($this->getEquals() && $value !== $this->getEquals()) {
            $this->error(self::IS_NOT_EQUAL, $value);
            $isValid = false;
        }

        return $isValid;
    }

    public function getMin() {
        return $this->min;
    }

    public function getMax() {
        return $this->max;
    }

    public function getEquals() {
        return $this->equals;
    }

    public function setMin($min) {
        $this->min = $min;
    }

    public function setMax($max) {
        $this->max = $max;
    }

    public function setEquals($equals) {
        $this->equals = $equals;
    }

}
