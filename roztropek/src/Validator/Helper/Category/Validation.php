<?php

namespace Roztropek\Validator\Helper\Category;

use Question\Entity\Category;

class Validation {

    public static function isParentValid($parent) {
        if (!$parent) {
            return true;
        }
        return $parent->getParent() == null;
    }

}
