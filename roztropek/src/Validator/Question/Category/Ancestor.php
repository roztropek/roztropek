<?php

namespace Roztropek\Validator\Question\Category;

use Zend\Validator\AbstractValidator;

class Ancestor extends AbstractValidator {

    const NOT_EXISTS = 'notExists';
    const HAS_PARENT = 'hasParent';

    protected $messageTemplates = array(
        self::NOT_EXISTS => "Parent doesent exists",
        self::HAS_PARENT => "Parent has parent"
    );

    public function isValid($value) {
        $this->setValue($value);
        if(!$value){
            return true;
        }
        try {
            $parent = $value->getParent();
        } catch (\Doctrine\ORM\EntityNotFoundException $ex) {
            $this->error(self::NOT_EXISTS);
            return false;
        }
        if ($parent) {
            $this->error(self::HAS_PARENT);
            return false;
        }
        return true;
    }

}
