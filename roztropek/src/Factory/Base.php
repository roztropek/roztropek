<?php

namespace Roztropek\Factory;

use Zend\ServiceManager\ServiceLocatorAwareInterface;

abstract class Base {
    
    public $serviceLocator;

    /**
     * @param ServiceLocatorAwareInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }
    
    protected function getService($name){
        return $this->serviceLocator->get($name);
    }
    
    /**
     * @param array
     * @return mixed
     */
    abstract public function create(array $data);
    
}