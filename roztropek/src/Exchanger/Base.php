<?php

namespace Roztropek\Exchanger;

use Zend\InputFilter\Factory;
use Roztropek\Exception\IsNotValidException;

abstract class Base {

    /**
     * @var array 
     */
    private $inputFilterConfig = [];

    protected function createInputFilter(array $fields) {
        $factory = new Factory();
        $inputFilterConfig = array_intersect_key($this->getInputFilterConfig(), array_flip($fields));
        return $factory->createInputFilter($inputFilterConfig);
    }

    protected function getInputFilterConfig() {
        return $this->inputFilterConfig;
    }

    protected function getAllFields() {
        return array_keys($this->getInputFilterConfig());
    }

    public function getRequiredFields() {
        $fields = [];
        foreach ($this->getInputFilterConfig() as $fieldName => $fieldConfig) {
            if (array_key_exists('required', $fieldConfig) && $fieldConfig['required'] === true) {
                $fields[] = $fieldName;
            }
        }
        return $fields;
    }

    public function getExchangeData(array $data) {
        $fieldsToExchange = array_keys($data);
        $inputFilter = $this->createInputFilter($fieldsToExchange);
        $dataToSet = array_intersect_key($data, array_flip($fieldsToExchange));
        $inputFilter->setData($dataToSet);
        if (!$inputFilter->isValid()) {
            $exception = new IsNotValidException('Błędne dane przekazane do Exchanger: ' . get_class($this) . ' ' . json_encode($inputFilter->getMessages()));
            $exception->setMessages($inputFilter->getMessages());
            throw $exception;
        }
        $values = $inputFilter->getValues();
        return $inputFilter->getValues();
    }

}
