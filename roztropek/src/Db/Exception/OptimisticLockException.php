<?php
namespace Roztropek\Db\Exception;

class OptimisticLockException extends \Exception {}
