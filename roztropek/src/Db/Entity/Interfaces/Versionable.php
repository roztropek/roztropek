<?php

namespace Roztropek\Db\Entity\Interfaces;

interface Versionable {
    public function getVersion();
    public function isLocked();
    public function lock();
    public function unlock();
    public function markAsDirty();
}
