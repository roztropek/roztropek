<?php

namespace Roztropek\Db\Entity;

use Roztropek\Exchanger\Base as Exchanger;

abstract class Base {

    /**
     * @var \Roztropek\Exchanger\Base
     */
    protected $exchanger;

    /**
     * @var string
     */
    protected $exchangerClass;

    /**
     * @var array
     */
    private $settersMapping = [];

    public function __construct(array $data) {
        $this->exchangeData(array_merge(array_flip($this->getExchanger()->getRequiredFields()), $data));
    }

    protected function getExchangerClass() {
        if (!$this->exchangerClass) {
            $this->exchangerClass = str_replace('\\Entity\\', '\\Exchanger\\', get_class($this));
        }
        return $this->exchangerClass;
    }

    protected function setExchangerClass($exchangerClass) {
        $this->exchangerClass = $exchangerClass;
    }

    protected function getSettersMapping() {
        return $this->settersMapping;
    }

    protected function setSettersMapping(array $settersMapping) {
        $this->settersMapping = $settersMapping;
    }

    protected function getExchanger() {
        if (!$this->exchanger) {
            $exchangerClass = $this->getExchangerClass();
            $this->setExchanger(new $exchangerClass());
        }
        return $this->exchanger;
    }

    protected function setExchanger(Exchanger $exchanger) {
        $this->exchanger = $exchanger;
    }

    public function exchangeData(array $data) {
        $exchanger = $this->getExchanger();
        $dataToExchange = $exchanger->getExchangeData($data);
        $this->runSetters($this->getSettersMapping(), $dataToExchange);
    }

    protected function runSetters(array $methodMapping, array $data) {
        foreach ($methodMapping as $dataKey => $methodName) {
            if (array_key_exists($dataKey, $data)) {
                $this->$methodName($data[$dataKey]);
            }
        }
    }

}