<?php

namespace Roztropek\Db\Entity\Traits;

trait Versionable {

    /**
     * @ORM\Version
     * @ORM\Column(type="integer") 
     */
    protected $version;

    /** @ORM\Column(type="string", length=40, nullable=true) */
    protected $dirtyHash;
    protected $isLocked = false;

    /**
     * @return int
     */
    public function getVersion() {
        return $this->version;
    }

    /**
     * @return boolean
     */
    public function isLocked() {
        return $this->isLocked;
    }

    public function lock() {
        $this->isLocked = true;
    }
    
    public function unlock() {
        $this->isLocked = false;
    }

    public function markAsDirty() {
        $this->dirtyHash = md5(microtime());
    }
}
