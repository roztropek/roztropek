<?php

namespace Roztropek\Db\Entity\Traits;

trait Creatable {
    /** @ORM\Column(type="datetime") */
    protected $createdAt;
    
    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->createdAt = new \DateTime();
    }
    
    public function getCreatedAt() {
        return $this->createdAt;
    }
}
