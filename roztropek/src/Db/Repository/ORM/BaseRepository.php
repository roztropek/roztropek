<?php

namespace Roztropek\Db\Repository\ORM;

use \Doctrine\ORM\EntityRepository;
use Doctrine\ORM\AbstractQuery;
use Roztropek\Db\Entity\Base as BaseEntity;
use Roztropek\Utils\Result\Result;

class BaseRepository extends EntityRepository {
    
    
    /**
     * @param int $id
     * @return Result
     */
    public function delete($id) {
        $entity = $this->find($id);
        $result = new Result();
        if ($entity) {
            $this->getEntityManager()->remove($entity);
        }
        return $result;
    }
    
    protected function getCount(\Doctrine\ORM\QueryBuilder $qb, $whatToCount){
        $count = $qb->expr()->count($whatToCount);
        $qbCount = clone $qb;
        $qbCount->select($count);
        return  $qbCount->getQuery()->getSingleScalarResult();
    }
    
    /**
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection(){
        return $this->getEntityManager()->getConnection();
    }
}
