<?php

namespace Roztropek\Email;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mail\Transport\Smtp as SmtpTransport;

class Transport {

    public static function getTransport($config) {
        $options = new SmtpOptions($config);
        $smtpTransport = new SmtpTransport();
        $smtpTransport->setOptions($options);
        return $smtpTransport;
    }

}
