<?php

namespace Roztropek\Email;

class Template {

    public static function getTemplate($name, array $params = []) {
        $functionName = sprintf('get%sTemplate', ucfirst($name));
        if (is_callable([__CLASS__, $functionName])) {
            $template = call_user_func([__CLASS__, $functionName]);
            if (!empty($params)) {
                $search = array_map(function($key) {
                    return sprintf('{{%s}}', $key);
                }, array_keys($params));
                $replace = array_values($params);
                return str_replace($search, $replace, $template);
            }
            return $template;
        } else {
            throw new \InvalidArgumentException('nie znaleziono template ' + $name);
        }
    }

    protected static function getRegisterTemplate() {
        return 'Dziękujemy za rejestrację. Aby aktywować konto kliknij tu: {{applicationUrl}}/#/account-activation/{{activateCode}}';
    }

    protected static function getContactTemplate() {
        return 'Zgłoszenie od {{name}} - {{email}}<br/>
            temat: {{subject}}<br/>
            tresc: {{text}}';
    }

    protected static function getResetPasswordRequestTemplate() {
    $template = '<div style="background-color:#00533e;padding:0;margin:0;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:1.4">
  <div style="height:70px;background-color:#00684e;text-align:center;padding:30px 0 0">
    <img src="{{applicationUrl}}/app/assets/img/logo.png">
  </div>
  <div style="background-color:#fff;border-radius:2px;padding:40px;width:100%;max-width:600px;margin:30px auto">
    <div style="padding:30px 40px;border:4px solid #baa529">
      <h1 style="margin:0 0 20px 0;text-align:center;font-weight:300">Cześć, {{nick}}</h1>
      <div style="text-align:center;margin:0 0 20px 0"><img src="{{avatar}}" style="width:160px;height:160px;border-radius:50%"></div>
      <h2 style="margin:0 0 20px 0;text-align:center;font-weight:400">Zapomniałeś hasła?</h2>
      <p style="margin:0 0 30px">Nie martw się, nic się nie stało. Aby zresetować swoje hasło kliknij przycisk poniżej.</p>
      <a style="font-weight:600;width:100%;max-width:300px;text-align:center;margin:0 auto;border-radius:2px;font-size:14px;display:block;background-color:#baa529;color:#fff;text-shadow:1px 1px 1px rgba(0,0,0,.3);padding:15px 20px;text-decoration:none;text-transform:uppercase" href="{{applicationUrl}}/#/change-password/{{token}}">Resetuj hasło</a>
    </div>
  </div>
</div>';


        return $template;
//        
//        return 'Witamy,
//            Aby dokończyć reset hasła wejdź na stronę: :applicationUrl:/change-password/:token:';
    }

}
