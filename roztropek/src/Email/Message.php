<?php

namespace Roztropek\Email;

use Zend\Mail\Message as SendMessage;
use Roztropek\Utils\Helper\ArrayHelper;

class Message {

    /**
     * 
     * @param string $type
     * @param array $config
     * @return Message
     */
    public static function create($config = []) {
        $message = new SendMessage();
        $from = ArrayHelper::getKey('from', $config, 'no-reply@roztropek.pl', 'string');
        $fromName = ArrayHelper::getKey('fromName', $config, 'Roztropek.pl', 'string');
        $to = ArrayHelper::getKey('to', $config, null, 'string');
        $replyTo = ArrayHelper::getKey('replyTo', $config, null, 'string');
        $replyToName = ArrayHelper::getKey('replyToName', $config, null, 'string');
        $subject = ArrayHelper::getKey('subject', $config, 'mail', 'string');

        $message->setFrom($from, $fromName)
                ->setTo($to)
                ->setSubject($subject);
        if ($replyTo !== null) {
            $message->setReplyTo($replyTo, $replyToName);
        }
        return $message;
    }

}
