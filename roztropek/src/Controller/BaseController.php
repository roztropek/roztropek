<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Roztropek\Controller;

use Zend\Mvc\Controller\AbstractActionController;
//use Roztropek\Db\Exception\OptimisticLockException;
use Doctrine\ORM\OptimisticLockException;

abstract class BaseController extends AbstractActionController {
    
    protected $entityManager;
    protected $entityClass;
    protected $retryCount = 0;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager() {
        if (!$this->entityManager) {
            $this->setEntityManager($this->getServiceLocator()->get('doctrine.entitymanager.orm_default'));
        }
        return $this->entityManager;
    }

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    protected function setEntityManager(\Doctrine\ORM\EntityManager $em) {
        $this->entityManager = $em;
    }

    protected function getRepository($repositoryName, $managerType = 'entity') {
        $manager = $this->getEntityManager();
        return $manager->getRepository($repositoryName);
    }

    protected function flush() {
        $this->getEntityManager()->flush();
    }

    protected function retry() {
        $this->retryCount++;
        return $this->params()->getController()->dispatch($this->getRequest());
    }

    protected function finalize($response = null, $isFlush = true) {
        if ($isFlush === true) {
            try {
                $this->flush();
            } catch (OptimisticLockException $ex) {
                if ($this->retryCount < 5) {
                    $response = $this->retry();
                } else {
                    throw new \Exception('Optimistic lock - coś nie wyszło');
                }
            }
        }
        $this->getService('Notifier')->flush();
        return $response;
    }

    protected function getService($serviceName) {
        return $this->getServiceLocator()->get($serviceName);
    }

}
