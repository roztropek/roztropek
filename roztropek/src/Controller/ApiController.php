<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Roztropek\Controller;

use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Json\Json;

abstract class ApiController extends BaseController {

    const CONTENT_TYPE_JSON = 'json';

    /**
     * @var int From Zend\Json\Json
     */
    protected $jsonDecodeType = Json::TYPE_ARRAY;

    /**
     * @var array
     */
    protected $contentTypes = array(
        self::CONTENT_TYPE_JSON => array(
            'application/hal+json',
            'application/json'
        )
    );
    
    public function onDispatch(MvcEvent $e) {
        if (strtolower($this->getRequest()->getMethod()) === 'options') {
            $e->stopPropagation();
        }else{
            parent::onDispatch($e);
        } 
    }

    protected function notAllowed() {
        $this->response->setStatusCode(405);
        return new JsonModel(array('success' => false));
    }

    protected function getData() {
        $request = $this->getRequest();
        $content = $request->getContent();
        
        // JSON content? decode and return it.
        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($content, $this->jsonDecodeType);
            return $data;
        }

        return [];
    }
    
    protected function getFilteredData(array $fields = []){
        $data = $this->getData();
        return array_intersect_key($data,array_flip($fields));
    }

    protected function getIdentity() {
        return $this->getServiceLocator()->get('auth')->getIdentity();
    }

    /**
     * Check if request has certain content type
     *
     * @param  Request $request
     * @param  string|null $contentType
     * @return bool
     */
    public function requestHasContentType(Request $request, $contentType = '') {
        /** @var $headerContentType \Zend\Http\Header\ContentType */
        $headerContentType = $request->getHeaders()->get('content-type');
        if (!$headerContentType) {
            return false;
        }

        $requestedContentType = $headerContentType->getFieldValue();
        if (strstr($requestedContentType, ';')) {
            $headerData = explode(';', $requestedContentType);
            $requestedContentType = array_shift($headerData);
        }
        $requestedContentType = trim($requestedContentType);
        if (array_key_exists($contentType, $this->contentTypes)) {
            foreach ($this->contentTypes[$contentType] as $contentTypeValue) {
                if (stripos($contentTypeValue, $requestedContentType) === 0) {
                    return true;
                }
            }
        }

        return false;
    }
    
    public function isIdentityMember(){
        $identity = $this->getIdentity();
        return $identity && $identity['role'] === 'member';
    }
    
    public function isIdentityPlayer(){
        $identity = $this->getIdentity();
        return $identity && $identity['role'] === 'player';
    }
    
    public function isIdentityAdmin(){
        $identity = $this->getIdentity();
        return $identity && $identity['role'] === 'administrator';
    }
    
    public function getRole(){
        $identity = $this->getIdentity();
        return !$identity ? 'guest' : $identity['role'];
    }
    
    public function isIdentityGuest(){
        $identity = $this->getIdentity();
        return empty($identity);
    }
}
