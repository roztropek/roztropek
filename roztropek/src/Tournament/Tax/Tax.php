<?php

namespace Roztropek\Tournament\Tax;

class Tax {

    protected static $taxes = [
        'tournament' => [
            'sitgo' => [
                'regular' => 0.25,
                'premium' => 0.15
            ],
            'tournament' => [
                'regular' => 0.25,
                'premium' => 0.15
            ]
        ],
        'default' => 0.25
    ];

    /**
     * @param int $cashType
     * @param int $tournamentType
     * @param boolean $isPremium
     * @return float
     */
    public static function getTaxFactor($cashType, $tournamentType, $isPremium = false) {
        switch ($cashType) {
            case 1:
                switch ($tournamentType) {
                    case 1:
                        $taxes = self::$taxes['tournament']['sitgo'];
                        return $isPremium === true ? $taxes['premium'] : $taxes['regular'];
                        break;
                    case 2:
                        $taxes = self::$taxes['tournament']['tournament'];
                        return $isPremium === true ? $taxes['premium'] : $taxes['regular'];
                        break;
                    default:
                        return self::$taxes['default'];
                }
                break;
            case 2:
                return 0;
                break;
            default:
                return self::$taxes['default'];
        }
    }

    /**
     * @param int $cashType
     * @param int $tournamentType
     * @param int $bid
     * @param boolean $isPremium
     * @return int
     */
    public static function getTaxValue($cashType, $tournamentType, $bid, $isPremium = false) {
        return floor($bid * self::getTaxFactor($cashType, $tournamentType, $isPremium));
    }

}
