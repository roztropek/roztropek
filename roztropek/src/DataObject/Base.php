<?php
namespace Roztropek\DataObject;
use Zend\InputFilter\Factory;

abstract class Base {
    
    /**
     * @var \Zend\InputFilter\InputFilter
     */
    protected $inputFilter;
    
    /**
     * @var array 
     */
    private $inputFilterConfig;
    
    public function __construct(array $data) {
        $this->setInputFilterConfig($this->createInputFilterConfig());
        $inputKeys = $this->mapData($data);
        $this->inputFilter = $this->createInputFilter();
        $this->inputFilter->setData($this->filterInputKeys($inputKeys));
        if(!$this->inputFilter->isValid()){
            throw new \InvalidArgumentException('Błędne dane przekazane do ValueObject: '. get_class($this).' '.json_encode($this->inputFilter->getMessages()));
        }
    }
    
    protected function createInputFilter(){
        $factory = new Factory();
        
        return $factory->createInputFilter($this->getInputFilterConfig());
    }
    
    protected function createInputFilterConfig(){
        return [];
    }
    
    protected function setInputFilterConfig(array $inputFilterConfig){
        $this->inputFilterConfig = $inputFilterConfig;
    }
    
    protected function getInputFilterConfig(){
        return $this->inputFilterConfig;
    }
    
    protected function filterInputKeys(array $data){
        return array_intersect_key($data,array_flip($this->getAllowedFields()));
    }
    
    protected function getAllowedFields(){
        return array_keys($this->getInputFilterConfig());
    }
    
    protected function mapData(array $data){
        return $data;
    }
    
    public function getData(){
        return $this->inputFilter->getValues();
    }
}
