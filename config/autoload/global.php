<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'path' => [
        'avatar' => [
            'min' => __DIR__ . '/../../public/avatar/min',
            'natural' => __DIR__ . '/../../public/avatar/natural',
            'normal' => __DIR__ . '/../../public/avatar/normal',
            'tiny' => __DIR__ . '/../../public/avatar/tiny',
        ]
    ],
    'sendMail' => [
        'no-reply' => [
            'name' => 'mail.roztropek.pl',
            'host' => 'mail.roztropek.pl',
            'port' => 587,
            'connection_class' => 'login',
            'connection_config' => array(
                'username' => 'no-reply@roztropek.pl',
                'password' => 'roztropek123',
            ),
        ]
    ]
);
