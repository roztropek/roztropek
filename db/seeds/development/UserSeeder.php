<?php

use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed {

    protected $wallets = [];
    protected $users = [];

    public function run() {       
        $this->execute("DELETE FROM QuizQuestion;");
        $this->execute("DELETE FROM Quiz;");
        $this->execute("DELETE FROM Notification;");
        $this->execute("DELETE FROM Account;");
        $this->execute("DELETE FROM User;");
        $this->execute("DELETE FROM Wallet;");
        $this->execute("DELETE FROM Transaction;");
        
        foreach (self::$names as $name) {
            $this->addUser($name);
            $this->addUser(sprintf('%s%d', $name, strlen($name)*3));
            foreach (self::$nicknames as $nick) {
                $this->addUser(sprintf('%s_%s', $name, $nick));
            }
        }

        foreach (self::$nicknames as $name) {
            $this->addUser($name);
            $this->addUser(sprintf('%s%d', $name, rand(15, 99)));
        }
        
        $this->table('Wallet')->insert($this->wallets)->save();
        $this->table('User')->insert($this->users)->save();
    }

    protected function getUserData($nick) {
        $uuid = Ramsey\Uuid\Uuid::uuid4();
        $userData = [
            'id' => $uuid->toString(),
            'email' => sprintf('kefas.poland+%s@gmail.com', $nick),
            'password' => 'd20cf282f4c8e2fbeb74b463c1270db5f4df7bce',
            'nick' => $nick,
            'experience' => 0,
            'status' => 1,
            'avatar' => rand(1, 5)
        ];
        return $userData;
    }

    protected function addUser($nick) {
        $walletData = $this->getWalletData();
        $userData = $this->getUserData($nick);
        $userData['idWallet'] = $walletData['id'];
        $this->wallets[] = $walletData;
        $this->users[] = $userData;
    }

    protected function getWalletData() {
        $uuid = Ramsey\Uuid\Uuid::uuid4();
        $wallet = [
            'id' => $uuid->toString(),
            'playCoins' => 80000,
            'gold' => 80000,
            'version' => 1
        ];
        return $wallet;
    }

    protected static $names = ['rafal', 'adam', 'robert', 'jerzy', 'bartek',
        'maniek', 'janek', 'jan', 'jarek', 'pawel', 'piotr', 'borys', 'mieszko',
        'emil', 'egon', 'gwidon', 'ernest', 'donald', 'miron', 'bruno', 'radek',
        'marian', 'damian', 'kamil'
    ];
    protected static $nicknames = [
        'lysy', 'sciana', 'tasak', 'gruby', 'chudy', 'maly', 'cichy', 'bosy',
        'rudy', 'benny', 'bary', 'silny', 'szczodry', 'kafar', 'kefir',
        'turas', 'ciemny', 'bialy', 'szary', 'mulat', 'radar', 'goly', 'slepy'
    ];

}
