<?php

use Phinx\Seed\AbstractSeed;

class AdminSeeder extends AbstractSeed {

    public function run() {
        $this->execute("DELETE FROM Administrator;");
        $this->table('Administrator')->insert($this->administrator)->save();
    }

    protected $administrator = array(
        array('id' => 'a25f67df-7fe5-11e5-5436-57ad60d81bfc', 'login' => 'szkodnik', 'password' => 'afe2cda3cfd21399bdc29f05abba08a08ce2a727', 'type' => '1'),
        array('id' => 'a25f67df-7fe5-11e5-9121-57ad60d81bfc', 'login' => 'kefas', 'password' => 'd20cf282f4c8e2fbeb74b463c1270db5f4df7bce', 'type' => '1'),
        array('id' => 'a25f67df-7fe5-11e5-9874-57ad60d81bfc', 'login' => 'damian', 'password' => '173f052d8abfcc02e68151e320eefd811ac1c239', 'type' => '1')
    );

}
