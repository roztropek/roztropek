<?php

use Phinx\Migration\AbstractMigration;

class ChangeAccountToBankAccount extends AbstractMigration {

    public function up() {
        $sql = "ALTER TABLE User ADD bankAccount VARCHAR(50) DEFAULT NULL;
            ALTER TABLE Wallet DROP activeCoins, DROP tickets;
            DROP INDEX IDX_A295BD916D8F3D56 ON Payment;
            ALTER TABLE Payment CHANGE idaccount account VARCHAR(255) DEFAULT NULL;
        ";
        $this->execute($sql);
    }

}
