<?php

use Phinx\Migration\AbstractMigration;

class ChangeIdaccountToAccountInPaymente extends AbstractMigration
{
    public function up()
    {
        $sql = "ALTER TABLE Payment CHANGE idaccount account VARCHAR(255) DEFAULT NULL;";
        $this->execute($sql);
    }
}
