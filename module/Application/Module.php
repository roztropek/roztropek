<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Zend\Http\Request as HttpRequest;

class Module {

    protected static $startTime;

    public function onBootstrap(MvcEvent $e) {
//        error_reporting(E_ALL ^ E_USER_DEPRECATED ^ E_STRICT);
        $request = $e->getRequest();
        $this->extendDoctrine($e);
        $this->attachDoctrineListeners($e);
        if ($request instanceof HttpRequest) {
            $this->doHttpRequest($e);
        }
    }

    public function extendDoctrine(MvcEvent $e) {
        $sm = $e->getApplication()->getServiceManager();
        $doctrineEntityManager = $sm->get('doctrine.entitymanager.orm_default');
        $doctrineEntityManager->getConfiguration()->addCustomStringFunction('date_format', '\DoctrineExtensions\Query\Mysql\DateFormat');
        $doctrineEntityManager->getConfiguration()->addCustomStringFunction('round', '\DoctrineExtensions\Query\Mysql\Round');
    }

    public function attachDoctrineListeners(MvcEvent $e) {
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $doctrineEntityManager = $sm->get('doctrine.entitymanager.orm_default');
        $doctrineEventManager = $doctrineEntityManager->getEventManager();
        $doctrineEventManager->addEventListener(
                [\Doctrine\ORM\Events::onFlush], new \Application\Listener\DoctrineListener($sm)
        );
    }

    public function doHttpRequest(MvcEvent $e) {
        try {
            $originHeader = $e->getRequest()->getHeader('Origin');
            $origin = $originHeader ? $originHeader->getFieldValue() : null;
            if ($origin) {
                header('Access-Control-Allow-Origin : ' . $origin);
            }
        } catch (\Exception $ex) {
            
        }

        header('Access-Control-Allow-Methods: OPTIONS, GET, POST, PUT, DELETE');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With, x-token');
        header('Access-Control-Request-Headers: x-token');
        $this->setJsonStrategy($e);
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $authService = $e->getApplication()->getServiceManager()->get('Auth');
        $authService->authenticate();

        $this->logRequest($e);
        $e->getApplication()->getEventManager()->attach('dispatch', function($e) {
            $aclService = $e->getApplication()->getServiceManager()->get('acl');
            $params = $e->getRouteMatch()->getParams();
            $result = $aclService->isAllowed(\Auth\Service\Acl::getResourceFromControllername($params['controller']), $params['action']);
            if (!$result->isAuthorized()) {
                $jsonModel = new JsonModel(['success' => false]);
                $e->getResponse()->setStatusCode($result->getResult());
                $jsonModel->setTerminal(true);
                $e->setResult($jsonModel);
                $e->setViewModel($jsonModel);
                $e->stopPropagation();
            }
        }, 100);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function setJsonStrategy(MvcEvent $e) {
        $app = $e->getTarget();
        $locator = $app->getServiceManager();
        $view = $locator->get('ZendViewView');
        $strategy = $locator->get('ViewJsonStrategy');
        $view->getEventManager()->attach($strategy, 100);

        // attach a listener to check for errors
        $events = $app->getEventManager();
        $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'onDispatchError']);
        $events->attach(MvcEvent::EVENT_RENDER, array($this, 'onRenderError'));
    }

    public function onDispatchError(MvcEvent $e) {
        $exception = $e->getParam('exception');
        if ($exception instanceof \Roztropek\Exception\ActionFailedException) {
            $jsonModel = new JsonModel([
                'success' => false,
                'message' => explode('|', $exception->getMessage()),
                'reason' => $exception->getReason()
            ]);
            $jsonModel->setTerminal(true);
            if ($e->getRequest() instanceof HttpRequest) {
                $e->getResponse()->setStatusCode(422);
            }
            $e->setResult($jsonModel);
            $e->setViewModel($jsonModel);
        }
    }

    public function onRenderError(MvcEvent $e) {
        $request = $e->getRequest();
        if ($request instanceof HttpRequest) {
            $this->doHttpRenderError($e);
        }
    }

    protected function doHttpRenderError(MvcEvent $e) {
        // must be an setEnvironmentVariableerror
        $response = $e->getResponse();
        // if we have a JsonModel in the result, then do nothing
        $currentModel = $e->getResult();
        if ($currentModel instanceof JsonModel) {
            return;
        }

        $jsonModel = new JsonModel();

        // make debugging easier if we're using xdebug!
        ini_set('html_errors', 0);

        if (!$e->isError()) {
            $jsonModel->setVariable('success', true);
        } else {
            $jsonModel->setVariable("title", $response->getReasonPhrase());
            $jsonModel->setVariable("httpStatus", $response->getStatusCode());
            // Find out what the error is
            $exception = $currentModel->getVariable('exception');
            if ($currentModel instanceof ModelInterface && $currentModel->reason) {
                switch ($currentModel->reason) {
                    case 'error-controller-cannot-dispatch':
                        $jsonModel->detail = 'The requested controller was unable to dispatch the request.';
                        break;
                    case 'error-controller-not-found':
                        $jsonModel->detail = 'The requested controller could not be mapped to an existing controller class.';
                        break;
                    case 'error-controller-invalid':
                        $jsonModel->detail = 'The requested controller was not dispatchable.';
                        break;
                    case 'error-router-no-match':
                        $jsonModel->detail = 'The requested URL could not be matched by routing.';
                        break;
                    default:
                        $jsonModel->detail = $currentModel->message;
                        break;
                }
            }
            if ($exception) {
                if ($exception->getCode()) {
                    $e->getResponse()->setStatusCode($exception->getCode());
                }
                $jsonModel->detail = $exception->getMessage();

                // find the previous exceptions
                $messages = array();
                while ($exception = $exception->getPrevious()) {
                    $messages[] = "* " . $exception->getMessage();
                };
                if (count($messages)) {
                    $exceptionString = implode("n", $messages);
                    $jsonModel->messages = $exceptionString;
                }
            }
        }

        $jsonModel->setTerminal(true);
        $e->setResult($jsonModel);
        $e->setViewModel($jsonModel);
    }

    protected function logRequest(MvcEvent $e) {
        $serviceManager = $e->getApplication()->getServiceManager();
        $request = $e->getRequest();
        $authService = $serviceManager->get('Auth');
        $identity = $authService->getIdentity();
        $idUser = $identity ? $identity['idUser'] : 'guest';

        $queryString = $request->getQuery()->toString();
        $postParams = $request->getPost()->toString();
        $headers = $request->getHeaders()->toString();
        $content = $request->getContent();
        $clientIp = $request->getServer('REMOTE_ADDR');
        $logMessage = implode(' ', [
            $idUser,
            $clientIp,
            $request->getMethod(),
            $request->getUriString(),
            $headers ? base64_encode($headers) : 'X',
            $content ? base64_encode($content) : 'X',
            $queryString ? base64_encode($queryString) : 'X',
            $postParams ? base64_encode($postParams) : 'X',
        ]);
        $logger = $serviceManager->get('myLoggerr');
        if (!$request->isOptions()) {
            $logger->info($logMessage);
        }
    }

}
