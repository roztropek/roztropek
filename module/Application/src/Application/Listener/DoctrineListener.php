<?php

namespace Application\Listener;

use Roztropek\Db\Entity\Interfaces\Versionable;
use Doctrine\DBAL\LockMode;
use Roztropek\Db\Exception\OptimisticLockException;

class DoctrineListener {

    private $sm;

    public function __construct($sm) {
        $this->sm = $sm;
    }

    public function onFlush($eventArgs) {
        $entityManager = $eventArgs->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();

        foreach ($unitOfWork->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Versionable && $entity->isLocked()) {
                $entityManager->lock($entity, LockMode::OPTIMISTIC, $entity->getVersion());
                /*narazie zostawiam tak*/
//                $query = sprintf("SELECT e.version FROM %s e WHERE e.id = '%s'", get_class($entity), $entity->getId());
//                $result = $entityManager->createQuery($query)->getSingleScalarResult();
//                if($result != $entity->getVersion()) {
//                    throw new OptimisticLockException(sprintf('wersje sie nie zgadzaja'));
//                }
            }
        }
    }

}
