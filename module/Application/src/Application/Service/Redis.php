<?php

namespace Application\Service;

use Predis\Client;
use Roztropek\Service\Base;

class Redis extends Base {

    protected $client = null;

    public function getClient() {
        if (!$this->client) {
            $this->setClient(new Client());
        }

        return $this->client;
    }

    protected function setClient(Client $client) {
        $this->client = $client;
    }

}

?>
