<?php

namespace Application\Service;

use Roztropek\Service\Base;
use Roztropek\Email\Transport;
use Roztropek\Email\Message;
use Roztropek\Email\Template;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;

class Mailer extends Base {

    public function send($type, $options = [], $data = []) {
        $config = $this->getService('Config');
        $transportConfig = $config['sendMail']['no-reply'];
        $data['applicationUrl'] = $config['frontend']['url'];
        $message = $this->createMessage($type, $options);

        $transport = Transport::getTransport($transportConfig);
        $body = Template::getTemplate($type, $data);
        $htmlPart = new MimePart($body);
        $htmlPart->type = "text/html";
        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts([$htmlPart]);
        $message->setBody($mimeMessage);
        return $transport->send($message);
    }

    protected function createMessage($type, $options = []) {
        $message = null;
        switch ($type) {
            case 'register':
                $options['subject'] = 'Dziękujemy za rejestrację';
                break;
            case 'contact':
                $options['subject'] = 'Nowe zgłoszenie';
                break;
            case 'resetPasswordRequest':
                $options['subject'] = 'Reset hasła';
                break;
            default:
                throw new \InvalidArgumentException('nie znalazlem typu ' . $type);
        }
        $message = Message::create($options);
        return $message;
    }

}
