<?php

namespace Application\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Utils\Helper\ArrayHelper;

class ContactController extends ApiController {

    public function usAction() {
        $data = $this->getFilteredData(['subject', 'email', 'text', 'name']);
        $config = $this->getService('Config');
        $configEmail = $config['email'] ? : [];
        $contactEmail = ArrayHelper::getKey('contact', $configEmail, '', 'string');
        $emailTo = ArrayHelper::getKey('email', $data, '', 'string');

        if (!$contactEmail) {
            throw new \Exception('konfiguracja maila nie jest zdefiniowana');
        }

        if ($emailTo && $contactEmail) {
            $this->getService('Mailer')->send('contact', [
                'to' => $contactEmail,
                'from' => 'kontakt@roztropek.pl',
                'replyTo' => $emailTo,
                'fromName' => 'kontakt'
                    ], $data);
        } else {
            new JsonModel(['success' => true, 'messages' => 'Pole email nie może być puste']);
        }
        return new JsonModel(['success' => true]);
    }

}
