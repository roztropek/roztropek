<?php

namespace Payment;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'payment' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/payment',
                    'defaults' => array(
                        '__NAMESPACE__' => __NAMESPACE__ . '\Controller',
                        'controller' => __NAMESPACE__,
                        'action' => 'list',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'action' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/:controller[/:action[/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z_-]*',
                                'action' => '[a-zA-Z][a-zA-Z_-]*',
                                'id' => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
//            __NAMESPACE__.'\Controller\\'.__NAMESPACE__ => __NAMESPACE__.'\Controller\\'.__NAMESPACE__.'Controller'
            'Payment\Controller\Payment' => 'Payment\Controller\PaymentController',
            'Payment\Controller\Payout' => 'Payment\Controller\PayoutController',
            'Payment\Controller\Payin' => 'Payment\Controller\PayinController'
        ),
    ),
    'view_manager' => array(
        'view_manager' => array(
//        'template_path_stack' => array(
//            'question' => __DIR__ . '/../view'
//        ),
            'strategies' => array(
                'ViewJsonStrategy',
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Transaction' => 'Payment\Service\Transaction',
            'Payment' => 'Payment\Service\Payment',
            'PayIn' => 'Payment\Service\PayIn',
            'PayOut' => 'Payment\Service\PayOut',
            'Tpay' => 'Payment\Service\Tpay',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_orm_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_orm_driver'
                )
            )
        )
    )
);
