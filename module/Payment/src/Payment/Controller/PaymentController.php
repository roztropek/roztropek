<?php

namespace Payment\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Utils\Helper\ArrayHelper;
use Payment\Factory\Money as MoneyFactory;
use Payment\Extractor\Payment as PaymentExtractor;

class PaymentController extends ApiController {

    public function getListAction() {
        $params = $this->params()->fromQuery();
        $payments = $this->getService('Payment')->getList($params);
        $result = [];
        foreach ($payments as $paymentEntity) {
            $result[] = PaymentExtractor::extract($paymentEntity);
        }
        return new JsonModel(array('success' => true, 'result' => $result, 'totalCount' => $payments->count()));
    }

}
