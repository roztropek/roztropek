<?php

namespace Payment\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Exception\ActionFailedException;

class PayinController extends ApiController {

    public function createAction() {
        $data = $this->getFilteredData(['value']);
        if($data['value'] < 500) {
            throw new ActionFailedException('Wpłata nie może byś mniejsza niż 5zł');
        }
        $data['user'] = $this->getService('Auth')->getAuthenticatedUser();
        $paymentEntity = $this->getService('PayIn')->create($data);
        $paymentService = $this->getService('Tpay');
        $paymentData = $paymentService->createPaymentData($paymentEntity);
        $urlStringArray = [];
        foreach ($paymentData as $param => $value) {
            $urlStringArray[] = $param . '=' . urlencode($value);
        }

        return $this->finalize(new JsonModel([
                    'success' => true,
                    'url' => $paymentService::PAYMENT_URL,
                    'urlGET' => $paymentService::PAYMENT_URL . '?' . join('&', $urlStringArray),
                    'data' => $paymentData
        ]));
    }

    public function tpayAction() {
        $data = $this->params()->fromPost();
        $this->getService('Tpay')->processStatus($data);
        $this->finalize([]);
        echo 'TRUE';
        exit;
    }

}
