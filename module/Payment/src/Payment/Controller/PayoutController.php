<?php

namespace Payment\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Utils\Helper\ArrayHelper;
use Payment\Factory\Money as MoneyFactory;
use Payment\Extractor\Payment as PaymentExtractor;
use Roztropek\Exception\ActionFailedException;

class PayoutController extends ApiController {

    public function orderAction() {
        $data = $this->getFilteredData(['value', 'account']);
        $value = ArrayHelper::getKey('value', $data, -1, 'natural');
        if ($value < 2500) {
            throw new ActionFailedException('Minimalnie można wypłacić 25zł');
        }
        $user = $this->getService('Auth')->getFreshIdentity();
        $playCoins = MoneyFactory::create([
                    'currency' => MoneyFactory::PLAY_COIN,
                    'value' => $value
        ]);

        $account = $this->getService('Account')->find($data['account']);
        if (!$account) {
            throw new ActionFailedException('Nie znalazłem takiego konta');
        }
        $this->getService('PayOut')->settlePayout($user, $playCoins, $account);

        $response = new JsonModel(array('success' => true, 'messages' => []));
        return $this->finalize($response);
    }

    public function acceptAction() {
        $data = $this->getFilteredData(['id']);
        $id = ArrayHelper::getKey('id', $data, null, 'string');
        if (!$id) {
            throw new ActionFailedException('brak id');
        }
        $this->getService('PayOut')->acceptPayout($id);

        $response = new JsonModel(array('success' => true, 'messages' => []));
        return $this->finalize($response);
    }

    public function getListAction() {
        $params = $this->params()->fromQuery();
        $payments = $this->getService('Payment')->getList($params);
        $result = [];
        foreach ($payments as $paymentEntity) {
            $result[] = PaymentExtractor::extract($paymentEntity);
        }
        return new JsonModel(array('success' => true, 'result' => $result, 'totalCount' => $payments->count()));
    }
}
