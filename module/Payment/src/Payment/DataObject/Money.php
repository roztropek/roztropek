<?php

namespace Payment\DataObject;

use Roztropek\DataObject\Base;

class Money extends Base {

    protected $propertiesConfig = [
        'value' => [
            'required' => true,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 0,
                        'inclusive' => true
                    ]]
            ]
        ],
        'currency' => [
            'required' => true,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => ['activecoin', 'gold', 'playcoin', 'ticket']
                    ]]
            ],
            'filters' => [
                ['name' => 'StringToLower',
                    'options' => [
                        'encoding' => 'UTF-8'
                    ]]
            ]
        ],
    ];

    protected function createInputFilterConfig() {
        $inputConfig = parent::createInputFilterConfig();
        return array_replace($inputConfig, $this->propertiesConfig);
    }

}
