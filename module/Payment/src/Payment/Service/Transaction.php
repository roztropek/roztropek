<?php

namespace Payment\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Payment\ValueObject\Money\Money;
use Tournament\Entity\Tournament;
use User\Entity\User;
use Payment\Factory\Transaction as TransactionFactory;
use \Payment\Entity\Transaction as TransactionEntity;

class Transaction extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Payment\Entity\Transaction';

    public function recharge(User $user, Money $money, $description, $reference = null, $referenceType = null) {
        $entityClass = $this->getEntityClass();
        $transactionEntity = $this->create([
                    'money' => $money,
                    'user' => $user,
                    'description' => $description,
                    'reference' => $reference,
                    'referenceType' => $referenceType,
                    'type' => TransactionEntity::CREDIT
        ]);
        
        $user->recharge($money);
        return $transactionEntity;
    }

    public function pay(User $user, Money $money, $description, $reference, $referenceType) {
        $transactionEntity = $this->create([
                    'money' => $money,
                    'user' => $user,
                    'description' => $description,
                    'reference' => $reference,
                    'referenceType' => $referenceType,
                    'type' => TransactionEntity::DEBIT
        ]);
        
        $user->pay($money);
        return $transactionEntity;
    }

    public function payForJoinTournament(User $user, Tournament $tournament) {
        if ($this->canAffordForPayForJoinTournament($user, $tournament)) {
            $price = $tournament->getPrice($user);
            $transactionEntity = $this->pay($user, $price, 'joinTournament', $tournament->getId(), 'Tournament');
            return $transactionEntity;
        }
    }

    public function canAffordForPayForJoinTournament(User $user, Tournament $tournament) {
        $tournamentPrice = $tournament->getPrice($user);
        return $user->hasEnoughMoney($tournamentPrice);
    }
    
    public function create(array $data) {
        $entityClass = $this->getEntityClass();
        $entity = new $entityClass($data);
        $this->getEntityManager()->persist($entity);
        return $entity;
    }

}

?>
