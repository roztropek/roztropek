<?php

namespace Payment\Service;

use Roztropek\Service\Base;
use User\Entity\User as UserEntity;
use Payment\Entity\PayIn as PayInEntity;
use Payment\Extractor\PayIn as PayInExtractor;

class Tpay extends Base {

    const CUSTOMER_ID = '25371';
    const PAYMENT_URL = 'https://secure.tpay.com';
    const SECRET = '83b87332f04cffa82a861f4';

    public function createPaymentData(PayInEntity $payInEntity) {
        $data = PayInExtractor::extract($payInEntity);
        return [
            'id' => self::CUSTOMER_ID,
            'kwota' => $data['value'] / 100,
            'crc' => $data['id'],
            'opis' => 'Opis',
            'md5sum' => $this->createCheckSum($data)
        ];
    }

    public function processStatus(array $data) {
        $paymentId = $data['tr_crc'];
        $paymentEntity = $this->getService('PayIn')->get($paymentId);
        if ($paymentEntity && $this->verifyIncomingChecksum($data)) {
            $paymentEntity->setDescription($data['tr_id']);
            if (strtolower($data['tr_status']) == 'true') {
                if (!$paymentEntity->isFinished()) {
                    $paymentEntity->setFinished();
                    $this->getService('PayIn')->doPay($paymentEntity);
                }
            } else {
                $paymentEntity->setErrored();
            }
        }
    }

    protected function createCheckSum(array $data) {
        $decValue = $data['value'] / 100;
        return md5(self::CUSTOMER_ID . $decValue . $data['id'] . self::SECRET);
    }

    protected function verifyIncomingChecksum(array $data) {
        $md5Sum = md5(self::CUSTOMER_ID . $data['tr_id'] . $data['tr_amount'] . $data['tr_crc'] . self::SECRET);
        return $data['md5sum'] == $md5Sum;
    }

}

?>
