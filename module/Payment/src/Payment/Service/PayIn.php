<?php

namespace Payment\Service;

use Roztropek\Service\Base;
use Payment\ValueObject\Money\PlayCoin;
use Payment\Entity\PayIn as PayInEntity;

class PayIn extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Payment\Entity\PayIn';

    /**
     * @param array $data
     * @return \Payment\Entity\PayIn
     */
    public function create(array $data) {
        $class = $this->getEntityClass();
        $payinEntity = new $class($data);
        $this->getEntityManager()->persist($payinEntity);
        return $payinEntity;
    }

    public function doPay(PayInEntity $payInEntity) {
        $money = new PlayCoin($payInEntity->getValue());
        $this->getService('Transaction')->recharge($payInEntity->getUser(), $money, $payInEntity->getDescription(), $payInEntity->getId(), 'payIn');
    }

    public function get($id) {
        return $this->getEntityRepository()->find($id);
    }

}
