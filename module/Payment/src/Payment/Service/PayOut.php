<?php

namespace Payment\Service;

use Roztropek\Service\Base;
use Payment\ValueObject\Money\PlayCoin;
use User\Entity\User as UserEntity;
use Payment\Entity\PayOut as PayOutEntity;
use Payment\Factory\Money as MoneyFactory;
use Roztropek\Exception\ActionFailedException;

class PayOut extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Payment\Entity\PayOut';

    public function settlePayout(UserEntity $user, PlayCoin $money) {
        //do przerobienia
        if (!$user->hasEnoughMoney($money)) {
            throw new ActionFailedException('nie masz wystarczająco dużo środków do zlecenia wypłaty');
        }

        if ($account->getUser()->getId() !== $user->getId()) {
            throw new ActionFailedException('nie masz takiego numeru konta');
        }

        $payoutEntity = $this->create([
            'user' => $user,
            'value' => $money->getValue(),
        ]);

        return $payoutEntity;
    }

    public function acceptPayout($payout) {
        if (is_string($payout)) {
            $payoutEntity = $this->getEntityRepository()->find($payout);
        } else if ($payout instanceof PayOutEntity) {
            $payoutEntity = $payout;
        }
        $result = $payout->accept();
        if ($result === 1) {
            $money = MoneyFactory::create([
                        'currency' => MoneyFactory::PLAY_COIN,
                        'value' => $payout->getValue()
            ]);
            $this->getService('Transaction')->pay(
                    $payout->getUser(), $money, 'payout', $payout->getId(), 'PayOut'
            );
            return true;
        }
        return false;
    }

    /**
     * @param array $data
     * @return \Payment\Entity\PayOut
     */
    protected function create(array $data) {
        $class = $this->getEntityClass();
        $payoutEntity = new $class($data);
        $this->getEntityManager()->persist($payoutEntity);
        return $payoutEntity;
    }

}

?>
