<?php

namespace Payment\Service;

use Roztropek\Service\Base;
use User\Entity\User as UserEntity;

class Payment extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Payment\Entity\Payment';

    public function getList(array $params = []) {
        $identity = $this->getService('Auth')->getIdentity();

        if ($identity && $identity['role'] === 'player') {
            $params['user'] = $identity['idUser'];
        }
        return $this->getEntityRepository()->getList($params);
    }

}

?>
