<?php

namespace Payment\Extractor;

use Roztropek\Utils\Object\Object;
use Payment\Entity\PayIn as PayInEntity;

class PayIn extends Payment {

    /**
     * @param \Payment\Entity\PayIn $payInEntity
     * @return array
     */
    public static function extract(PayInEntity $payInEntity) {
        $data = parent::extractPayment($payInEntity);
        $data['type'] = 'payin';
        $data['status'] = self::statusAsString($payInEntity->getStatus());
        return $data;
    }
    
    protected static function statusAsString($status) {
        switch($status) {
            case PayInEntity::STATUS_CREATED:
                return 'created';
                break;
            case PayInEntity::STATUS_PENDING:
                return 'pending';
                break;
            case PayInEntity::STATUS_FINISHED:
                return 'finished';
                break;
            case PayInEntity::STATUS_ERROR:
                return 'errored';
                break;
            default:
                return 'unknown';
                break;
        }
    }

}
