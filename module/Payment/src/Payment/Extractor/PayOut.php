<?php

namespace Payment\Extractor;

use Roztropek\Utils\Object\Object;
use Payment\Entity\PayOut as PayOutEntity;

class PayOut extends Payment {

    /**
     * @param \Payment\Entity\PayOut $payOutEntity
     * @return array
     */
    public static function extract(PayOutEntity $payOutEntity) {
        $data = parent::extractPayment($payOutEntity);
        $data['type'] = 'payout';
        $data['accountNumber'] = $payOutEntity->getAccount()->getNumber();
        $data['status'] = self::statusAsString($payOutEntity->getStatus());
        return $data;
    }

    protected static function statusAsString($status) {
        switch ($status) {
            case PayOutEntity::STATUS_ORDERED:
                return 'ordered';
                break;
            case PayOutEntity::STATUS_ACCEPTED:
                return 'accepted';
                break;
            case PayOutEntity::STATUS_DECLINED:
                return 'declined';
                break;
            default:
                return 'unknown';
                break;
        }
    }

}
