<?php

namespace Payment\Extractor;

use Roztropek\Utils\Object\Object;
use Payment\Entity\Payment as PaymentEntity;
use User\Extractor\User as UserExtractor;

class Payment {

    /**
     * @param PaymentEntity $paymentEntity
     * @return array
     */
    public static function extract(PaymentEntity $paymentEntity) {
        $extractorClass = static::getExtractorClass($paymentEntity);
        return $extractorClass::extract($paymentEntity);
    }

    protected static function getExtractorClass(PaymentEntity $paymentEntity) {
        $classname = Object::getShortClassName($paymentEntity);
        return sprintf('\Payment\Extractor\%s', $classname);
    }

    protected static function extractPayment(PaymentEntity $paymentEntity) {
        $user = $paymentEntity->getUser();
        $extractedUser = $user ? UserExtractor::extract($user) : null;
        $extractedPayment = [
            'id' => $paymentEntity->getId(),
            'date' => $paymentEntity->getCreatedAt(),
            'description' => $paymentEntity->getDescription(),
            'value' => $paymentEntity->getValue(),
            'user' => $extractedUser
        ];
        
        return $extractedPayment;
    }

}
