<?php

namespace Payment\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Doctrine\ORM\AbstractQuery;
use Roztropek\Utils\Helper\ArrayHelper;
use Roztropek\Utils\Helper\ValueHelper;

class TransactionRepository extends BaseRepository {

    public function getWinnersRanking($type) {
        switch ($type) {
            case 'last30days':
                $dateString = '-30 days';
                break;
            case 'lastWeek':
                $dateString = '-7 days';
                break;
            case 'daily':
                $dateString = 'midnight';
                break;
            case 'weekly':
                $dateString = 'last monday';
                break;
            case 'monthly' :
            default:
                $dateString = 'first day of this month';
                break;
        }
        $date = date('Y-m-d', strtotime($dateString));
        $dql = "SELECT SUM (t.value) as value, u.id as userId, u.avatar, u.nick
            FROM \Payment\Entity\Transaction t
            JOIN t.user u
            WHERE t.description = 'winInTournament'
            AND t.createdAt > :startDate
            GROUP BY u.id
            ORDER BY value DESC
            ";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('startDate', $date);
        return $query->getScalarResult();
    }

}
