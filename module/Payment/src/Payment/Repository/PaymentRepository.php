<?php

namespace Payment\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Doctrine\ORM\AbstractQuery;
use Roztropek\Utils\Helper\ArrayHelper;
use Roztropek\Utils\Helper\ValueHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Payment\Entity\Payment as PaymentEntity;

class PaymentRepository extends BaseRepository {

    public function getList(array $params = []) {
        $user = ArrayHelper::getKey('user', $params, null, 'string');
        $page = ArrayHelper::getKey('page', $params, 0, 'natural');
        $pageSize = ArrayHelper::getKey('limit', $params, 20, 'natural');

        if ($page < 1) {
            $page = 1;
        }
        if ($pageSize > 20) {
            $pageSize = 20;
        }

        $qb = $this->createQueryBuilder('p')
                ->setMaxResults($pageSize)
                ->setFirstResult($pageSize * ($page-1));

        if ($user) {
            $qb->andWhere('p.user = :user');
            $qb->andWhere('(p INSTANCE OF :paymentTypePayIn AND p.status > 0) OR (p INSTANCE OF :paymentTypePayout)');
            $qb->setParameter('user', $user);
            $qb->setParameter('paymentTypePayIn', PaymentEntity::PAY_IN);
            $qb->setParameter('paymentTypePayout', PaymentEntity::PAY_OUT);
        }

        $qb->orderBy('p.createdAt', 'DESC');

        return new Paginator($qb);
    }

}
