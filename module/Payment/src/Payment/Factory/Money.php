<?php

namespace Payment\Factory;

use Payment\DataObject\Money as MoneyDataObject;

final class Money {
    const ACTIVE_COIN = 'activecoin';
    const PLAY_COIN = 'playcoin';
    const TICKET = 'ticket';
    const GOLD = 'gold';
    
    /**
     * @param array $data currency, value
     * @return \Payment\ValueObject\Money
     */
    public static function create(array $data) {
        $moneyDataObject = new MoneyDataObject($data);
        $filteredValues = $moneyDataObject->getData();
        $currency = $filteredValues['currency'];
        $value = $filteredValues['value'];
        $moneyClass = self::getMoneyClassNameForCurrency($currency);
        return new $moneyClass($value);
    }
    
    protected static function getMoneyClassNameForCurrency($currency){
        switch($currency){
            case self::ACTIVE_COIN:
                return '\Payment\ValueObject\Money\ActiveCoin';
                break;
            case self::PLAY_COIN:
                return '\Payment\ValueObject\Money\PlayCoin';
                break;
            case self::TICKET:
                return '\Payment\ValueObject\Money\Ticket';
                break;
            case self::GOLD:
                return '\Payment\ValueObject\Money\Gold';
                break;
            default:
                throw new \InvalidArgumentException('Zły rodzaj waluty');
                break;
        }
    }
}
