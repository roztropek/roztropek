<?php

namespace Payment\Exchanger;

use Roztropek\Exchanger\Base;
use Payment\Entity\Transaction as TransactionEntity;

class Transaction extends Base {

    private $inputFilterConfig = [
        'money' => [
            'required' => true,
            'validators' => [
                [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\Payment\ValueObject\Money\Money'
                    ]
                ]
            ]
        ],
        'user' => [
            'required' => true,
            'validators' => [
                [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\User\Entity\User'
                    ]
                ]
            ]
        ],
        'description' => [
            'required' => false,
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'reference' => [
            'required' => false
        ],
        'referenceType' => [
            'required' => false
        ],
        'type' => [
            'required' => true,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => [TransactionEntity::CREDIT, TransactionEntity::DEBIT],
                        'strict' => true
                    ]]
            ]
        ]
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
