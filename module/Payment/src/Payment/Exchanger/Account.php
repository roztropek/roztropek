<?php

namespace Payment\Exchanger;

use Roztropek\Exchanger\Base;

class Account extends Base {

    private $inputFilterConfig = [
        'user' => [
            'required' => true,
            'validators' => [
                ['name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\User\Entity\User'
                    ]]
            ]
        ],
        'number' => [
            'required' => true,
            'validators' => [
                ['name' => 'stringLength',
                    'options' => [
                        'min' => 10
                    ]]
            ]
        ]
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
