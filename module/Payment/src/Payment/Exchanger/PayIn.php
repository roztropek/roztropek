<?php

namespace Payment\Exchanger;

use Roztropek\Exchanger\Base;

class PayIn extends Base {

    private $inputFilterConfig = [
     'value' => [
            'required' => true,
            'validators' => [
                ['name' => 'greaterThan',
                    'options' => [
                        'min' => 1000,
                        'message' => 'Minimalna wartość wpłaty to 10 zł'
                    ]
                ]
            ]
        ],
        'user' => [
            'required' => true,
            'validators' => [
                [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\User\Entity\User'
                    ]
                ]
            ]
        ],
        'description' => [
            'required' => false,
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ]
    ];
    
    public function getInputFilterConfig() 
    {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
