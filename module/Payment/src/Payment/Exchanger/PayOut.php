<?php

namespace Payment\Exchanger;

use Roztropek\Exchanger\Base;

class PayOut extends Base {
    private $inputFilterConfig = [
         'value' => [
            'required' => true,
            'validators' => [
                ['name' => 'greaterThan',
                    'options' => [
                        'min' => 5000,
                        'message' => 'Minimalna wartość wypłaty to 50 zł'
                    ]
                ]
            ]
        ],
        'user' => [
            'required' => true,
            'validators' => [
                [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\User\Entity\User'
                    ]
                ]
            ]
        ],
        'description' => [
            'required' => false,
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'account' => [
            'required' => true
        ],
        
    ];
    
    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }
}