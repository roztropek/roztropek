<?php

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;
use Payment\DataObject\Transaction as TransactionDataObject;
use Payment\ValueObject\Money\Money;
use User\Entity\User;
use Roztropek\Db\Entity\Traits\Creatable;

/**
 * @ORM\Entity(repositoryClass="Payment\Repository\TransactionRepository")
 * @ORM\Table(name="Transaction")
 * @ORM\HasLifecycleCallbacks
 * */
class Transaction extends Base {

    use Creatable;

    const DEBIT = 'debit';
    const CREDIT = 'credit';
    const TRANSFER = 'transfer';

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="integer") */
    protected $value;

    /** @ORM\Column(type="string", length=20) */
    protected $currency;

    /** @ORM\Column(type="string") */
    protected $description;

    /** @ORM\Column(type="string", length=40) */
    protected $reference = '';

    /** @ORM\Column(type="string", length=40) */
    protected $referenceType = '';

    /** @ORM\Column(type="string", length=40) */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    protected $user;
    private $settersMapping = [
        'money' => 'setMoney',
        'description' => 'setDescription',
        'reference' => 'setReference',
        'referenceType' => 'setReferenceType',
        'user' => 'setUser',
        'type' => 'setType'
    ];

    public function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function getId() {
        return $this->id();
    }

    public function getValue() {
        return $this->value;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getReference() {
        return $this->reference;
    }

    public function getUser() {
        return $this->user;
    }

    public function getReferenceType() {
        return $this->referenceType;
    }

    protected function setReferenceType($referenceType) {
        $this->referenceType = $referenceType;
    }

    protected function setValue($value) {
        $this->value = $value;
    }

    protected function setCurrency($currency) {
        $this->currency = $currency;
    }

    protected function setDescription($description) {
        $this->description = $description;
    }

    protected function setReference($reference) {
        $this->reference = $reference;
    }

    protected function setUser(User $user) {
        $this->user = $user;
    }

    protected function setMoney(Money $money) {
        $this->setCurrency($money->getCurrency());
        $this->setValue($money->getValue());
    }

    protected function setType($type) {
        $this->type = $type;
    }

    protected function setUp(array $data) {
        parent::setUp($data);
        $this->runSetters($this->settersMapping, $data);
    }

}
