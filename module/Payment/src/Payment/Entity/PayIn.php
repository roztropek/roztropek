<?php

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Payment\Repository\PayInRepository")
 * */
class PayIn extends Payment {

    const STATUS_CREATED = 0;
    const STATUS_PENDING = 1;
    const STATUS_FINISHED = 2;
    const STATUS_ERROR = 9;

    private $settersMapping = [];

    public function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function setFinished() {
        $this->setStatus(self::STATUS_FINISHED);
    }

    public function setPending() {
        $this->setStatus(self::STATUS_PENDING);
    }

    public function setErrored() {
        $this->setStatus(self::STATUS_ERROR);
    }

    public function isFinished() {
        return $this->getStatus() === self::STATUS_FINISHED;
    }
}
