<?php

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payment\Entity\Account as AccountEntity;
use \Roztropek\Db\Entity\Traits\Creatable;

/**
 * @ORM\Entity(repositoryClass="Payment\Repository\PayOutRepository")
 * */
class PayOut extends Payment {

    use Creatable;

    const STATUS_ORDERED = 0;
    const STATUS_DECLINED = 2;
    const STATUS_ACCEPTED = 1;

    /**
     * @ORM\Column(type="string")
     */
    protected $account;
    private $settersMapping = [
        'account' => 'setAccount'
    ];

    public function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    /**
     * @return string
     */
    public function getAccount():string {
        return $this->account ?: '';
    }

    /**
     * @param string $account
     */
    protected function setAccount(string $account) {
        $this->account = $account;
    }

    public function accept() {
        if ($this->getStatus() === self::STATUS_ACCEPTED) {
            return 0;
        }
        $this->setStatus(self::STATUS_ACCEPTED);
        return 1;
    }

    public function decline() {
        if ($this->getStatus() === self::STATUS_DECLINED) {
            return 0;
        } else if ($this->getStatus() === self::STATUS_ACCEPTED) {
            return -1;
        } else {
            $this->setStatus(self::STATUS_DECLINED);
            return 1;
        }
    }

}
