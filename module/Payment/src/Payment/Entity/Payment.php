<?php

namespace Payment\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;
use User\Entity\User;
use Roztropek\Db\Entity\Traits\Creatable;
use Payment\Exchanger\Payment as PaymentExchanger;

/**
 * @ORM\Entity(repositoryClass="Payment\Repository\PaymentRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"payIn" = "PayIn", "payOut" = "PayOut"})
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Payment")
 */
abstract class Payment extends Base {

    use Creatable;

    const PAY_IN = 'payIn';
    const PAY_OUT = 'payOut';

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="integer") */
    protected $value;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    protected $user;

    /** @ORM\Column(type="string", nullable=true) */
    protected $description;
    
    /** @ORM\Column(type="integer") */
    protected $status = 0;
    
    private $settersMapping = [
        'value' => 'setValue',
        'user' => 'setUser',
        'description' => 'setDescription'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function getId() {
        return $this->id;
    }

    public function getValue() {
        return $this->value;
    }

    public function getUser() {
        return $this->user;
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function getDescription() {
        return $this->description;
    }

    protected function setValue($value) {
        $this->value = $value;
    }

    protected function setType($type) {
        $this->type = $type;
    }

    protected function setUser(User $user) {
        $this->user = $user;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    
    protected function setStatus($status) {
        $this->status = $status;
    }
}
