<?php

namespace Payment\ValueObject\Money;

abstract class Money {

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var int
     */
    protected $value;

    public function __construct($value) {
        $this->setValue($value);
        if (!$this->getCurrency()) {
            throw new \Exception('waluta musi być zdefiniowana');
        }
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     * @return \Payment\ValueObject\Money\Money
     * @throws \InvalidArgumentException
     */
    public function add(Money $money) {
        $this->checkCurrency($money);
        $value = $money->getValue() + $this->getValue();
        return new static($value);
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     * @return \Payment\ValueObject\Money\Money
     * @throws \InvalidArgumentException
     */
    public function substract(Money $money) {
        $this->checkCurrency($money);
        $value = $this->getValue() - $money->getValue();
        return new static($value);
    }

    /**
     * @param float $multipler
     * @return \Payment\ValueObject\Money\Money
     */
    public function multiply($multipler) {
        return new static($this->getValue() * $multipler);
    }

    /**
     * @param float $divider
     * @return \Payment\ValueObject\Money\Money
     */
    public function divide($divider) {
        return new static($this->getValue() / $divider);
    }

    /**
     * @return int
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     * @return int
     */
    public function compareWith(Money $money) {
        $this->checkCurrency($money);
        if ($this->getValue() > $money->getValue()) {
            return 1;
        }

        if ($this->getValue() < $money->getValue()) {
            return -1;
        }

        if ($this->getValue() == $money->getValue()) {
            return 0;
        }
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     * @return boolean
     */
    public function greaterThan(Money $money) {
        $this->checkCurrency($money);
        return $this->getValue() > $money->getValue();
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     * @return boolean
     */
    public function lessThan(Money $money) {
        $this->checkCurrency($money);
        return $this->getValue() < $money->getValue();
    }

    /**
     * @return array
     */
    public function extract() {
        return [
            'currency' => $this->getCurrency(),
            'value' => $this->getValue()
        ];
    }
    
    public function toArray() {
        return $this->extract();
    }

    protected function setValue($value) {
        $this->value = $value;
    }

    protected function checkCurrency(Money $money) {
        if ($money->getCurrency() !== $this->getCurrency()) {
            throw new \InvalidArgumentException('nie zgadza się waluta!');
        }
    }
}
