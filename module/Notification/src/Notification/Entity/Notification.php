<?php

namespace Notification\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;
use User\Entity\User;
use Roztropek\Db\Entity\Traits\Creatable;

/**
 * @ORM\Entity(repositoryClass="Notification\Repository\NotificationRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Notification")
 */
class Notification extends Base {

    use Creatable;
    
    const TOURUNAMENT_PLAY = 'playTournament';
    const TOURNAMENT_END = 'endTournament';

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="boolean") */
    protected $isReaded = false;

    /**
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id", nullable=false)
     */
    protected $user;

    /** @ORM\Column(type="string", nullable=false, length=2048) */
    protected $data;
    
    /** @ORM\Column(type="string", nullable=false) */
    protected $event;
    
    private $settersMapping = [
        'user' => 'setUser',
        'data' => 'setData',
        'event' => 'setEvent'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    protected function getId() {
        return $this->id;
    }

    public function isReaded() {
        return $this->isReaded;
    }
    
    public function markAsReaded() {
        $this->isReaded = true;
    }
    
    public function markAsUnreaded() {
        $this->isReaded = false;
    }

    public function getUser() {
        return $this->user;
    }

    public function getData() {
        return json_decode($this->data, true);
    }

    public function getEvent() {
        return $this->event;
    }
    
    protected function setId($id) {
        $this->id = $id;
    }

    protected function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    protected function setUser($user) {
        $this->user = $user;
    }

    protected function setEvent($event) {
        $this->event = $event;
    }
    
    protected function setData(array $data) {
        $this->data = json_encode($data, JSON_UNESCAPED_UNICODE);
    }

}
