<?php

namespace Notification\Model;

use Roztropek\Db\Entity\Base;

class Message extends Base {

    protected $body;
    protected $destination;
    protected $code;
    private $settersMapping = [
        'body' => 'setBody',
        'destination' => 'setDestination',
        'code' => 'setCode'
    ];

    protected function getExchangerClass() {
        return '\Notification\Exchanger\Message';
    }

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function getBody() {
        return $this->body;
    }

    public function getDestination() {
        return $this->destination;
    }

    public function getCode() {
        return $this->code;
    }

    protected function setBody($body) {
        $this->body = $body;
    }

    protected function setDestination($destination) {
        $this->destination = $destination;
    }

    protected function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'code' => $this->getCode(),
            'body' => $this->getBody(),
            'destination' => $this->getDestination()
        ];
    }

    /**
     * @return string
     */
    public function __toString() {
        return json_encode($this->toArray());
    }

}
