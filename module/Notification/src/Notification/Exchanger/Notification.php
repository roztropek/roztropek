<?php

namespace Notification\Exchanger;

use Roztropek\Exchanger\Base;

class Notification extends Base {

    private $inputFilterConfig = [
        'user' => [
            'required' => true
        ],
        'data' => [
            'required' => true
        ],
        'event' => [
            'required' => true
        ]
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
