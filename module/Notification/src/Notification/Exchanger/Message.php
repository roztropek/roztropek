<?php

namespace Notification\Exchanger;

use Roztropek\Exchanger\Base;

class Message extends Base {

    private $inputFilterConfig = [
        'body' => [
            'required' => true
        ],
        'destination' => [
            'required' => true
        ],
        'code' => [
            'required' => true
        ]
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
