<?php

namespace Notification\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Roztropek\Utils\Helper\ArrayHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;

class NotificationRepository extends BaseRepository {

    public function getList(array $params = []) {
        $user = ArrayHelper::getKey('user', $params, null, 'string');
        $offset = ArrayHelper::getKey('offset', $params, 0, 'natural');

        $qb = $this->createQueryBuilder('n')
                ->setMaxResults(20)
                ->setFirstResult($offset);

        if ($user) {
            $qb->andWhere('n.user = :user');
            $qb->setParameter('user', $user);
        }

        $qb->orderBy('n.createdAt', 'DESC');

        return new Paginator($qb);
    }

    public function getUnreadedCount($user) {
        return $this->createQueryBuilder('n')
                        ->select('COUNT(n.id)')
                        ->andWhere('n.user = :user')
                        ->andWhere('n.isReaded = :isReaded')
                        ->setParameter('user', $user)
                        ->setParameter('isReaded', false)
                        ->getQuery()
                        ->getSingleScalarResult();
    }
}
