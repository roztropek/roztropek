<?php

namespace Notification\Extractor;


use Notification\Entity\Notification as NotificationEntity;

class Notification {

    /**
     * @param NotificationEntity $notificationEntity
     * @return array
     */
    public static function extract(NotificationEntity $notificationEntity) {
        return [
            'date' => $notificationEntity->getCreatedAt(),
            'data' => $notificationEntity->getData(),
            'event' => $notificationEntity->getEvent(),
            'isReaded' => $notificationEntity->isReaded()
        ];
    }

}
