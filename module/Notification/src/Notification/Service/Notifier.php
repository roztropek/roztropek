<?php

namespace Notification\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Roztropek\Utils\Helper\ArrayHelper;
use User\Entity\User as UserEntity;
use Notification\Entity\Notification as NotificationEntity;
use Notification\Model\Message as MessageModel;
use Zend\Http\Client as HttpClient;

class Notifier extends Base {

    protected $notifications = [];
    protected $client;
    protected $isActive = false;

    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator) {
        parent::createService($serviceLocator);
        $config = $this->getService('config');
        $notifierConfig = ArrayHelper::getKey('notifier', $config, []);
        if (!$notifierConfig) {
            return $this;
        }
        $this->isActive = boolval(ArrayHelper::getKey('isActive', $notifierConfig, false));
        $url = ArrayHelper::getKey('url', $notifierConfig, '', 'string');
        if (!$url) {
            throw new \Exception('nieprawidłowy url');
        }
        $httpClient = new HttpClient($url);
        $httpClient->setMethod('POST');
        $httpClient->setHeaders([
            'x-token' => ArrayHelper::getKey('secret', $notifierConfig, '', 'string'),
            'Content-type' => 'application/json'
        ]);
        $this->client = $httpClient;
        return $this;
    }

    public function notify(array $data) {
        $this->notifications[] = new MessageModel($data);
    }

    public function flush(MessageModel $message = null) {
        if (!$this->isActive || (!$message && empty($this->getNotifications()))) {
            return;
        }
        $client = $this->getClient();
        if ($message) {
            $body = json_encode([
                'message' => $message->toArray()
            ]);
        } else {
            $body = $this->prepareBody();
        }

        $client->setRawBody($body);
        try {
            $client->send();
        } catch (\Exception $ex) {
            //w przyszlosci moze cos tu zaloguje ;)
        }

        $this->clear();
    }

    public function clear() {
        $this->notifications = [];
    }

    protected function prepareBody() {
        $messages = [];
        foreach ($this->getNotifications() as $message) {
            $messages[] = $message->toArray();
        }
        return json_encode(['messages' => $messages]);
    }

    protected function getNotifications() {
        return $this->notifications;
    }

    /**
     * @return Zend\Http\Client
     */
    protected function getClient() {
        return $this->client;
    }

}

?>
