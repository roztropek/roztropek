<?php

namespace Notification\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Roztropek\Utils\Helper\ArrayHelper;
use User\Entity\User as UserEntity;
use Notification\Entity\Notification as NotificationEntity;
use Notification\Extractor\Notification as NotificationExtractor;

class Notification extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = '\Notification\Entity\Notification';

    public function getList(array $params) {
        $identity = $this->getService('Auth')->getIdentity();

        if ($identity && $identity['role'] === 'player') {
            $params['user'] = $identity['idUser'];
        }
        return $this->getEntityRepository()->getList($params);
    }

    public function getUnreadedCount() {
        $identity = $this->getService('Auth')->getIdentity();
        if (!$identity) {
            throw new \Exception('Brak identity');
        }
        return (int) $this->getEntityRepository()->getUnreadedCount($identity['idUser']);
    }

    public function notify(UserEntity $userEntity, $event, array $data) {
        $notificationEntity = $this->create([
            'user' => $userEntity,
            'event' => $event,
            'data' => $data,
        ]);
        $this->getService('Notifier')->notify([
            'code' => 'notification',
            'destination' => $userEntity->getId(),
            'body' => NotificationExtractor::extract($notificationEntity)
        ]);
    }

    public function readAll() {
        $identity = $this->getService('Auth')->getIdentity();
        if (!$identity) {
            throw new \Exception('Brak identity');
        }
        $this->getEntityRepository()->createQueryBuilder('n')
                ->update()
                ->set('n.isReaded', true)
                ->where('n.user = :user')
                ->setParameter('user', $identity['idUSer'])
                ->getQuery()->execute();
        return true;
    }

    protected function create(array $data) {
        $notificationEntity = new NotificationEntity($data);
        $this->getEntityManager()->persist($notificationEntity);
        return $notificationEntity;
    }

}

?>
