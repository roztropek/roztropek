<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Notification\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Notification\Extractor\Notification as NotificationExtractor;

class NotificationController extends ApiController {

    public function getListAction() {
        $notificationService = $this->getService('Notification');
        $params = $this->params()->fromQuery();
        $notifications = $notificationService->getList($params);
        $result = [];
        
        foreach ($notifications as $notificationEntity) {
            $result[] = NotificationExtractor::extract($notificationEntity);
        }
        
        return new JsonModel([
            'success' => true,
            'result' => $result,
            'totalCount' => $notifications->count(),
            'unreaded' => $notificationService->getUnreadedCount()
        ]);
    }

    public function readAll() {
        $this->getService('Notification')->readAll();
        return new JsonModel(['success' => true]);
    }

}
