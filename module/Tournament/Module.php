<?php

namespace Tournament;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\Console\Adapter\AdapterInterface as Console;

class Module implements ConsoleBannerProviderInterface {

    public function getConsoleBanner(Console $console) {
        return '';
    }

    public function getConsoleUsage(Console $console) {
        return [
            'payWinnings' => 'Wypłaca wygrane w turniejach ktora na to oczekuja',
            'fillSitAndGoTournaments' => 'uzupelnia braki w turniejach',
            'checkAllTournaments' => 'sprawdza, czy nie zakończyć turniejów'
        ];
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
