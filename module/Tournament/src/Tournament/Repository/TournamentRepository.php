<?php

namespace Tournament\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Doctrine\ORM\AbstractQuery;
use Roztropek\Utils\Helper\ArrayHelper;
use Roztropek\Utils\Helper\ValueHelper;
use Doctrine\ORM\Tools\Pagination\Paginator;
use User\Entity\User as UserEntity;

class TournamentRepository extends BaseRepository {

    public function getList(array $params) {
        $winCurrency = ArrayHelper::getKey('winCurrency', $params, null, 'string');
        $payCurrency = ArrayHelper::getKey('payCurrency', $params, null, 'string');
        $userLimit = ArrayHelper::getKey('userLimit', $params, null, 'natural');
        $user = ArrayHelper::getKey('user', $params, null, 'string');
        $status = ArrayHelper::getKey('status', $params, 1, 'natural');
        $limit = ArrayHelper::getKey('limit', $params, 20, 'natural');
        $type = ArrayHelper::getKey('type', $params, null, 'natural');


        $qb = $this->createQueryBuilder('t')
                ->andWhere('t.status = :status')
                ->setMaxResults($limit)
                ->setFirstResult(0);

        $qb->setParameters([
            'status' => $status
        ]);

        if ($limit > 20) {
            $limit = 20;
        }

        if ($userLimit) {
            $qb->andWhere('t.userLimit = :userLimit');
            $qb->setParameter('userLimit', $userLimit);
        }

        if ($payCurrency) {
            $qb->andWhere('t.payCurrency = :payCurrency');
            $qb->setParameter('payCurrency', $payCurrency);
        }

        if ($winCurrency) {
            $qb->andWhere('t.winCurrency = :winCurrency');
            $qb->setParameter('winCurrency', $winCurrency);
        }

        if ($user) {
            $qb->andWhere('q.idUser = :idUser');
            $qb->setParameter('idUser', $user);
        }

        if ($type) {
            if ($type > 2) {
                $type = 1;
            }
            $qb->andWhere('t INSTANCE OF :type');
            $qb->setParameter('type', $type);
        }

        $qb->orderBy('t.createdAt', 'ASC');
        return new Paginator($qb, false);
    }

    public function getHistory(UserEntity $player, $params) {
        $limit = ArrayHelper::getKey('limit', $params, 20, 'natural');
        $userLimit = ArrayHelper::getKey('userLimit', $params, null, 'natural');
        $page = ArrayHelper::getKey('page', $params, 1, 'natural');

        if ($page < 1) {
            $page = 1;
        }

        if ($limit > 20) {
            $limit = 20;
        }

        $qb = $this->createQueryBuilder('t')
                ->join('t.quizzes', 'q')
                ->andWhere('t.winCurrency = :winCurrency')
                ->andWhere('t.status = 2')
                ->andWhere('q.player = :player')
                ->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit);

        if ($userLimit) {
            $qb->andWhere('t.userLimit = :userLimit');
            $qb->setParameter('userLimit', $userLimit);
        }

        $qb->setParameter('winCurrency', 'playCoin');
        $qb->setParameter('player', $player);
        $qb->orderBy('q.endDate', 'DESC');
        return new Paginator($qb);
    }

}
