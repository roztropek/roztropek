<?php

namespace Tournament\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Doctrine\ORM\AbstractQuery;
use Roztropek\Utils\Helper\ArrayHelper;
use Roztropek\Utils\Helper\ValueHelper;

class TimeTournamentRepository extends BaseRepository {

    public function getEmptyTournamentsToClose() {
        $emptyTournamentSelectSql = "
            SELECT t.* FROM `Tournament` t 
            LEFT JOIN Quiz q ON q.idTournament = q.id
            WHERE t.endDate < NOW() 
            AND t.type = 2 
            AND t.status = 1
            GROUP BY t.id
            HAVING COUNT(q.id) = 0";
        return $this->getConnection()->executeQuery($emptyTournamentSelectSql)
                        ->fetchAll();
    }

    public function closeTournaments(array $tournaments) {
        $idsTournaments = array_map(function($tournament) {
            return $tournament['id'];
        }, $tournaments);
        if (count($idsTournaments) > 0) {
            $sql = sprintf("UPDATE Tournament SET status = 2 WHERE id IN(%s)", implode(',', $idsTournaments));
            echo $sql . "\n";
            $conn = $this->getEntityManager()->getConnection();
            $stmt = $conn->prepare($sql);
            return $stmt->execute();
        }
        return 0;
    }

    public function getTimeTournamentsToClose() {
        $tournamentsToDoneSql = "SELECT t.* FROM `Tournament` t 
            LEFT JOIN Quiz q ON q.idTournament = t.id
            WHERE t.endDate < NOW()
            AND t.status = 1
            GROUP BY t.id
            HAVING SUM(IF(q.status = 1,1,0)) = 0";

        return $this->getConnection()->executeQuery($tournamentsToDoneSql)->fetchAll();
    }

    public function getSitAndGoTournamentsToClose() {
        $tournamentsToDoneSql = "SELECT t.* FROM `Tournament` t 
            LEFT JOIN Quiz q ON q.idTournament = t.id
            AND t.status = 1
            AND t.userLimit > 0
            GROUP BY t.id
            HAVING SUM(IF(q.status = 1,1,0)) = 0
            AND COUNT(q.id) >= t.userLimit";

        return $this->getConnection()->executeQuery($tournamentsToDoneSql)->fetchAll();
    }

    public function closeTournamentsAndPayWinners(array $tournaments) {
        $conn = $this->getConnection();
        $quizFields = [
            'prize',
            'idUser'
        ];
        foreach ($tournaments as $tournament) {
            $tournamentDetails = $this->getDetails($tournament['id'], ['quizFields' => $quizFields]);
            if (empty($tournamentDetails)) {
                continue;
            }

            $details = $tournamentDetails['details'];
            $quizzes = $tournamentDetails['quizzes'];

            if (is_array($tournamentDetails) && array_key_exists('quizzes', $tournamentDetails)) {
                $conn->beginTransaction();
                foreach ($tournamentDetails['quizzes'] as $quiz) {
                    if ($quiz['prize'] == 0) {
                        break;
                    }
                    $columnName = 'playCoins';
                    $sql = sprintf("
                        UPDATE Wallet w 
                        SET %s = %s+%d
                        WHERE id = %d
                        ", $columnName, $columnName, $quiz['prize'], $quiz['idUser']);
                    $conn->executeQuery($sql);
                }
                $conn->executeQuery("UPDATE Tournament SET status = 2 WHERE id = " . $details['id']);
                try {
                    $conn->commit();
                    echo "[Success] Udało się podsumować turniej " . $details['id'] . "\n";
                } catch (Exception $ex) {
                    echo "[Error] Nie udało się podsumować turnieju " . $details['id'] . "\n";
                    $conn->rollBack();
                }
            }
        }
    }

}
