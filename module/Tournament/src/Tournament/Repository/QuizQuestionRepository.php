<?php

namespace Tournament\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Roztropek\Utils\Result\Result;
use Doctrine\ORM\AbstractQuery;

class QuizQuestionRepository extends BaseRepository {

    public function getQuestionWithQuiz($idQuestion) {
        $dql = 'SELECT qq FROM ' . $this->getClassName() . ' qq '
                . 'JOIN qq.quiz q WHERE qq.id = :idQuestion AND qq.answer IS NULL AND q.status = 1'
                . 'ORDER BY qq.id';
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('idQuestion', $idQuestion);
        $query->setMaxResults(1);
        return array_pop($query->execute());
    }

    public function getNextQuestion($idQuiz) {
        $dql = 'SELECT qq FROM ' . $this->getClassName() . ' qq '
                . 'JOIN qq.quiz q WHERE qq.quiz = :idQuiz AND qq.answer IS NULL AND q.status = 1'
                . 'ORDER BY qq.id';
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameter('idQuiz', $idQuiz);
        $query->setMaxResults(1);
        return array_pop($query->execute());
    }

}
