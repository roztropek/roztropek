<?php

namespace Tournament\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Doctrine\ORM\AbstractQuery;
use Roztropek\Utils\Helper\ArrayHelper;
use Roztropek\Utils\Helper\ValueHelper;

class SitAndGoTournamentRepository extends BaseRepository {

    public function getTournamentCount(array $params) {
        $payCurrency = ArrayHelper::getKey('payCurrency', $params, 'playCoin', 'string');
        $winCurrency = ArrayHelper::getKey('winCurrency', $params, 'playCoin', 'string');
        $userLimit = ArrayHelper::getKey('userLimit', $params, 3, 'natural');
        $bid = ArrayHelper::getKey('bid', $params, 100, 'natural');

        $query = $this->createQueryBuilder('t')
                ->select('count(t.id)')
                ->andWhere('t.winCurrency = :winCurrency')
                ->andWhere('t.payCurrency = :payCurrency')
                ->andWhere('t.bid = :bid')
                ->andWhere('t.status = 1')
                ->andWhere('t.userLimit = :userLimit');

        $query->setParameter('winCurrency', $winCurrency);
        $query->setParameter('payCurrency', $payCurrency);
        $query->setParameter('userLimit', $userLimit);
        $query->setParameter('bid', $bid);

        return $query->getQuery()->getSingleScalarResult();
    }

    public function getEmptyTournamentsToClose() {
        $emptyTournamentSelectSql = "
            SELECT t.* FROM `Tournament` t 
            LEFT JOIN Quiz q ON q.idTournament = q.id
            WHERE t.endDate < NOW() 
            AND t.type = 2 
            AND t.status = 1
            GROUP BY t.id
            HAVING COUNT(q.id) = 0";
        return $this->getConnection()->executeQuery($emptyTournamentSelectSql)
                        ->fetchAll();
    }

    public function closeTournaments(array $tournaments) {
        $idsTournaments = array_map(function($tournament) {
            return $tournament['id'];
        }, $tournaments);
        if (count($idsTournaments) > 0) {
            $sql = sprintf("UPDATE Tournament SET status = 2 WHERE id IN(%s)", implode(',', $idsTournaments));
            echo $sql . "\n";
            $conn = $this->getEntityManager()->getConnection();
            $stmt = $conn->prepare($sql);
            return $stmt->execute();
        }
        return 0;
    }

    public function getTimeTournamentsToClose() {
        $tournamentsToDoneSql = "SELECT t.* FROM `Tournament` t 
            LEFT JOIN Quiz q ON q.idTournament = t.id
            WHERE t.endDate < NOW()
            AND t.status = 1
            GROUP BY t.id
            HAVING SUM(IF(q.status = 1,1,0)) = 0";

        return $this->getConnection()->executeQuery($tournamentsToDoneSql)->fetchAll();
    }

    public function getSitAndGoTournamentsToClose() {
        $tournamentsToDoneSql = "SELECT t.* FROM `Tournament` t 
            LEFT JOIN Quiz q ON q.idTournament = t.id
            AND t.status = 1
            AND t.userLimit > 0
            GROUP BY t.id
            HAVING SUM(IF(q.status = 1,1,0)) = 0
            AND COUNT(q.id) >= t.userLimit";

        return $this->getConnection()->executeQuery($tournamentsToDoneSql)->fetchAll();
    }

}
