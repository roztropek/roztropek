<?php

namespace Tournament\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Roztropek\Utils\Result\Result;
use Doctrine\ORM\AbstractQuery;

class QuizRepository extends BaseRepository {

    public function getDetails($idQuiz) {
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT count(id) as totalQuestions, SUM(points) as totalPoints, count(answer) as answers
                FROM QuizQuestion
                WHERE idQuiz = :idQuiz';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('idQuiz', $idQuiz, \PDO::PARAM_INT);
        $stmt->execute();

        $result = $stmt->fetch();
        return $result;
    }

    public function clearQuizzes() {
        $config = $this->serviceLocator->get('Config');
        $answerTime = $config['quiz']['answer']['time'];
        $sumSql = "SELECT SUM(qq.points) FROM QuizQuestion qq WHERE qq.idQuiz = q.id";
        $questionNumberSql = "SELECT t.questionNumber FROM Tournament t WHERE t.id = q.idTournament";
        $notStartedQuizConditionSql = sprintf("q.startDate IS NULL AND TIME_TO_SEC(DATE_DIFF(NOW(),q.insertDate )) > %d", $answerTime);
        $notEndedQuizConditionSql = sprintf("TIME_TO_SEC(DATE_DIFF(NOW(), q.startDate)) > (%s*%d)", $questionNumberSql, $answerTime);
        $setSql = sprintf("q.status = 3, q.points = %s q.time = IF(q.startDate IS NULL,0,%s*%d)", $sumSql, $questionNumberSql, $answerTime);
        $whereSql = sprintf("q.status = 2 AND ((%s) OR (%s))", $notStartedQuizConditionSql, $notEndedQuizConditionSql);
        $updateSql = sprintf("UPDATE quiz q SET %s WHERE %s", $setSql, $whereSql);

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($updateSql);
        $result = $stmt->execute();
        return $result;
    }

}
