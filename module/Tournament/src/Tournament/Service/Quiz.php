<?php

namespace Tournament\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Tournament\Entity\Quiz as QuizEntity;
use Tournament\Extractor\Question as QuestionExtractor;
use Tournament\Extractor\QuizQuestion as QuizQuestionExtractor;
use Tournament\Extractor\Quiz as QuizExtractor;
use Tournament\Entity\QuizQuestion as QuizQuestionEntity;
use Tournament\Exception\QuizServiceException;

class Quiz extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Tournament\Entity\Quiz';
    
    public function create(array $data) {
        $entityClass = $this->getEntityClass();
        $entity = new $entityClass($data);
        $this->getEntityManager()->persist($entity);
        return $entity;
    }
    
    public function getNextQuestion($quizEntity) {
        $result = $this->createResult(Result::RESOURCE_NOT_FOUND);

        if (!$quizEntity instanceof QuizEntity) {
            $quizEntity = $this->getEntityRepository()->find($quizEntity);
        }

        if (!$quizEntity) {
            throw new QuizServiceException('Nie ma takiego quizu');
        }

        if (!$this->checkQuizOwner($quizEntity)) {
            throw new QuizServiceException('Nie ma takiego quizu');
        }
        $this->checkQuiz($quizEntity);
        if ($quizEntity->isEnded()) {
            return $this->prepareResult($quizEntity);
        }

        $currentQuestion = $quizEntity->getFirstUnansweredQuestion();

        if (!$currentQuestion && $quizEntity->getQuestionCount() < $quizEntity->getQuestionLimit()) {
            $question = $this->getFreshQuestion($quizEntity);
            if ($question) {
                $questionData = QuestionExtractor::extract($question);
                $questionData['quiz'] = $quizEntity;
                $questionData['question'] = $question;
                $quizQuestion = new QuizQuestionEntity($questionData);
                $this->getEntityManager()->persist($quizQuestion);
                $currentQuestion = $quizQuestion;
            }
        }

        if (!$currentQuestion) {
            throw new QuizServiceException('Coś poszło nie tak');
        }

        if (!$quizEntity->isStarted()) {
            $quizEntity->start();
        }
        $extractedQuestion = QuizQuestionExtractor::extract($currentQuestion);
        $extractedQuiz = QuizExtractor::extract($quizEntity);
        $result->setResult([
            'question' => $extractedQuestion,
            'details' => $extractedQuiz
        ]);
        $result->setSuccess(Result::OK);
        return $result;
    }

    public function answer($idQuiz, $idQuestion, $answer) {
        $result = $this->createResult(Result::RESOURCE_NOT_FOUND);
        $quizEntity = $this->getEntityRepository()->find($idQuiz);

        if (!$quizEntity) {
            throw new QuizServiceException('Nie ma takiego quizu');
        }

        if (!$this->checkQuizOwner($quizEntity)) {
            throw new QuizServiceException('Nie ma takiego quizu');
        }

        if ($this->checkQuiz($quizEntity)) {
            return $this->prepareResult($quizEntity);
        }

        $quizEntity->answer($idQuestion, $answer);

        if ($this->checkQuiz($quizEntity)) {
            return $this->prepareResult($quizEntity);
        }
        return $this->getNextQuestion($quizEntity);
    }

    public function checkIfStopped(QuizEntity $quizEntity) {
        if ($quizEntity->isEnded()) {
            return false;
        }
        $quizEntity->checkIfStop();
        return $quizEntity->isEnded();
    }

    protected function checkQuizOwner(QuizEntity $quizEntity, $identity = null) {
        $authService = $this->serviceLocator->get('Auth');
        $identity = $identity ?: $authService->getIdentity();
        return $identity['idUser'] == $quizEntity->getPlayer()->getId();
    }

    protected function checkQuiz(QuizEntity $quizEntity) {
        $isStopped = $this->checkIfStopped($quizEntity);
        if ($isStopped) {
            $this->getService('Tournament')->checkTournament($quizEntity->getTournament());
        }
        return $isStopped;
    }

    protected function prepareResult(QuizEntity $quizEntity) {
        $result = $this->createResult(Result::RESOURCE_NOT_FOUND);
        $result->setSuccess(Result::OK);
        $result->setMessages('Quiz się zakończył');
        $result->setResult(['details' => QuizExtractor::extract($quizEntity)]);
        return $result;
    }

    protected function getFreshQuestion(QuizEntity $quizEntity) {
        $exludedIds = $quizEntity->collectQuestionIds();
        return $this->getRepository('\Question\Entity\Question')->getRandomQuestion($exludedIds);
    }

}
