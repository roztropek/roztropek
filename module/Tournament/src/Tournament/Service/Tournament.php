<?php

namespace Tournament\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use User\Entity\User;
use Roztropek\Utils\Helper\ArrayHelper;
use Tournament\Entity\Tournament as TournamentEntity;
use Tournament\Entity\SitAndGoTournament as SitAndGoTournamentEntity;
use Tournament\Extractor\Tournament as TournamentExtractor;
use Notification\Entity\Notification as NotificationEntity;
use User\Extractor\User as UserExtractor;
use Tournament\Exception\TournamentServiceException;

class Tournament extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Tournament\Entity\Tournament';
    protected $tournamentsToRebuild = [];

    public function create(array $data) {
        $type = ArrayHelper::getKey('type', $data);
        $serviceName = null;
        switch ($type) {
            case 'time':
            case 'timeTournament':
                $serviceName = 'TimeTournament';
                break;
            case 'sitAndGo':
            case 'sitAndGoTournament':
                $serviceName = 'SitAndGouTournament';
            default:
                throw new \InvalidArgumentException('Nieprawidłowy typ!');
        }
        return $this->getService($serviceName)->create($data);
    }

    public function getDetails($id) {
        $result = $this->createResult(Result::OK);

        $identity = $this->getService('Auth')->getIdentity();
        $player = null;
        if ($identity && $identity['role'] === 'player') {
            $player = $this->getEntityManager()->getReference('\User\Entity\User', $identity['idUser']);
        }

        $tournament = $this->getEntityRepository()->find($id);
        if (!$tournament) {
            throw new TournamentServiceException('Nie ma takiego turnieju');
        }

        $extractedTournament = TournamentExtractor::extract($tournament, $player);
        $prizes = [];
        foreach ($tournament->getPrizesTable() as $prize) {
            $quiz = $prize['quiz'];
            $player = $quiz->getPlayer();
            $prizes[] = [
                'status' => $quiz->getStatus(),
                'points' => $quiz->getPoints() ?: 0,
                'correctAnswers' => $quiz->getCorrectAnswersCount() ?: 0,
                'time' => $quiz->getTime() ?: 0,
                'prize' => $prize['prize']->getValue(),
                'player' => UserExtractor::extract($player)
            ];
        }
        $extractedTournament['quizzes'] = $prizes;
        $result->setResult($extractedTournament);
        return $result;
    }

    public function join($idTournament, User $player) {
        $transactionService = $this->getService('Transaction');
        $tournament = $this->getEntityRepository()->find($idTournament);
        if (!$tournament) {
            $tournamentServiceException = new TournamentServiceException('Nie ma takiego turnieju');
            $tournamentServiceException->setReason(TournamentServiceException::TOUNRAMENT_NOT_FOUND);
            throw $tournamentServiceException;
        }

        $canAffordJoinForTournament = $transactionService->canAffordForPayForJoinTournament($player, $tournament);
        if (!$canAffordJoinForTournament) {
            $price = $tournament->getPrice();
            $tournamentServiceException = new TournamentServiceException('Za mało środków');
            switch (strtolower($price->getCurrency())) {
                case 'playcoin':
                    $tournamentServiceException->setReason(TournamentServiceException::NOT_ENOUGH_COINS);
                    break;
                case 'gold':
                    $tournamentServiceException->setReason(TournamentServiceException::NOT_ENOUGH_GOLD);
                    break;
            }

            throw $tournamentServiceException;
        }
        $canPlayTournament = $tournament->canPlayerJoin($player);
        if (!$canPlayTournament) {
            $tournamentServiceException = new TournamentServiceException('Nie możesz dołączyć');
            $tournamentServiceException->setReason(TournamentServiceException::CANT_JOIN);
            throw $tournamentServiceException;
        }

        $quizData = [
            'tournament' => $tournament,
            'player' => $player,
            'status' => 0
        ];

        $transactionService->payForJoinTournament($player, $tournament);
        $quizEntity = $this->getService('Quiz')->create($quizData);

        $this->getService('Notification')->notify($player, NotificationEntity::TOURUNAMENT_PLAY, [
            'tournamentId' => $tournament->getId(),
            'quizId' => $quizEntity->getId(),
            'price' => $tournament->getPrice($player)->toArray()
        ]);
        return $quizEntity->getId();
    }

    public function getList(array $params = []) {
        $identity = $this->getService('Auth')->getIdentity();
        $player = null;
        if ($identity && $identity['role'] === 'player') {
            $player = $this->getEntityManager()->getReference('\User\Entity\User', $identity['idUser']);
        }
        $tournaments = $this->getEntityRepository()->getList($params);
        $extractedTournaments = [];
        foreach ($tournaments as $tournament) {
            $extractedTournament = TournamentExtractor::extract($tournament, $player);
            $extractedTournaments[] = $extractedTournament;
        }
        return $this->createResult(Result::OK, ['totalCount' => count($tournaments), 'tournaments' => $extractedTournaments]);
    }

    public function getHistory(array $params = []) {
        $identity = $this->getService('Auth')->getIdentity();
        $player = $this->getEntityManager()->getReference('\User\Entity\User', $identity['idUser']);
        $tournaments = $this->getEntityRepository()->getHistory($player, $params);
        return $tournaments;
    }

    public function getTournamentsToPayWinnings() {
        return $this->getEntityRepository()->findBy(['status' => 9]);
    }

    public function checkIfStopped(TournamentEntity $tournament) {
        if ($tournament->isClosed()) {
            return false;
        }
        $tournament->checkIfStop();
        return $tournament->isClosed();
    }

    public function checkTournament(TournamentEntity $tournament) {
        if ($this->checkIfStopped($tournament)) {
            $this->tournamentsToRebuild[] = $tournament;
        }
    }

    public function rebuildTournamentCount() {
        foreach (array_unique($this->tournamentsToRebuild) as $tournament) {
            if ($tournament instanceof SitAndGoTournamentEntity) {
                $sitAndGoTournamentService = $this->getService('SitAndGoTournament');
                $sitAndGoTournamentService->rebuildTournamentCount($tournament);
            }
        }
    }

    public function payWinnings(TournamentEntity $tournament) {
        if ($tournament->getStatus() == 9) {
            $prizesTable = $tournament->getPrizesTable();
            $transactionService = $this->getService('Transaction');
            foreach ($prizesTable as $prize) {
                $money = $prize['prize'];
                $quiz = $prize['quiz'];
                $player = $quiz->getPlayer();
                if ($money->getValue() > 0) {
                    $transactionService->recharge($player, $money, 'winInTournament', $quiz->getId(), 'Quiz');
                    $this->getService('Notification')->notify($player, NotificationEntity::TOURNAMENT_END, [
                        'tournamentId' => $tournament->getId(),
                        'quizId' => $quiz->getId(),
                        'money' => $money->toArray()
                    ]);
                }
            }
            $tournament->markAsSettled();
        }
    }

}
