<?php

namespace Tournament\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Roztropek\Utils\Helper\ArrayHelper;
use Tournament\Entity\SitAndGoTournament as SitAndGoTournamentEntity;
use Tournament\Factory\SitAndGoTournament as SitAndGoTournamentFactory;

class SitAndGoTournament extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Tournament\Entity\SitAndGoTournament';

    public function getTournamentCount(array $params) {
        return $this->getEntityRepository()->getTournamentCount($params);
    }

    public function create(array $data) {
        $tournamentEntity = new SitAndGoTournamentEntity($data);
        $this->getEntityManager()->persist($tournamentEntity);
        return $this->createResult(Result::CREATED, $tournamentEntity);
    }

    public function rebuildTournamentCount(SitAndGoTournamentEntity $tournament) {
        $expectedTournamentCountKey = sprintf('%s-%s-%d-%d-%d', $tournament->getPayCurrency(), $tournament->getWinCurrency(), $tournament->getUserLimit(), $tournament->getBid(), $tournament->getQuestionType());
        $this->rebuildTournamentCountFromStringConfig($expectedTournamentCountKey);
    }

    public function rebuildTournamentCountFromStringConfig($configKey) {
        $tournamentCountToCreate = 0;
        $config = $this->getService('Config');
        $defaultTournamentCount = $config['defaultTournamentCount'];
        $expectedTournamentCount = ArrayHelper::getKey($configKey, $defaultTournamentCount, 0);
        if ($expectedTournamentCount > 0) {
            $configArray = explode('-', $configKey);
            $params = [
                'payCurrency' => array_shift($configArray),
                'winCurrency' => array_shift($configArray),
                'userLimit' => (int) array_shift($configArray),
                'bid' => (int) array_shift($configArray),
                'questionType' => (int) array_shift($configArray),
            ];
            $tournamentCount = $this->getTournamentCount($params);
            $tournamentCountToCreate = $expectedTournamentCount - $tournamentCount;
            if ($tournamentCountToCreate > 0) {
                for ($i = 0; $i < $tournamentCountToCreate; $i++) {
                    $result = $this->create($params);
                    if (!$result->isValid()) {
                        throw new \Exception('cos nie pyklo');
                    }
                }
            }
        }
        return $tournamentCountToCreate;
    }

}

?>
