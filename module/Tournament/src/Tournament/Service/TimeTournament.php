<?php

namespace Tournament\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Roztropek\Utils\Helper\ArrayHelper;
use Tournament\Entity\TimeTournament as TimeTournamentEntity;

class TimeTournament extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    protected $entityClass = 'Tournament\Entity\TimeTournament';

    public function getList(array $params = array()) {
        return $this->createResult(Result::OK, $this->getEntityRepository()->getList($params));
    }
    
    public function create(array $data) {
        $endDate = ArrayHelper::getKey('endDate', $data, '', 'string');
        $startDate = ArrayHelper::getKey('startDate', $data, '', 'string');
        $duration = ArrayHelper::getKey('duration', $data, 0, 'natural');
        if ($duration > 0 && $startDate && !$endDate) {
            $data['endDate'] = date('Y-m-d H:i:s', strtotime(sprintf('+ %d minutes', $duration), strtotime($startDate)));
        }
        $tournamentEntity = new TimeTournamentEntity($data);
        $this->getEntityManager()->persist($tournamentEntity);
        return $this->createResult(Result::CREATED, $tournamentEntity);
    }

}
