<?php

namespace Tournament\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Utils\Helper\ArrayHelper;
use Tournament\Extractor\Tournament as TournamentExtractor;

class TournamentController extends ApiController {

    public function joinAction() {
        $data = $this->getData();
        $id = ArrayHelper::getKey('id', $data, null, 'string');
        if (!$id) {
            return new JsonModel(['success' => false]);
        }

        $quizId = $this->getService('Tournament')->join($id, $this->getService('Auth')->getFreshIdentity());
        $response = new JsonModel(['success' => true, 'result' => $quizId]);
        return $this->finalize($response);
    }

    public function listAction() {
        $params = $this->params()->fromQuery();

        if ($this->isIdentityPlayer()) {
            $params['checkUser'] = $this->getIdentity()['idUser'];
        }

        $result = $this->getService('Tournament')->getList($params);
        $resultData = $result->getResult();
        $tournaments = $resultData['tournaments'];
        usort($tournaments, function($prev, $next) {
            if ($prev['type'] !== $next['type']) {
                //timeTournament najpierw
                return $prev['type'] === 'timeTournament' ? -1 : 1;
            }
            if ($prev['bid'] !== $next['bid']) {
                return $prev['bid'] <=> $next['bid'];
            }
            if ($prev['pot'] !== $next['pot']) {
                return $prev['pot'] <=> $next['pot'];
            }
            if ($prev['userLimit'] !== $next['userLimit']) {
                return $prev['userLimit'] <=> $next['userLimit'];
            }

            return 0;
        });

        return new JsonModel(array('success' => true, 'result' => $tournaments, 'totalCount' => $resultData['totalCount']));
    }

    public function historyAction() {
        $repository = $this->getEntityManager()->getRepository('Tournament\Entity\Tournament');
        $params = $this->params()->fromQuery();
        $paginator = $this->getService('Tournament')->getHistory($params);

        $tournaments = [];
        $tournamentsIds = [];
        foreach ($paginator as $tournamentEntity) {
            $tournament = TournamentExtractor::extract($tournamentEntity, $this->getService('Auth')->getFreshIdentity());
            unset($tournament['price'], $tournament['quizzesToContinue']);
            $tournament['prize'] = 0;
            $playerQuizzes = $tournamentEntity->getPlayerQuizzes($this->getIdentity()['idUser']);
            foreach ($playerQuizzes as $playerQuiz) {
                $answers = $playerQuiz->getAnswersHistory();
                $tournamentsIds[$playerQuiz->getId()] = $tournamentEntity->getId();
                $tournament['quizDetails'] = [
                    'id' => $playerQuiz->getId(),
                    'answers' => $answers,
                    'endDate' => $playerQuiz->getEndDate(),
                    'playedTime' => $playerQuiz->getPlayedTime(),
                    'totalPoints' => array_reduce($answers, function($sum, $answer) {
                                return $sum+=$answer['points'];
                            }, 0)
                ];
            }
            $tournaments[$tournamentEntity->getId()] = $tournament;
        }

        $winnings = $this->getRepository('Payment\Entity\Transaction')
                ->findBy([
            'type' => 'credit',
            'referenceType' => 'Quiz',
            'description' => 'winInTournament',
            'reference' => array_flip($tournamentsIds)
        ]);

        foreach ($winnings as $winningEntity) {
            $reference = $winningEntity->getReference();
            if (array_key_exists($reference, $tournamentsIds)) {
                $tournaments[$tournamentsIds[$reference]]['prize'] += $winningEntity->getValue();
            }
        }

        return new JsonModel(['success' => true, 'result' => array_values($tournaments), 'totalCount' => count($paginator)]);
    }

    public function detailsAction() {
        $repository = $this->getEntityManager()->getRepository('Tournament\Entity\Tournament');
        $id = ArrayHelper::getKey('id', $this->params()->fromRoute(), null, 'string');
        if (!$id) {
            return new JsonModel(['success' => false]);
        }
        $result = $this->getService('Tournament')->getDetails($id);
        return $this->finalize(new JsonModel(['success' => true, 'result' => $result->getResult()]), false);
    }

    public function simulateAction() {
//        $value = \Roztropek\Utils\Hash\Transformer::encode([
//                    'idQuestion' => 231,
//                    'idQuiz' => 24
//                        ], 'question');
//
//        $decoded = \Roztropek\Utils\Hash\Transformer::decode($value, 'question');
//
//        return new JsonModel(['hash' => $value, 'decoded' => $decoded]);

        $quizCount = (int) $this->params()->fromQuery('quizCount', 20);
        $bid = (int) $this->params()->fromQuery('bid', 20);
        $percentWinners = $this->params()->fromQuery('percentWinner', 20) / 100;
        $calulatorMultipler = $this->params()->fromQuery('multipler', 1);

        $totalBid = $quizCount * $bid;

        $tournaments = \Tournament\Model\WinnersTree::generateTournaments($quizCount);
        $results = \Tournament\Model\WinnersTree::getWinnersTree($tournaments, $percentWinners, $totalBid, $calulatorMultipler);
        return new JsonModel(['quizCount' => $quizCount, 'bid' => $bid, 'totalBid' => $totalBid, 'winners' => $results]);
    }

    public function createAction() {
        $data = $this->getData();
        $result = $this->getService('Tournament')->create($data);
        if (true === $result->isValid()) {
            $response = new JsonModel(array('success' => true, 'id' => $result->getResult()->getId(), 'messages' => $result->getMessages()));
            return $this->finalize($response);
        }
        return new JsonModel(array('success' => false, 'messages' => $result->getMessages()));
    }

}
