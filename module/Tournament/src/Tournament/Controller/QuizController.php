<?php

namespace Tournament\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Utils\Helper\ArrayHelper;
use Roztropek\Utils\Hash\Transformer;

class QuizController extends ApiController {

    public function nextQuestionAction() {
        $idQuiz = $this->params('id', false);
        $result = $this->getService('Quiz')->getNextQuestion($idQuiz);
        $this->getResponse()->setStatusCode($result->getSuccess());
        $response = new JsonModel(['success' => $result->isValid(), 'result' => $result->getResult(), 'messages' => $result->getMessages()]);
        $response = $this->finalize($response);
        $this->getService('Tournament')->rebuildTournamentCount();
        $this->flush();
        return $response;
    }

    /**
     * @todo rozliczanie turnieju
     */
    public function answerAction() {
        $id = $this->params('id', false);
        $data = $this->getData();
        $answer = ArrayHelper::getKey('answer', $data, null, 'string');
        $idQuestion = ArrayHelper::getKey('token', $data, null, 'string');
        if (!$id && !$idQuestion || !$answer) {
            $this->getResponse()->setStatusCode(422);
            return new JsonModel(['success' => false, 'message' => 'nieprawidłowe dane']);
        }

        $result = $this->getServiceLocator()->get('Quiz')->answer($id, $idQuestion, $answer);
        $this->getResponse()->setStatusCode($result->getSuccess());
        $response = new JsonModel(['success' => $result->isValid(), 'result' => $result->getResult(), 'messages' => $result->getMessages()]);
        $response = $this->finalize($response);
        $this->getService('Tournament')->rebuildTournamentCount();
        $this->flush();
        return $response;
    }

}
