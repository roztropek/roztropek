<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Tournament\Controller;

class CliController extends \Roztropek\Controller\CliController {

    public function payWinningsAction() {
        $tournamentService = $this->getService('Tournament');
        $tournamentsToPayWinnings = $tournamentService->getTournamentsToPayWinnings();
        echo sprintf("Znalazlem %d turniejów do rozliczenia\n", count($tournamentsToPayWinnings));
        foreach ($tournamentsToPayWinnings as $tournament) {
            $tournamentService->payWinnings($tournament);
            echo sprintf("rozliczylem turniej %s\n", $tournament->getId());
        }
        echo "Skonczylem rozliczac turnieje, robie flush \n";
        $this->finalize();
        echo "Skonczylem. \n";
    }

    public function fillSitAndGoTournamentsAction() {
        echo "zaczynam skrypt\n";
        $sitAndGoTournamentService = $this->getService('SitAndGoTournament');
        $config = $this->getService('Config');
        $tournamentCountConfig = $config['defaultTournamentCount'];
        foreach ($tournamentCountConfig as $expectedTournamentsKey => $expectedTournamentsCount) {
            $createdCount = $sitAndGoTournamentService->rebuildTournamentCountFromStringConfig($expectedTournamentsKey);
            echo sprintf("Stworzyłem %d/%d turniejów o kluczu: %s\n", $createdCount, $expectedTournamentsCount, $expectedTournamentsKey);
        }
        $this->finalize();
        echo "Skonczylem\n";
    }

    public function checkAllTournamentsAction() {
        $tournamentService = $this->getService('Tournament');
        $tournamentsToCheck = $this->getRepository('Tournament\Entity\Tournament')->getList(['status' => 1]);
        echo sprintf("Turniejów do sprawdzenia: %d\n", count($tournamentsToCheck));
        foreach ($tournamentsToCheck as $tournament) {
            $tournamentService->checkTournament($tournament);
        }

        $this->finalize();
        echo "Skonczylem\n";
    }

}
