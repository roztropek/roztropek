<?php

namespace Tournament\Exception;

use Roztropek\Exception\ActionFailedException;

class TournamentServiceException extends ActionFailedException {

    const TOUNRAMENT_NOT_FOUND = 'TOURNAMENT_NOT_FOUND';
    const CANT_JOIN = 'TOURNAMENT_NOT_FOUND';
    const NOT_ENOUGH_COINS = 'NOT_ENOUGH_COINS';
    const NOT_ENOUGH_GOLD = 'NOT_ENOUGH_GOLD';

}
