<?php

namespace Tournament\Exception;
use Roztropek\Exception\ActionFailedException;


class QuizServiceException extends ActionFailedException
{}