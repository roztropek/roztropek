<?php

namespace Tournament\Calculator\Price;

use User\Entity\User as Player;
use Payment\ValueObject\Money\Money;

class Price {

    protected $player;
    protected $price;

    public function __construct($price) {
        $this->setPrice($price);
    }

    public function getCalculatedPrice() {
        return $this->getPrice()->multiply(1.25);
    }

    public function setPlayer(Player $player) {
        $this->player = $player;
    }

    protected function setPrice($price) {
        $this->price = $price;
    }

    /**
     * @return \Payment\ValueObject\Money\Money
     */
    protected function getPrice() {
        return $this->price;
    }

    protected function getPlayer() {
        return $this->player;
    }

}
