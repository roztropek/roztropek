<?php

namespace Tournament\Calculator\Prize;

use Tournament\Calculator\Prize\Interfaces\PrizeCalculator as PrizeCalculatorInterface;

class Progressive {

    /**
     *  protected static $treeTypes = [
     *   '1' => [
     *       'winnerMultipler' => 0.3,
     *       'multipler' => 1.7
     *   ],
     *   '2' => [
     *       'winnerMultipler' => 0.5,
     *       'multipler' => 1.25
     *   ],
     *   '3' => [
     *       'winnerMultipler' => 0.1,
     *       'multipler' => 1.8
     *   ],
     * ];
     */
    protected $winnerPercent;
    protected $multipler;

    public function __construct($winnerPercent, $multipler) {
        $this->setMultipler($multipler);
        $this->setWinnerPercent($winnerPercent);
    }

    public function getPrizes($bid, $totalGames) {
        if ($totalGames === 0) {
            return [];
        }
        $winnersTable = [];
        $winnersCount = $this->getWinnersCount($totalGames);
        for ($remainingWinners = $winnersCount; $remainingWinners > 0; --$remainingWinners) {
            $winMoney = $remainingWinners == 1 ? $bid : $this->getSubstractor($bid, $remainingWinners);
            $bid-=$winMoney;
            array_unshift($winnersTable, $winMoney);
        }
        $loserCount = $totalGames - $winnersCount;
        if ($loserCount > 0) {
            return $winnersTable + array_fill($winnersCount, ($totalGames - $winnersCount), 0);
        }
        return $winnersTable;
    }

    protected function setWinnerPercent($winnerPercent) {
        $this->winnerPercent = $winnerPercent;
    }

    protected function setMultipler($multipler) {
        $this->multipler = $multipler;
    }

    protected function getWinnerPercent() {
        return $this->winnerPercent;
    }

    protected function getMultipler() {
        return $this->multipler;
    }

    protected function getWinnerMultipler() {
        return $this->getWinnerPercent() / 100;
    }

    protected function getWinnersCount($totalGames) {
        return ceil($totalGames * $this->getWinnerMultipler());
    }

    protected function getSubstractor($bid, $remainingTournaments) {
        $multipler = $this->getMultipler();
        if ($multipler != 1) {
            switch ($remainingTournaments) {
                case 1:
                    return 0;
                    break;
                case 2:
                    return ceil($bid / 3);
                    break;
                default:
                    break;
            }
        }
        return floor($bid / ($remainingTournaments * $multipler));
    }

}
