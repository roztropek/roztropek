<?php

namespace Tournament\Calculator\Prize\Interfaces;

interface PrizeCalculator {

    public function getPrizes($bid, $totalGames);
}
