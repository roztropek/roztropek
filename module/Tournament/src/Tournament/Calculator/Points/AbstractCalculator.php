<?php

namespace Tournament\Calculator\Points;

use Tournament\Entity\Quiz as QuizEntity;

abstract class AbstractCalculator {

    /**
     * @var \Tournament\Entity\Quiz 
     */
    protected $quizEntity;

    public function __construct(QuizEntity $quizEntity) {
        $this->setQuizEntity($quizEntity);
    }

    /**
     * @param \Tournament\Entity\Quiz $quizEntity
     */
    protected function setQuizEntity(QuizEntity $quizEntity) {
        $this->quizEntity = $quizEntity;
    }

    /**
     * @return \Tournament\Entity\Quiz 
     */
    protected function getQuizEntity() {
        return $this->quizEntity;
    }

    abstract public function calculatePoints();
}
