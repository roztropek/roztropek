<?php

namespace Tournament\Calculator\Points;

class Streak {

    const CORRECT_ANSWER_POINTS = 100;
    const STREAK_MULTIPLER_INCREMENTATOR = 0.1;
    const MAX_BONUS = 100;
    const ONE_SECOND_PENALTY = 10;

    public function calculatePoints($isCorrect, $streak, $answerTime) {
        $points = $isCorrect ? self::CORRECT_ANSWER_POINTS : 0;
        $streakMultipler = $this->calculateStreakMultipler($streak);
        $pointsWithStreak = $isCorrect ? $this->calculatePointsWithStreak($streak, $answerTime) : 0;

        return [
            'basePoints' => $points,
            'points' => $pointsWithStreak,
            'streakMultipler' => $streakMultipler
        ];
    }

    public function calculatePointsWithStreak($streak, $answerTime) {
        $timeBonus = $this->calculateTimeBonus($answerTime);
        $streakMultipler = $this->calculateStreakMultipler($streak);
        $pointsWithBonus = static::CORRECT_ANSWER_POINTS + $timeBonus;
        $pointsWithStreak = round($pointsWithBonus * $streakMultipler);
        return $pointsWithStreak;
    }

    protected function calculateTimeBonus($answerTime) {
        $timeBonus = static::MAX_BONUS - ($answerTime * static::ONE_SECOND_PENALTY);
        return $timeBonus > 0 ? $timeBonus : 0;
    }

    protected function calculateOneSecondPenalty($streak) {
        $streakMultipler = $this->calculateStreakMultipler($streak);
        return (static::MAX_BONUS * $streakMultipler) / (static::MAX_BONUS / static::ONE_SECOND_PENALTY);
    }

    protected function getMaxPossiblePoints($streak) {
        return $this->calculatePointsWithStreak($streak, 0);
    }

    protected function calculateStreakMultipler($streak) {
        return $streak > 0 ? 1 + round(static::STREAK_MULTIPLER_INCREMENTATOR * $streak, 1) : 1;
    }

    public function getStreakDetails($streak) {
        $streakMultipler = $this->calculateStreakMultipler($streak);
        return [
            'streak' => $streak,
            'streakMultipler' => $streakMultipler,
            'maxPossiblePoints' => $this->getMaxPossiblePoints($streak),
            'oneSecondPenalty' => $this->calculateOneSecondPenalty($streak),
            'maxBonusPoints' => static::MAX_BONUS * $streakMultipler,
            'minCorrectAnswerPoints' => static::CORRECT_ANSWER_POINTS * $streakMultipler
        ];
    }

}
