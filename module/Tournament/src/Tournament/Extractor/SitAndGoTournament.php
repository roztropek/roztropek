<?php

namespace Tournament\Extractor;

use Tournament\Entity\SitAndGoTournament as SitAndGoTournamentEntity;
use Tournament\Extractor\Tournament;
use User\Entity\User;

class SitAndGoTournament extends Tournament {

    /**
     * @param \Tournament\Entity\SitAndGoTournament $timeTournament
     * @return array
     */
    public static function extract(\Tournament\Entity\Tournament $sitAndGoTournament, User $player = null) {
        $data = parent::extractTournament($sitAndGoTournament, $player);
        $data['type'] = 'sitAndGoTournament';
        return $data;
    }

}
