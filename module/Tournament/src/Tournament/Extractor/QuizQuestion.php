<?php

namespace Tournament\Extractor;

use Tournament\Entity\QuizQuestion as QuizQuestionEntity;
use Roztropek\Image\Factory\Text as ImageTextFactory;

class QuizQuestion {

    /**
     * @param Tournament\Entity\QuizQuestion $quizQuestionEntity
     * @return array
     */
    public static function extract(QuizQuestionEntity $quizQuestionEntity) {
        $answerImgConfig = [
            'width' => 900,
            'height' => 200,
            'fontSize' => 20,
            'padding' => 10
        ];

        $textImage = ImageTextFactory::create($answerImgConfig);
        $textImage->setText($quizQuestionEntity->getQuestionText());
        return [
            'token' => $quizQuestionEntity->getId(),
            'question' => $quizQuestionEntity->getQuestionText(),
            'imageQuestion' => $textImage->putImage(false),
            'answers' => self::extractAnswers($quizQuestionEntity)
        ];
    }

    protected static function extractAnswers(QuizQuestionEntity $quizQuestionEntity) {
        $questionAnswerImgConfig = [
            'width' => 200,
            'height' => 100,
            'fontSize' => 15,
            'padding' => 8
        ];
        $returnAnswers = [];
        $textImage = ImageTextFactory::create($questionAnswerImgConfig);
        $answers = json_decode($quizQuestionEntity->getAnswers(), true);

        foreach ($answers as $token => $answer) {
            $textImage->setText($answer['text']);
            $returnAnswers[] = [
                'text' => $answer['text'],
                'textImage' => $textImage->putImage(false),
                'answer' => $token
            ];
        }
        return $returnAnswers;
    }

}
