<?php

namespace Tournament\Extractor;

class Question {

    /**
     * @param \Question\Entity\Question $questionEntity
     * @return array
     */
    public static function extract(\Question\Entity\Question $questionEntity) {
        $answers = [];
        foreach ($questionEntity->getFakeAnswers() as $answer) {
            $answers[] = [
                'text' => $answer->getText(),
                'hash' => self::getIdHash($questionEntity->getId())
            ];
        }
        $correctHash = self::getIdHash($questionEntity->getId());
        $answers[] = [
            'text' => $questionEntity->getCorrectAnswer(),
            'hash' => $correctHash
        ];
        shuffle($answers);

        foreach ($answers as $key => $answer) {
            $newKey = $answer['hash'];
            unset($answer['hash']);
            $answers[$newKey] = $answer;
            unset($answers[$key]);
        }

        return [
            'questionText' => $questionEntity->getText(),
            'answers' => json_encode($answers),
            'correctAnswer' => $correctHash
        ];
    }

    /**
     * @param string $idQuestion
     * @return string
     */
    public static function getIdHash($idQuestion) {
        return sha1(microtime() . $idQuestion . rand(1000, 9999));
    }

}
