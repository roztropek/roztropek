<?php

namespace Tournament\Extractor;

use Roztropek\Utils\Object\Object;
use User\Entity\User;
use Tournament\Extractor\Quiz as QuizExtractor;

class Tournament {

    /**
     * @param \Tournament\Entity\Tournament $tournamentEntity
     * @return array
     */
    public static function extract(\Tournament\Entity\Tournament $tournamentEntity, User $player = null) {
        $extractorClass = static::getExtractorClass($tournamentEntity);
        return $extractorClass::extract($tournamentEntity, $player);
    }

    protected static function getExtractorClass(\Tournament\Entity\Tournament $tournamentEntity) {
        $classname = Object::getShortClassName($tournamentEntity);
        return sprintf('\Tournament\Extractor\%s', $classname);
    }

    protected static function extractTournament(\Tournament\Entity\Tournament $tournamentEntity, User $player = null) {
        $extractedTournament = [
            'id' => $tournamentEntity->getId(),
            'userLimit' => $tournamentEntity->getUserLimit(),
            'bid' => $tournamentEntity->getBid(),
            'minPot' => $tournamentEntity->getMinPot(),
            'status' => $tournamentEntity->getStatus(),
            'questionNumber' => $tournamentEntity->getQuestionNumber(),
            'requireLvl' => $tournamentEntity->getRequireLvl(),
            'pot' => $tournamentEntity->getPot(),
            'quizCount' => $tournamentEntity->getQuizCount(),
            'price' => $tournamentEntity->getPrice($player)->extract(),
            'winCurrency' => $tournamentEntity->getWinCurrency(),
            'totalTime' => $tournamentEntity->getTotalTime()
        ];
        $extractedTournament['quizzesToContinue'] = [];
        $canJoin = false;
        $haveUnfinishedQuizzes = false;
        if ($player) {
            $canJoin = $tournamentEntity->canPlayerJoin($player);
            if (!$tournamentEntity->isClosed()) {
                $unfinishedQuizzes = $tournamentEntity->getUnfinishedQuizzes($player->getId());
                if (count($unfinishedQuizzes) > 0) {
                    $haveUnfinishedQuizzes = true;
                    foreach ($unfinishedQuizzes as $unfinishedQuiz) {
                        $extractedTournament['quizzesToContinue'][] = QuizExtractor::extract($unfinishedQuiz);
                    }
                }
            }
        }
        $canJoinStatus = $canJoin == true ? 'play' : ($player ? 'played' : 'cant join');
        $extractedTournament['quizStatus'] = $haveUnfinishedQuizzes == true ? 'continue' : $canJoinStatus;
        $extractedTournament['canJoin'] = $canJoin;
        $extractedTournament['tax'] = $tournamentEntity->getPrice()->getValue() - $extractedTournament['bid'];

        return $extractedTournament;
    }

}
