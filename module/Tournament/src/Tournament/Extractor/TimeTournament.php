<?php

namespace Tournament\Extractor;

use Tournament\Entity\TimeTournament as TimeTournamentEntity;
use Tournament\Extractor\Tournament;
use User\Entity\User;

class TimeTournament extends Tournament {

    /**
     * @param \Tournament\Entity\TimeTournament $timeTournament
     * @return array
     */
    public static function extract(\Tournament\Entity\Tournament $timeTournament, User $player = null) {
        $data = parent::extractTournament($timeTournament, $player);
        $data['type'] = 'timeTournament';
        $data['startDate'] = $timeTournament->getStartDate()->getTimestamp();
        $data['endDate'] = $timeTournament->getEndDate()->getTimestamp();
        $data['duration'] = $timeTournament->getDuration();
        $data['timeLeft'] = $timeTournament->getTimeLeft();
        return $data;
    }

}
