<?php

namespace Tournament\Extractor;

use Tournament\Entity\Quiz as QuizEntity;
use User\Extractor\User as UserExtractor;

class Quiz {

    /**
     * @param \Tournament\Entity\Quiz $quizEntity
     * @return array
     */
    public static function extract(QuizEntity $quizEntity) {
        return [
            'id' => $quizEntity->getId(),
            'totalQuestions' => $quizEntity->getQuestionLimit(),
            'player' => UserExtractor::extract($quizEntity->getPlayer()),
            'correctAnswersCount' => $quizEntity->getCorrectAnswersCount(),
            'totalPoints' => $quizEntity->getPoints(),
            'answers' => $quizEntity->getQuestionCount(),
            'answersHistory' => $quizEntity->getAnswersHistory(),
            'currentStreakDetails' => $quizEntity->getCurrentStreakDetails(),
            'timeLeft' => $quizEntity->getTimeLeft(),
            'playedTime' => $quizEntity->getPlayedTime(),
            'totalTime' => $quizEntity->getTotalTime()
        ];
    }

}
