<?php

namespace Tournament\Extractor\Details;

use Tournament\Entity\TimeTournament as TimeTournamentEntity;
use Tournament\Extractor\Details\Tournament;

class TimeTournament extends Tournament {

    /**
     * @param \Tournament\Entity\TimeTournament $timeTournament
     * @return array
     */
    public static function extract(TimeTournamentEntity $timeTournament) {
        $data = parent::extractTournament($timeTournament);
        $data['startDate'] = $timeTournament->getStartDate();
        $data['endDate'] = $timeTournament->getEndDate();
        return $data;
    }

}
