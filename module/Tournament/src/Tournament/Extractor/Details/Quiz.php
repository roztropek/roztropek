<?php

namespace Tournament\Extractor\Details;

use Tournament\Entity\Quiz as QuizEntity;
use Tournament\Extractor\Quiz as QuizExtractor;

class Quiz extends QuizExtractor {

    /**
     * @param \Tournament\Entity\Quiz $quizEntity
     * @return array
     */
    public static function extract(QuizEntity $quizEntity) {
        return [
            'totalQuestions' => $quizEntity->getQuestionLimit(),
            'totalPoints' => $quizEntity->getPoints(),
            'answers' => $quizEntity->getQuestionCount(),
            'timeLeft' => $quizEntity->getTimeLeft(),
            'totalTime' => $quizEntity->getTotalTime()
        ];
    }

}
