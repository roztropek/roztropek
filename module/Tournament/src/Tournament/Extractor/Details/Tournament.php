<?php

namespace Tournament\Extractor\Details;

use Roztropek\Utils\Object\Object;
use Tournament\Extractor\Tournament as TournamentExtractor;
use Tournament\Extractor\Quiz as QuizExtractor;

class Tournament extends TournamentExtractor {
    protected static function getExtractorClass(\Tournament\Entity\Tournament $tournamentEntity) {
        $classname = Object::getShortClassName($tournamentEntity);
        return sprintf('\Tournament\Extractor\Details\%s', $classname);
    }

    protected static function extractTournament(\Tournament\Entity\Tournament $tournamentEntity) {
        $extractedTournament = parent::extractTournament($tournamentEntity);
        $quizzes = [];
        foreach ($tournamentEntity->getQuizzes() as $quiz) {
            $quiz = static::extractQuizDetails($quiz);
            $quizzes[] = static::extractQuizDetails($quiz);
        }
        $extractedTournament['quizzes'] = $quizzes;
        return $extractedTournament;
    }

    protected static function extractQuizDetails(\Tournament\Entity\Quiz $quiz) {
        $extractedQuiz = QuizExtractor::extract($quiz);
        return $extractedQuiz;
    }

}
