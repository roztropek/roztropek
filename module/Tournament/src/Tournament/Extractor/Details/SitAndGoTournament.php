<?php

namespace Tournament\Extractor\Details;

use Tournament\Entity\SitAndGoTournament as SitAndGoTournamentEntity;
use Tournament\Extractor\Details\Tournament;

class SitAndGoTournament extends Tournament {

    /**
     * @param \Tournament\Entity\TimeTournament $timeTournament
     * @return array
     */
    public static function extract(SitAndGoTournamentEntity $sitAndGoTournament) {
        return parent::extractTournament($sitAndGoTournament);
    }

}
