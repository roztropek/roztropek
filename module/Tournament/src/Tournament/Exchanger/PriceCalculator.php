<?php

namespace Tournament\Exchanger;

use Roztropek\Exchanger\Base;

class PriceCalculator extends Base {

    private $inputFilterConfig = [
        'money' => [
            'required' => true
        ],
    ];
    
    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }
}
