<?php

namespace Tournament\Exchanger;

use Roztropek\Exchanger\Base;

class Quiz extends Base {

    private $inputFilterConfig = [
        'tournament' => [
            'required' => true
        ],
        'player' => [
            'required' => true
        ],
        'time' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 0,
                        'inclusive' => true
                    ]]
            ]
        ],
        'points' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 0,
                        'inclusive' => true
                    ]]
            ]
        ],
        'questions' => [
            'required' => false
        ],
        'insertDate' => [
            'required' => false
        ],
        'startDate' => [
            'required' => false
        ],
        'endDate' => [
            'required' => false
        ],
    ];
    
    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }
}
