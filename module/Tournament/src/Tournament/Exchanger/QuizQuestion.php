<?php

namespace Tournament\Exchanger;

use Roztropek\Exchanger\Base;

class QuizQuestion extends Base {

    private $inputFilterConfig = [
        'question' => [
            'required' => true,
            'validators' => [
                [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\Question\Entity\Question'
                    ]
                ]
            ]
        ],
        'quiz' => [
            'required' => true,
            'validators' => [
                [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\Tournament\Entity\Quiz'
                    ]
                ]
            ]
        ],
        'questionText' => [
            'required' => true,
            'validators' => [
                ['name' => 'StringLength',
                    'options' => [
                        'min' => 2,
                        'max' => 100,
                    ]]
            ]
        ],
        'points' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 0,
                        'inclusive' => true
                    ]]
            ]
        ],
        'answer' => [
            'required' => false,
            'validators' => [
                ['name' => 'StringLength',
                    'options' => [
                        'min' => 2,
                        'max' => 100,
                    ]]
            ]
        ],
        'answers' => [
            'required' => true
        ],
        'correctAnswer' => [
            'required' => true,
            'validators' => [
                ['name' => 'StringLength',
                    'options' => [
                        'min' => 2,
                        'max' => 100,
                    ]]
            ]
        ],
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
