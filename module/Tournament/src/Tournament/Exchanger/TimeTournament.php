<?php

namespace Tournament\Exchanger;

use Roztropek\Utils\Helper\ArrayHelper;

class TimeTournament extends Tournament {

    private $inputFilterConfig = [
        'startDate' => [
            'required' => true
        ],
        'endDate' => [
            'required' => true
        ]
    ];
    
    public function getInputFilterConfig() {
        $parentConfig = parent::getInputFilterConfig();
        return array_merge($parentConfig, $this->inputFilterConfig);
    }
}
