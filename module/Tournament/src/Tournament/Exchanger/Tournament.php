<?php

namespace Tournament\Exchanger;

use Roztropek\Exchanger\Base;

abstract class Tournament extends Base {

    private $inputFilterConfig = [
        'userLimit' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 3,
                        'inclusive' => true
                    ]]
            ]
        ],
        'treeType' => [
            'required' => false,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => [1, 2],
                        'strict' => true
                    ]]
            ]
        ],
        'bid' => [
            'required' => false,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => [100, 200, 500, 1000, 2000],
                        'strict' => true
                    ]]
            ]
        ],
        'minPot' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 10,
                        'inclusive' => true
                    ]]
            ]
        ],
        'cashType' => [
            'required' => false,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => [1, 2],
                        'strict' => true
                    ]]
            ]
        ],
        'questionNumber' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 5,
                        'inclusive' => true
                    ]]
            ]
        ],
        'requireLvl' => [
            'required' => false,
            'validators' => [
                ['name' => 'GreaterThan',
                    'options' => [
                        'min' => 1,
                        'inclusive' => true
                    ]]
            ]
        ],
        'questionType' => [
            'required' => false,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => [1, 2],
                        'strict' => true
                    ]]
            ]
        ],
        'payCurrency' => [
            'required' => false,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => ['playCoin', 'gold', 'ticket'],
                        'strict' => true
                    ]]
            ]
        ],
        'winCurrency' => [
            'required' => false,
            'validators' => [
                ['name' => 'InArray',
                    'options' => [
                        'haystack' => ['playCoin', 'gold', 'ticket'],
                        'strict' => true
                    ]]
            ]
        ],
    ];
    
    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
