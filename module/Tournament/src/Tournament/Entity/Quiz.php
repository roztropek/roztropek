<?php

namespace Tournament\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;
use Tournament\Entity\QuizQuestion;
Use Tournament\Calculator\Points\Streak as PointsCalculator;

/**
 * @ORM\Entity(repositoryClass="Tournament\Repository\QuizRepository")
 * @ORM\Table(name="Quiz")
 * @ORM\HasLifecycleCallbacks
 * */
class Quiz extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @todo - jak będzie dobrze działał eager zrobić eager
     * @ORM\ManyToOne(targetEntity="Tournament\Entity\Tournament", inversedBy="quizzes")
     * @ORM\JoinColumn(name="idTournament", referencedColumnName="id")
     */
    protected $tournament;

    /**
     * * @todo - jak będzie dobrze działał eager zrobić eager
     * @ORM\ManyToOne(targetEntity="User\Entity\User")
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     */
    protected $player;

    /**
     * @todo - jak będzie dobrze działał eager zrobić eager
     * @ORM\OneToMany(targetEntity="Tournament\Entity\QuizQuestion", mappedBy="quiz")
     * @ORM\OrderBy({"insertDate" = "ASC"})
     * */
    protected $questions;

    /** @ORM\Column(type="integer") */
    protected $status = 1;

    /** @ORM\Column(type="integer") */
    protected $points = 0;

    /** @ORM\Column(type="integer", nullable=true) */
    protected $time;

    /** @ORM\Column(type="datetime") */
    protected $insertDate;

    /** @ORM\Column(type="datetime", nullable=true) */
    protected $startDate;

    /** @ORM\Column(type="datetime", nullable=true) */
    protected $endDate;
    private $settersMapping = [
        'tournament' => 'setTournament',
        'player' => 'setPlayer'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->insertDate = new \DateTime();
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return \Tournament\Entity\Tournamet
     */
    public function getTournament() {
        return $this->tournament;
    }

    public function getPlayer() {
        return $this->player;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getPoints() {
        return $this->points;
    }

    public function getInsertDate() {
        return $this->insertDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function getTime() {
        return $this->time;
    }

    public function getPlayedTime() {
        if ($this->isEnded()) {
            return $this->getTime();
        } else {
            return $this->getTotalTime() - $this->getTimeLeft();
        }
    }

    public function getTimeLeft() {
        if ($this->isEnded()) {
            return 0;
        }
        if ($this->isStarted()) {
            $date = $this->getStartDate();
            $timestampStart = $date->getTimestamp();
            $diff = time() - $timestampStart;
            $timeLeft = $this->getTotalTime() - $diff;
            return $timeLeft < 0 ? 0 : $timeLeft;
        }
    }

    public function isStarted() {
        return !!$this->getStartDate();
    }

    public function getTotalTime() {
        return $this->getTournament()->getTotalTime();
    }

    public function getTimePerQuestion() {
        return $this->timePerQuestion;
    }

    public function start() {
        if (!$this->isStarted()) {
            $this->setStartDate(new \DateTime());
        }
    }

    public function addQuestion(QuizQuestion $questionEntity) {
        if (!$this->getQuestion($questionEntity->getId())) {
            $this->getQuestions()->add($questionEntity);
        }
    }

    public function getQuestionLimit() {
        return $this->getTournament()->getQuestionNumber();
    }

    public function getQuestionCount() {
        return count($this->getQuestions());
    }

    public function getAnswersHistory() {
        $questionHistory = array();
        $startTime = $this->getStartDate();
        $streak = 0;
        foreach ($this->getQuestions() as $question) {
            if ($question->getAnswer() !== null) {
                $isCorrect = $question->getAnswer() === $question->getCorrectAnswer();
                $answerTime = $question->getAnswerTime();
                $pointsDetails = $this->getPointsCalculator()->calculatePoints($isCorrect, $streak, $answerTime);
                $questionHistory[] = [
                    'time' => $answerTime,
                    'points' => $question->getPoints(),
                    'isCorrect' => $isCorrect,
                    'streak' => $streak,
                    'pointsDetails' => $pointsDetails,
                ];
                $startTime = $question->getInsertDate();
                $streak = $isCorrect ? $streak + 1 : 0;
            }
        }
        return $questionHistory;
    }

    public function getCurrentStreakDetails() {
        return $this->getPointsCalculator()->getStreakDetails($this->getCurrentStreak());
    }

    public function getCorrectAnswersCount() {
        return array_reduce($this->getAnswersHistory(), function($sum, $answer) {
            return $answer['isCorrect'] ? $sum + 1 : $sum;
        }, 0);
    }

    public function getFirstUnansweredQuestion() {
        foreach ($this->getQuestions() as $question) {
            if (!$question->getAnswer()) {
                return $question;
            }
        }
        return null;
    }

    /**
     * @return PointsCalculator
     */
    protected function getPointsCalculator() {
        return new PointsCalculator();
    }

    public function answer($idQuestion, $answer) {

        $questionEntity = $this->getQuestion($idQuestion);
        if (!$questionEntity) {
            return false;
        }
        if ($questionEntity->getAnswer()) {
            return false;
        }
        $answers = json_decode($questionEntity->getAnswers(), true);
        if (!array_key_exists($answer, $answers)) {
            return false;
        }
        if (!$this->isStarted()) {
            $this->start();
        }

        $currentStreak = $this->getCurrentStreak();
        $isCorrect = $questionEntity->getCorrectAnswer() == $answers[$answer]['text'] ? true : false;
        $questionEntity->setAnswer($answers[$answer]['text']);

        $pointsDetails = $this->getPointsCalculator()->calculatePoints($isCorrect, $currentStreak, $questionEntity->getAnswerTime());
        $questionEntity->setPoints($pointsDetails['points']);
        $this->recalculatePoints();
    }

    protected function getCurrentStreak() {
        $streak = 0;
        $answerHistory = $this->getAnswersHistory();
        foreach ($answerHistory as $answer) {
            if ($answer['isCorrect'] === true) {
                $streak += 1;
            } else {
                $streak = 0;
            }
        }
        return $streak;
    }

    protected function stop() {
        if ($this->getStatus() === 1) {
            $this->setTime($this->getPlayedTime());
            $this->recalculatePoints();
            $this->setEndDate(new \DateTime());
            $this->setStatus(2);
        }
    }

    public function checkIfStart() {
        if (!$this->isStarted() && (time() - $this->getInsertDate()->getTimestamp()) > 60 * 2) {
            $this->start();
        }
    }

    public function checkIfStop() {
        if ($this->isEnded() || !$this->isStarted()) {
            return;
        }
        $timeLeft = $this->getTimeLeft();
        if ($timeLeft <= 0) {
            $this->stop();
            return;
        }
        if ($this->getQuestionCount() >= $this->getQuestionLimit() && !$this->getFirstUnansweredQuestion()) {
            $this->stop();
            return;
        }
    }

    public function isEnded() {
        return $this->getStatus() > 1;
    }

    protected function recalculatePoints() {
        $sum = 0;
        foreach ($this->getQuestions() as $question) {
            $sum += $question->getPoints();
        }
        $this->setPoints($sum);
    }

    public function getQuestion($idQuestion) {
        foreach ($this->getQuestions() as $question) {
            if ($question->getId() == $idQuestion) {
                return $question;
            }
        }
        return null;
    }

    public function collectQuestionIds() {
        $ids = [];
        foreach ($this->getQuestions() as $question) {
            $ids[] = $question->getQuestionId();
        }
        return $ids;
    }

    public function getQuestionType() {
        return $this->getTournament()->getQuestionType();
    }

    /**
     * @return Doctrine\Common\Collections\ArrayCollection
     */
    protected function getQuestions() {
        return $this->questions;
    }

    protected function setTime($time) {
        $this->time = $time;
    }

    protected function setTournament(Tournament $tournament) {
        $this->tournament = $tournament;
        $tournament->addQuiz($this);
    }

    protected function setPlayer(\User\Entity\User $player) {
        $this->player = $player;
    }

    protected function setStatus($status) {
        $this->status = $status;
    }

    protected function setPoints($points) {
        $this->points = $points;
    }

    protected function setInsertDate($insertDate) {
        $this->insertDate = $insertDate;
    }

    protected function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    protected function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    protected function setUp(array $data) {
        parent::setUp($data);
        $this->runSetters($this->settersMapping, $data);
    }

}
