<?php

namespace Tournament\Entity;

use Doctrine\ORM\Mapping as ORM;
use Tournament\DataObject\TimeTournament as TimeTournamentDataObject;
use Roztropek\Utils\Result\SimpleResult;
use User\Entity\User;

/**
 * @ORM\Entity(repositoryClass="Tournament\Repository\TimeTournamentRepository")
 * */
class TimeTournament extends Tournament {

    /** @ORM\Column(type="datetime") */
    protected $startDate;

    /** @ORM\Column(type="datetime") */
    protected $endDate;
    private $settersMapping = [
        'endDate' => 'setEndDate',
        'startDate' => 'setStartDate'
    ];
    
    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function getDuration() {
        $timeDiff = date_diff($this->getEndDate(), $this->getStartDate());
        $minutes = $timeDiff->days * 24 * 60;
        $minutes += $timeDiff->h * 60;
        $minutes += $timeDiff->i;
        return $minutes;
    }

    protected function setStartDate($startDate) {
        $this->startDate = new \DateTime($startDate);
    }

    protected function setEndDate($endDate) {
        $this->endDate = new \DateTime($endDate);
    }

    public function isClosed() {
        return $this->getStatus() > 1;
    }
    
    public function getTimeLeft() {
        $endDate = $this->getEndDate();
        $timeLeftInSeconds = $endDate->getTimestamp() - time();
        return $timeLeftInSeconds < 0 ? 0 : $timeLeftInSeconds;
    }

    public function checkIfStop() {
        if ($this->getTimeLeft() === 0) {
            if ($this->getQuizCount() === 0) {
                $this->reserveToBeSettled();
                return;
            }
            parent::checkIfStop();
        }
    }
}