<?php

namespace Tournament\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Tournament\Repository\SitAndGoTournamentRepository")
 */
class SitAndGoTournament extends Tournament {

    public function __construct(array $data) {
        parent::__construct($data);
        $this->setMinPot($this->getBid() * $this->getUserLimit());
    }

    public function getPot() {
        return $this->getUserLimit() * $this->getBid();
    }

    protected function getPrizes() {
        $quizzesCount = count($this->getQuizzes());
        $userLimit = $this->getUserLimit();
        $fullCount = $quizzesCount < $userLimit ? $userLimit : $quizzesCount;
        return $this->getPrizeCalculator()->getPrizes($this->getPot(), $fullCount);
    }

}
