<?php

namespace Tournament\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Roztropek\Db\Entity\Base;
use User\Entity\User;
use Tournament\Calculator\Prize\Progressive;
use Tournament\Factory\PriceCalculator;
use Payment\Factory\Money as MoneyFactory;
use Roztropek\Db\Entity\Traits\Creatable;
use Roztropek\Db\Entity\Traits\Versionable as VersionableTrait;
use Roztropek\Db\Entity\Interfaces\Versionable as VersionableInterface;

/**
 * @ORM\Entity(repositoryClass="Tournament\Repository\TournamentRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({"1" = "SitAndGoTournament", "2" = "TimeTournament"})
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="Tournament")
 */
abstract class Tournament extends Base implements VersionableInterface {

    use Creatable;

    use VersionableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="integer") */
    protected $userLimit = 3;

    /** @ORM\Column(type="integer") */
    protected $treeType = 1;

    /** @ORM\Column(type="integer") */
    protected $bid = 100;

    /** @ORM\Column(type="integer") */
    protected $minPot = 100;

    /** @ORM\Column(type="integer") */
    protected $status = 1;

    /** @ORM\Column(type="string", length=20) */
    protected $winCurrency = 'playCoin';

    /** @ORM\Column(type="string", length=20) */
    protected $payCurrency = 'playCoin';

    /** @ORM\Column(type="integer") */
    protected $questionType = 1;

    /** @ORM\Column(type="datetime", nullable=true) */
    protected $endedAt;

    /** @ORM\Column(type="integer") */
    protected $questionNumber = 12;
    protected $timePerQuestion = 10;

    /**
     * @todo - jak będzie dobrze działał eager zrobić eager
     * @ORM\OneToMany(targetEntity="\Tournament\Entity\Quiz", mappedBy="tournament")
     * @ORM\OrderBy({"points" = "DESC", "time" = "ASC", "startDate" = "ASC"})
     * */
    protected $quizzes;

    /** @ORM\Column(type="integer") */
    protected $requireLvl = 1;
    private $settersMapping = [
        'bid' => 'setBid',
        'minPot' => 'setMinPot',
        'winCurrency' => 'setWinCurrency',
        'payCurrency' => 'setPayCurrency',
        'requireLvl' => 'setRequireLvl',
        'userLimit' => 'setUserLimit',
        'treeType' => 'setTreeType',
        'questionType' => 'setQuestionType'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->createdAt = new \DateTime();
    }

    public function getId() {
        return $this->id;
    }

    public function getUserLimit() {
        return $this->userLimit;
    }

    public function getBid() {
        return $this->bid;
    }

    public function getMinPot() {
        return $this->minPot;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getQuestionNumber() {
        return $this->questionNumber;
    }

    public function getRequireLvl() {
        return $this->requireLvl;
    }

    /**
     * @return ArrayCollection
     */
    public function getQuizzes() {
        if (!$this->quizzes) {
            $this->setQuizzes(new ArrayCollection());
        }
        return $this->quizzes;
    }

    public function getPot() {
        $minPot = $this->getMinPot();
        $pot = $this->getUserLimit() * $this->getBid();
        return $minPot > $pot ? $minPot : $pot;
    }

    public function getQuizCount() {
        return count($this->getQuizzes());
    }

    public function addQuiz(Quiz $quiz) {
        if (!$this->getQuizzes()->contains($quiz)) {
            $this->getQuizzes()->add($quiz);
            $this->lock();
            $this->markAsDirty();
        }
    }

    public function checkIfStop() {
        if ($this->getStatus() != 1) {
            return;
        }
        $quizzes = $this->getQuizzes();
        if ($quizzes->count() < $this->getUserLimit()) {
            return;
        }
        foreach ($this->getQuizzes() as $quiz) {
            $quiz->checkIfStart();
            $quiz->checkIfStop();
            if (!$quiz->isEnded()) {
                return;
            }
        }

        $this->reserveToBeSettled();
    }

    public function getPrizesTable() {
        $quizzes = [];
        $prizes = $this->getPrizes();
        $qq = $this->getQuizzes();
        foreach ($this->getQuizzes() as $quiz) {
            $prize = array_shift(($prizes));
            $quizzes[] = [
                'quiz' => $quiz,
                'prize' => MoneyFactory::create(['currency' => $this->getWinCurrency(), 'value' => $prize])
            ];
        }
        return $quizzes;
    }

    public function getUnfinishedQuizzes($idPlayer) {
        $quizzes = $idPlayer !== null ? $this->getPlayerQuizzes($idPlayer) : $this->getQuizzes();
        return $quizzes->filter(function($quiz) {
                    return !$quiz->isEnded();
                });
    }

    public function isPlayerPlayed($idPlayer) {
        $playerQuizzes = $this->getPlayerQuizzes($idPlayer);
        return (count($playerQuizzes) > 0);
    }

    /**
     * @param \User\Entity\User $player
     * @return bool
     */
    public function canPlayerJoin(User $player) {
        $this->checkIfStop();
        if ($this->isClosed()) {
            return false;
        }
        $playerQuizzes = $this->getPlayerQuizzes($player->getId());
        return $playerQuizzes->count() == 0 && $this->getQuizCount() < $this->getUserLimit();
    }

    public function getTotalTime() {
        return $this->getTimePerQuestion() * $this->getQuestionNumber();
    }

    public function getTimePerQuestion() {
        return $this->timePerQuestion;
    }

    public function isClosed() {
        return $this->getStatus() != 1;
    }

    /**
     * 
     * @param User $player
     * @return \Payment\ValueObject\Money\Money
     */
    public function getPrice(User $player = null) {
        $priceCalculator = $this->getPriceCalculator();
        if ($player) {
            $priceCalculator->setPlayer($player);
        }
        return $priceCalculator->getCalculatedPrice();
    }

    public function markAsSettled() {
        $this->setStatus(2);
    }

    public function getQuestionType() {
        return $this->questionType ?: 1;
    }

    public function setQuestionType($questionType) {
        $this->questionType = $questionType;
    }

    protected function getPriceCalculator() {
        $money = MoneyFactory::create([
                    'currency' => $this->getPayCurrency(),
                    'value' => $this->getBid()
        ]);
        return PriceCalculator::create([
                    'money' => $money
        ]);
    }

    public function getPlayerQuizzes($idPlayer) {
        return $this->getQuizzes()->filter(function($quiz) use ($idPlayer) {
                    return $quiz->getPlayer()->getId() == $idPlayer;
                });
    }

    protected function getPrizes() {
        return $this->getPrizeCalculator()->getPrizes($this->getPot(), count($this->getQuizzes()));
    }

    protected function getEndedAt() {
        return $this->endedAt;
    }

    public function getWinCurrency() {
        return $this->winCurrency;
    }

    public function getPayCurrency() {
        return $this->payCurrency;
    }

    public function reserveToBeSettled() {
        $this->setStatus(9);
        $this->setEndedAt(new \DateTime());
    }

    protected function setUserLimit($userLimit) {
        $this->userLimit = $userLimit;
    }

    protected function setType($type) {
        $this->type = $type;
    }

    protected function setTreeType($treeType) {
        $this->treeType = $treeType;
    }

    protected function setBid($bid) {
        $this->bid = $bid;
    }

    protected function setMinPot($minPot) {
        $this->minPot = $minPot;
    }

    protected function setStatus($status) {
        $this->status = $status;
    }

    protected function setPayCurrency($payCurrency) {
        $this->payCurrency = $payCurrency;
    }

    protected function setWinCurrency($winCurrency) {
        $this->winCurrency = $winCurrency;
    }

    protected function setQuestionNumber($questionNumber) {
        $this->questionNumber = $questionNumber;
    }

    protected function setRequireLvl($requireLvl) {
        $this->requireLvl = $requireLvl;
    }

    protected function setEndedAt(\DateTime $endedAt) {
        $this->endedAt = $endedAt;
    }

    protected function setUp(array $data) {
        parent::setUp($data);
        $this->runSetters($this->settersMapping, array_filter($data));
    }

    protected function getPrizeCalculator() {
        /**
         * Zaślepka, tu będą ładowane dobre strategie
         */
        return new Progressive(30, 1.72);
    }

    protected function findQuiz($idQuiz) {
        foreach ($this->getQuizzes() as $quiz) {
            if ($quiz->getId() == $idQuiz) {
                return $quiz;
            }
        }
        return null;
    }

}
