<?php

namespace Tournament\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Roztropek\Db\Entity\Base;
use Tournament\DataObject\QuizQuestion as QuizQuestionDataObject;

/**
 * @ORM\Entity(repositoryClass="Tournament\Repository\QuizQuestionRepository")
 * @ORM\Table(name="QuizQuestion")
 * @ORM\HasLifecycleCallbacks
 * */
class QuizQuestion extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Question\Entity\Question")
     * @ORM\JoinColumn(name="idQuestion", referencedColumnName="id")
     */
    protected $question;

    /**
     * @ORM\ManyToOne(targetEntity="Tournament\Entity\Quiz", inversedBy="questions")
     * @ORM\JoinColumn(name="idQuiz", referencedColumnName="id")
     */
    protected $quiz;

    /** @ORM\Column(type="integer") */
    protected $points = 0;

    /** @ORM\Column(type="string") */
    protected $questionText;

    /** @ORM\Column(type="string", nullable=true) */
    protected $answer;

    /** @ORM\Column(type="string", length=500) */
    protected $answers;

    /** @ORM\Column(type="string") */
    protected $correctAnswer;

    /** @ORM\Column(type="datetime") */
    protected $insertDate;

    /** @ORM\Column(type="datetime", nullable=true) */
    protected $answeredAt;
    private $settersMapping = [
        'question' => 'setQuestion',
        'quiz' => 'setQuiz',
        'answers' => 'setAnswers'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist() {
        $this->insertDate = new \DateTime();
    }

    public function getInsertDate() {
        return $this->insertDate;
    }

    public function getId() {
        return $this->id;
    }

    public function getQuestion() {
        return $this->question;
    }

    public function getQuiz() {
        return $this->quiz;
    }

    public function getPoints() {
        return $this->points;
    }

    public function getAnswer() {
        return $this->answer;
    }

    public function getAnswers() {
        return $this->answers;
    }

    public function getCorrectAnswer() {
        return $this->correctAnswer;
    }

    public function getQuestionText() {
        return $this->questionText;
    }

    protected function setQuestionText($questionText) {
        $this->questionText = $questionText;
    }

    protected function setCorrectAnswer($correctAnswer) {
        $this->correctAnswer = $correctAnswer;
    }

    protected function setQuestion(\Question\Entity\Question $question) {
        $this->question = $question;
        $this->setQuestionText($question->getText());
        $this->setCorrectAnswer($question->getCorrectAnswer());
    }

    protected function setQuiz(\Tournament\Entity\Quiz $quiz) {
        $this->quiz = $quiz;
    }

    public function setPoints($points) {
        $this->points = $points;
    }

    public function setAnswer($answer) {
        $this->answer = $answer;
        if (!$this->getAnsweredAt()) {
            $this->answeredAt = new \DateTime();
        }
    }

    public function getAnswerTime() {
        if (!$this->getAnsweredAt()) {
            return 0;
        }
        return $this->getAnsweredAt()->getTimestamp() - $this->getInsertDate()->getTimestamp();
    }

    public function getAnsweredAt() {
        return $this->answeredAt;
    }

    public function getQuestionId() {
        return $this->getQuestion()->getId();
    }

    protected function setAnswers($answers) {
        $this->answers = $answers;
    }

    protected function setUp(array $data) {
        parent::setUp($data);
        $this->runSetters($this->settersMapping, $data);
    }

}
