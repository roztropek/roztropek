<?php

namespace Tournament\Factory;

use Roztropek\Utils\Helper\ArrayHelper;

final class Tournament {

    public static function create(array $data) {
        $type = ArrayHelper::getKey('type', $data, '', 'string');
        switch ($type) {
            case 'time':
            case 'timeTournament':
                $class = '\Tournament\Entity\TimeTournament';
                break;
            case 'sitAndGo':
            case 'sitAndGoTournament':
                $class = '\Tournament\Entity\SitAndGoTournament';
            default:
                throw new \InvalidArgumentException('Nieprawidłowy typ!');
        }
        return new $class($data);
    }

}
