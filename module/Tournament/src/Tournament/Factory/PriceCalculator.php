<?php

namespace Tournament\Factory;

use Tournament\DataObject\PriceCalculator as PriceCalculatorDataObject;
use Tournament\Calculator\Price\Price as PriceCalculatorObject;
use Roztropek\Factory\Base;

final class PriceCalculator {

    public static function create(array $data) {
        return new PriceCalculatorObject($data['money']);
    }

}
