<?php

namespace Tournament;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'tournament' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/tournament',
                    'defaults' => array(
                        '__NAMESPACE__' => __NAMESPACE__ . '\Controller',
                        'controller' => __NAMESPACE__,
                        'action' => 'list',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'action' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/:controller[/:action[/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                                'id' => '[a-zA-Z0-9][a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'console' => [
        'router' => [
            'routes' => [
                'pay-winnings' => [
                    'options' => [
                        'route' => 'payWinnings',
                        'defaults' => [
                            'controller' => __NAMESPACE__ . '\Controller\Cli',
                            'action' => 'payWinnings'
                        ]
                    ]
                ],
                'fill-sit-and-go-tournaments' => [
                    'options' => [
                        'route' => 'fillSitAndGoTournaments',
                        'defaults' => [
                            'controller' => __NAMESPACE__ . '\Controller\Cli',
                            'action' => 'fillSitAndGoTournaments'
                        ]
                    ]
                ],
                'check-all-tournaments' => [
                    'options' => [
                        'route' => 'checkAllTournaments',
                        'defaults' => [
                            'controller' => __NAMESPACE__ . '\Controller\Cli',
                            'action' => 'checkAllTournaments'
                        ]
                    ]
                ]
            ]
        ]
    ],
    'controllers' => array(
        'invokables' => array(
            __NAMESPACE__ . '\Controller\\' . __NAMESPACE__ => __NAMESPACE__ . '\Controller\\' . __NAMESPACE__ . 'Controller',
            __NAMESPACE__ . '\Controller\Quiz' => __NAMESPACE__ . '\Controller\QuizController',
            __NAMESPACE__ . '\Controller\Cli' => __NAMESPACE__ . '\Controller\CliController',
        ),
    ),
    'view_manager' => array(
        'view_manager' => array(
            'strategies' => array(
                'ViewJsonStrategy',
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Quiz' => 'Tournament\Service\Quiz',
            'Tournament' => 'Tournament\Service\Tournament',
            'TimeTournament' => 'Tournament\Service\TimeTournament',
            'SitAndGoTournament' => 'Tournament\Service\SitAndGoTournament'
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_orm_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_orm_driver'
                )
            )
        )
    ),
    //payCurrency-winCurrency-userLimit-bid-questionType
    'defaultTournamentCount' => [
        'playCoin-playCoin-4-200-1' => 1,
        'playCoin-playCoin-4-500-1' => 1,
        'playCoin-playCoin-4-1000-1' => 1,
        'playCoin-playCoin-7-200-1' => 1,
        'playCoin-playCoin-7-500-1' => 1,
        'playCoin-playCoin-7-1000-1' => 1,
        'playCoin-playCoin-10-1000-1' => 1,
        'gold-gold-5-200-2' => 1,
        'gold-gold-5-500-2' => 1,
        'gold-gold-10-200-2' => 1,
        'gold-gold-10-500-2' => 1,
        
    ]
);
