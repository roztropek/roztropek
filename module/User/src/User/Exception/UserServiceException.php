<?php
namespace User\Exception;

use Roztropek\Exception\ActionFailedException;

class UserServiceException extends ActionFailedException
{
    const NICK_OR_EMAIL_TAKEN = 'NICK_OR_EMAIL_TAKEN';
}

