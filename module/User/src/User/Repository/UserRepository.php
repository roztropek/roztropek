<?php

namespace User\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Roztropek\Utils\Result\Result;
use Doctrine\ORM\AbstractQuery;

class UserRepository extends BaseRepository {

    public function get($id, $hydrationMode = AbstractQuery::HYDRATE_ARRAY) {
        $tableName = $this->getTableName();
        $query = $this->createQueryBuilder('user')
                ->leftJoin('user.wallet', 'w')
                ->where('user.id = :id')
                ->setParameters(['id' => $id])
                ->getQuery();
        $user = array_pop($query->getResult($hydrationMode));
        if ($user) {
            unset($user['password']);
        }
        return $user;
    }

    public function register($data) {
        $result = new Result();
        $data = array_intersect_key($data, array_flip(['nick', 'email', 'password']));
        $messages = [];
        if (array_key_exists('nick', $data)) {
            if ($this->findOneBy(array('nick' => $data['nick']))) {
                $result->setSuccess(Result::IS_NOT_VALID);
                $messages = $result->getMessages();
                $messages['nick'] = 'nick jest zajęty';
                $result->setMessages($messages);
            }
        }
        if (array_key_exists('email', $data)) {
            if ($this->findOneBy(array('email' => $data['email']))) {
                $result->setSuccess(Result::IS_NOT_VALID);
                $messages = $result->getMessages();
                $messages['email'] = 'email jest zajęty';
                $result->setMessages($messages);
            }
        }
        $userEntity = new \User\Entity\User();
        $userEntity->exchangeData($data);
        if ($result->isValid() && $userEntity->isValid()) {
            $this->getEntityManager()->persist($userEntity);
            $result->setResult($userEntity);
            return $result;
        }
        $result->setMessages(array_merge($messages, $userEntity->getMessages()));
        $result->setSuccess(Result::IS_NOT_VALID);
        return $result;
    }

    public function activateUser($idUser) {
        $userEntity = $this->find($idUser);
        $userEntity->setStatus(1);
        return 1;
    }

    public function getUserWithWallet($idUser) {
        $wallet = $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('user', 'wallet')
                        ->from('\User\Entity\User', 'user')
                        ->where('user.id = :idUser')
                        ->join('user.wallet', 'wallet')
                        ->setParameter('idUser', $idUser)
                        ->getQuery()
                        ->getResult();
        return array_pop($wallet);
    }

}
