<?php

namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payment\ValueObject\Money\Money;
use Payment\ValueObject\Money\PlayCoin;
use Payment\ValueObject\Money\Gold;
use Payment\Factory\Money as MoneyFactory;
use Roztropek\Db\Entity\Interfaces\Versionable as VersionableInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="Wallet")
 * */
class Wallet implements VersionableInterface {
    
    use \Roztropek\Db\Entity\Traits\Versionable;
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="integer") * */
    protected $playCoins;

    /** @ORM\Column(type="integer") * */
    protected $gold;
    
    public function __construct() {
        $this->playCoins = 0;
        $this->gold = 0;
    }
    
    
    public function getId() {
        return $this->id;
    }

    /**
     * @return \Payment\ValueObject\Money\PlayCoin
     */
    public function getPlayCoins() {
        return MoneyFactory::create([
                    'currency' => 'playCoin',
                    'value' => $this->playCoins
        ]);
    }

    /**
     * @return \Payment\ValueObject\Money\Gold
     */
    public function getGold() {
        return MoneyFactory::create([
                    'currency' => 'gold',
                    'value' => $this->gold
        ]);
    }

    /**
     * @param Money $money
     * @return boolean
     */
    public function hasEnoughMoney(Money $money) {
        $currentMoney = $this->getMoney($money->getCurrency());
        return $currentMoney->compareWith($money) >= 0;
    }

    /**
     * @param \Payment\ValueObject\Money\PlayCoin $playCoins
     */
    protected function setPlayCoins(PlayCoin $playCoins) {
        $this->playCoins = $playCoins->getValue();
    }

    /**
     * @param \Payment\ValueObject\Money\Gold $gold
     */
    protected function setGold(Gold $gold) {
        $this->gold = $gold->getValue();
    }

    /**
     * @param \Payment\ValueObject\Money\Ticket $tickets
     */
    protected function setTickets(Ticket $tickets) {
        $this->tickets = $tickets->getValue();
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     */
    protected function setMoney(Money $money) {
        $methodName = $this->prepareMoneyMethodName($money->getCurrency(), 'set');
        return $this->$methodName($money);
    }

    /**
     * @param string $currency
     * @return \Payment\ValueObject\Money\Money
     */
    protected function getMoney($currency) {
        $methodName = $this->prepareMoneyMethodName($currency, 'get');
        return $this->$methodName();
    }

    /**
     * @param string $currency
     * @param string $prefix
     * @return string
     * @throws \InvalidArgumentException
     */
    protected function prepareMoneyMethodName($currency, $prefix) {
        $methodPostfix = '';
        switch (mb_strtolower($currency, 'utf-8')) {
            case 'playcoin':
                $methodPostfix = 'PlayCoins';
                break;
            case 'gold':
                $methodPostfix = 'Gold';
                break;
            default:
                throw new \InvalidArgumentException('Zły rodzaj waluty');
        }
        return $prefix . $methodPostfix;
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     */
    public function substractMoney(Money $money) {
        $currentMoney = $this->getMoney($money->getCurrency());
        $this->setMoney($currentMoney->substract($money));
        $this->lock();
    }

    /**
     * @param \Payment\ValueObject\Money\Money $money
     */
    public function addMoney(Money $money) {
        $currentMoney = $this->getMoney($money->getCurrency());
        $this->setMoney($currentMoney->add($money));
        $this->lock();
    }

}
