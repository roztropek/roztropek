<?php

namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Payment\ValueObject\Money\Money;
use Roztropek\Db\Entity\Base;
use User\Exchanger\User as UserExchanger;

/**
 * @ORM\Entity(repositoryClass="User\Repository\UserRepository")
 * @ORM\Table(name="User")
 * */
class User extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="string") * */
    protected $email;

    /** @ORM\Column(type="string") * */
    protected $password;

    /** @ORM\Column(type="string") * */
    protected $nick;

    /** @ORM\Column(type="integer") * */
    protected $experience = 0;

    /** @ORM\Column(type="integer") * */
    protected $status = 0;

    /** @ORM\Column(type="string" ,nullable=true, name="birth_date") * */
    protected $birthDate;

    /** @ORM\Column(type="smallint", nullable=true) * */
    protected $sex;

    /** @ORM\Column(type="string", nullable=true, length=1023 ) * */
    protected $description;

    /** @ORM\Column(type="string", nullable=true, length=50 ) * */
    protected $bankAccount;

    /**
     * @ORM\OneToOne(targetEntity="\User\Entity\Wallet", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="idWallet", referencedColumnName="id")
     * */
    protected $wallet;

    /**
     * @ORM\OneToOne(targetEntity="\User\Entity\User")
     * @ORM\JoinColumn(name="idRef", referencedColumnName="id")
     * */
    protected $referer;

    /** @ORM\Column(type="string", nullable=true, length=30)  */
    protected $name;

    /** @ORM\Column(type="string", nullable=true, length=100)  */
    protected $surname;

    /** @ORM\Column(type="string", nullable=true)  */
    protected $avatar;
    protected $purePassword;
    private $settersMapping = [
        'email' => 'setEmail',
        'password' => 'setPassword',
        'birthDate' => 'setBirthDate',
        'referer' => 'setReferer',
        'name' => 'setName',
        'surname' => 'setSurname',
        'avatar' => 'setAvatar',
        'birthDate' => 'setBirthDate',
        'sex' => 'setSex',
        'description' => 'setDescription',
        'nick' => 'setNick',
        'bankAccount' => 'setBankAccount'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function __construct(array $data) {
        $data['status'] = 0;
        $data['experience'] = 0;
        $data['avatar'] = rand(1, 6);
        $this->setWallet(new Wallet());
        parent::__construct($data);
    }

    public function getId() {
        return $this->id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getNick() {
        return $this->nick;
    }

    public function getExperience() {
        return $this->experience;
    }

    public function getStatus() {
        return $this->status;
    }

    public function getReferer() {
        return $this->referer;
    }

    public function getBankAccount() {
        return $this->bankAccount;
    }

    public function getName() {
        return $this->name;
    }

    public function getSurname() {
        return $this->surname;
    }

    /**
     * @return \User\Entity\Wallet
     */
    public function getWallet() {
        return $this->wallet;
    }

    public function getAvatar() {
        return $this->avatar ?: 1;
    }

    public function getBirthDate() {
        return $this->birthDate;
    }

    public function getSex() {
        return $this->sex;
    }

    public function getDescription() {
        return $this->description;
    }

    protected function getExchanger() {
        if (!$this->exchanger) {
            $this->setExchanger(new UserExchanger());
        }
        return $this->exchanger;
    }

    protected function setBirthDate($birthDate) {
        $this->birthDate = $birthDate;
    }

    protected function setSex($sex) {
        $this->sex = $sex;
    }

    protected function setDescription($description) {
        $this->description = $description;
    }

    protected function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    protected function setWallet(Wallet $wallet) {
        $this->wallet = $wallet;
    }

    protected function setEmail($email) {
        $this->email = $email;
    }

    protected function setPassword($password) {
        if ($password != null) {
            $this->purePassword = $password;
            $this->password = sha1($password);
        }
    }

    protected function setNick($nick) {
        $this->nick = $nick;
    }

    protected function setExperience($experience) {
        $this->experience = $experience;
    }

    protected function setStatus($status) {
        $this->status = $status;
    }

    protected function setBankAccount(string $bankAccount) {
        $this->bankAccount = $bankAccount;
    }

    public function setReferer(\User\Entity\User $referer) {
        $this->referer = $referer;
    }

    protected function setName($name) {
        $this->name = $name;
    }

    protected function setSurname($surname) {
        $this->surname = $surname;
    }

    /**
     * @return alias for getNick
     */
    public function getLogin() {
        return $this->getNick();
    }

    public function getMessages() {
        $messages = parent::getMessages();
        if (array_key_exists('purePassword', $messages)) {
            $messages['password'] = $messages['purePassword'];
            unset($messages['purePassword']);
        }
        return $messages;
    }

    public function pay(Money $money) {
        $this->getWallet()->substractMoney($money);
    }

    public function recharge(Money $money) {
        $this->getWallet()->addMoney($money);
    }

    public function hasEnoughMoney(Money $money) {
        return $this->getWallet()->hasEnoughMoney($money);
    }

    public function exchangeData(array $data) {
        $exchanger = $this->getExchanger();
        $exchanger->getExchangeData($data);
        $this->runSetters($this->settersMapping, $data);
    }

    /**
     * zaslepka, zwraca aktualny poziom gracza, narazie zawsze 1 :)
     * @return int
     */
    public function getLvl() {
        return 1;
    }

    public function activate() {
        $this->setStatus(1);
    }

}
