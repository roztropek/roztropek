<?php

namespace User\Form;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\FileInput;
use Zend\Form\Element;
use Zend\Form\Form;
use Roztropek\Config;

class UploadAvatarForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements() {
        $file = new Element\File('avatar');
        $this->add($file);
    }

    public function addInputFilter() {
        $inputFilter = new InputFilter();

        $fileInput = new FileInput('avatar');
        $fileInput->setRequired(true);
        $inputFilter->add($fileInput);

        $fileInput->getValidatorChain()
                ->attach(new \Zend\Validator\File\Extension(['jpeg', 'jpg', 'png']))
                ->attach(new \Zend\Validator\File\IsImage())
                ->attach(new \Zend\Validator\File\ImageSize(['minWidth' => 100, 'minHeight' => 100]))
                ->attach(new \Zend\Validator\File\Size(['min' => '2kB', 'max' => '200kb']));

        $this->setInputFilter($inputFilter);
    }

}
