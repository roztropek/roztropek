<?php

namespace User\Extractor;

class User {

    /**
     * @param \User\Entity\User $userEntity
     * @return array
     */
    public static function extract(\User\Entity\User $userEntity, $strategy = 'simple') {
        switch ($strategy) {
            case 'full':
                return static::fullExtract($userEntity);
                break;
            case 'simple':
                return self::simpleExtract($userEntity);
                break;
            default:
                return self::simpleExtract($userEntity);
                break;
        }
    }

    protected static function simpleExtract(\User\Entity\User $userEntity) {
        $avatarUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/avatar';
        $avatarFilename = sprintf('avatars_%d.png', $userEntity->getAvatar());
        $avatar = [
            'id' => $userEntity->getAvatar(),
            'path' => [
                'tiny' => $avatarUrl . '/' . $avatarFilename,
                'min' => $avatarUrl . '/' . $avatarFilename,
                'normal' => $avatarUrl . '/' . $avatarFilename,
                'natural' => $avatarUrl . '/' . $avatarFilename
        ]];
        return [
            'id' => $userEntity->getId(),
            'nick' => $userEntity->getNick(),
            'experience' => $userEntity->getExperience(),
            'avatar' => $avatar
        ];
    }

    protected static function fullExtract(\User\Entity\User $userEntity) {
        $data = static::simpleExtract($userEntity);
        $additionalData = [
            'email' => $userEntity->getEmail(),
            'wallet' => static::extractWallet($userEntity->getWallet()),
            'name' => $userEntity->getName(),
            'surname' => $userEntity->getSurname(),
            'bankAccount' => $userEntity->getBankAccount()
        ];
        return array_merge($data, $additionalData);
    }

    protected static function extractWallet(\User\Entity\Wallet $walletEntity) {
        return [
            'playCoin' => $walletEntity->getPlayCoins()->getValue(),
            'gold' => $walletEntity->getGold()->getValue(),
            'canPayout' => $walletEntity->getPlayCoins()->getValue() >= 2500,
            'maxPossiblePayout' => $walletEntity->getPlayCoins()->getValue() >= 2500 ? $walletEntity->getPlayCoins()->getValue() : 0,
            'minPayout' => 2500
        ];
    }
}
