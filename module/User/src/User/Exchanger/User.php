<?php

namespace User\Exchanger;

use Roztropek\Exchanger\Base;

class User extends Base {

    private $inputFilterConfig = [
        'email' => [
            'required' => true,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 200,
                        'message' => 'Email musi mieć więcej niż 3 a mniej niż 200 znaków'
                    ]
                ],
                ['name' => 'emailaddress',
                    'options' => [
                        'domain' => false,
                        'message' => 'Nieprawidłowy adres email'
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ],
        ],
        'password' => [
            'required' => true,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 5,
                        'max' => 25,
                        'message' => 'Hasło musi mieć pomiędzy 5 a 25 znakóœ'
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'nick' => [
            'required' => true,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 20,
                        'message' => 'Nick musi zawierać między 3 a 20 znaków'
                    ]
                ],
                ['name' => 'regex',
                    'options' => [
                        'pattern' => '/^[0-9a-zA-Z_-]+$/',
                        'message' => 'Nick musi składać się z liter lub cyfr lub znaków "-","_"'
                    ],
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ],
        ],
        'name' => [
            'required' => false,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 25,
                        'message' => 'Imię musi mieć więcej niż 2 a mniej niż 25 znaków'
                    ]
                ],
                ['name' => 'regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z]+$/',
                        'message' => 'Imię musi składać się z liter'
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'surname' => [
            'required' => false,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 25,
                        'message' => 'Nazwisko musi mieć więcej niż 2 a mniej niż 25 znaków'
                    ]
                ],
                ['name' => 'regex',
                    'options' => [
                        'pattern' => '/^[a-zA-Z]+$/',
                        'message' => 'Nazwisko musi składać się z liter'
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'experience' => [
            'required' => true,
            'validators' => [
                ['name' => 'greaterThan',
                    'options' => [
                        'min' => 0,
                        'inclusive' => true
                    ]
                ]
            ]
        ],
        'status' => [
            'required' => true,
            'validators' => [
                ['name' => 'between',
                    'options' => [
                        'inclusive' => true,
                        'min' => 0,
                        'max' => 1
                    ]
                ]
            ],
        ],
        'birthDate' => [
            'required' => false,
            'validators' => [
                ['name' => 'date']
            ],
        ],
        'sex' => [
            'required' => false,
            'validators' => [
                ['name' => 'between',
                    'options' => [
                        'inclusive' => true,
                        'min' => 0,
                        'max' => 1
                    ]
                ]
            ]
        ],
        'description' => [
            'required' => false,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 0,
                        'max' => 1000,
                        'inclusive' => true,
                        'message' => 'Opis może mieć maksymalnie 1000 znaków'
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'avatar' => [
            'required' => false,
            'validators' => [
                ['name' => 'between',
                    'options' => [
                        'inclusive' => true,
                        'min' => 1,
                        'max' => 6
                    ]
                ]
            ]
        ],
        'referer' => [
            'required' => false
        ],
        'bankAccount' => [
            'required' => false,
            'filters' => [
                ['name' => 'pregreplace',
                    'options' => [
                        'pattern' => '/\s+/',
                        'replacement' => ''
                    ]],
            ],
            'validators' => [
                [
                    'name' => 'iban',
                    'options' => [
                        'country_code' => 'PL',
                        'message' => 'Nieprawidłowy numer konta'
                    ]
                ]
            ]
        ]
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
