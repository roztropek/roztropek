<?php
namespace User\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Roztropek\Service\CrudEntity;
use Roztropek\Utils\Result\Result;
use User\Factory\User as UserFactory;
use User\Entity\User as UserEntity;
use User\Exception\UserServiceException;

class User extends CrudEntity {

    protected $entityClass = '\User\Entity\User';

    public function create(array $data) {
        $qb = $this->getEntityRepository()->createQueryBuilder('u')
                ->where('u.email = :email')
                ->orWhere('u.nick = :nick')
                ->setMaxResults(1);
        $result = $qb->getQuery()->execute([
            'email' => $data['email'],
            'nick' => $data['nick']
        ]);

        if ($result) {
            $exception = new UserServiceException('Nick lub email są zajęte');
            $exception->setReason(UserServiceException::NICK_OR_EMAIL_TAKEN);
            throw $exception;
        }

        $entityClass = $this->getEntityClass();
        $userEntity = new $entityClass($data);
        $this->getEntityManager()->persist($userEntity);
        return $userEntity;
    }
    
    public function activate($idUser) {
        $userEntity = $this->find($idUser);
        $userEntity->activate();
    }
    
    public function changePassword(UserEntity $userEntity, $password) {
        $userEntity->exchangeData(['password' => $password]);
    }

}
