<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use User\Extractor\User as UserExtractor;
use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Utils\Hash\Transformer;
use Roztropek\Utils\Helper\ArrayHelper;
use \Roztropek\Exception\ActionFailedException;
use Roztropek\Exception\IsNotValidException;
use \User\Exchanger\User as UserExchanger;

class UserController extends ApiController {

    public function editAction() {
        $id = $this->params('id', false);
        if ($id === false) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('success' => false));
        }

        $identity = $this->getIdentity();
        $data = $this->getFilteredData(['name', 'surname', 'birthDate', 'sex', 'description', 'avatar']);

        if ($this->isIdentityAdmin() || $this->isIdentityPlayer() && $identity['idUser'] == $id) {
            $result = $this->getService('User')->edit($id, $data);
            $this->getResponse()->setStatusCode($result->getSuccess());
            $success = $result->isValid();
            if ($success == true) {
                $response = new JsonModel(array('success' => $success));
                return $this->finalize($response);
            }
            return new JsonModel(array('success' => false, 'messages' => $result->getMessages()));
        }
        $this->getResponse()->setStatusCode(401);
        return new JsonModel(array('success' => false));
    }

    public function getAction() {
        $id = $this->params('id', false);
        if ($id === false) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('success' => false));
        }
        $result = $this->getService('User')->get($id);
        if (!$result->isValid()) {
            $this->getResponse()->setStatusCode($result->getSuccess());
            return new JsonModel(array('success' => false));
        }

        $user = $result->getResult();
        $extractStrategy = 'simple';
        if ($id == $this->getIdentity()['idUser']) {
            $extractStrategy = 'full';
        }

        $userData = UserExtractor::extract($user, $extractStrategy);
        $response = new JsonModel(array('success' => true, 'result' => $userData));
        return $this->finalize($response);
    }

    public function getDetailsAction() {
        $idUser = $this->getIdentity()['idUser'];
        $result = $this->getService('User')->get($idUser);
        $user = $result->getResult();
        $userData = \User\Extractor\User::extract($user, 'full');
        $response = new JsonModel(array('success' => true, 'result' => $userData));
        return $this->finalize($response);
    }

    public function registerAction() {
        $data = $this->getFilteredData(['email', 'password', 'nick']);
        $inputFilter = new UserExchanger();
        try {
            $dataToCreate = $inputFilter->getExchangeData($data);
        } catch (IsNotValidException $ex) {
            throw new ActionFailedException($ex->getMessagesAsString());
        }

        $userEntity = $this->getService('User')->create($data);

        $idResult = $userEntity->getId();
        $activationCode = Transformer::encode($idResult, 'activcode');
        $this->getService('Mailer')->send('register', ['to' => $userEntity->getEmail()], ['activateCode' => $activationCode, 'userName' => $userEntity->getLogin()]);
        $response = new JsonModel(array('success' => true, 'id' => $idResult));
        return $this->finalize($response);
    }

    public function activateAction() {
        $userCode = $this->params()->fromRoute('activateCode', '');
        $idUser = \Roztropek\Utils\Hash\Transformer::decode($userCode, 'activcode');
        if ($idUser !== false) {
            $userEntity = $this->getService('User')->find($idUser);
            if ($userEntity) {
                $this->getService('User')->activate($userEntity);
                $response = new JsonModel(['success' => true]);
                return $this->finalize($response);
            }
        }

        return new JsonModel(['success' => false]);
    }

    public function resetPasswordRequestAction() {
        $criteria = array_filter($this->getFilteredData(['email', 'nick']));
        if (!empty($criteria)) {
            $userEntity = $this->getService('User')->findOneBy($criteria);
            if ($userEntity) {
                $extractedUser = UserExtractor::extract($userEntity);
                $token = sha1(microtime());
                $redisClient = $this->getService('Redis')->getClient();
                $redisToken = sprintf('reset_password_request_%s', $token);
                $redisClient->set($redisToken, $userEntity->getId());
                $redisClient->expire($redisToken, 3600 * 25);

                $this->getService('Mailer')->send('resetPasswordRequest', [
                    'to' => $userEntity->getEmail(),
                    'fromName' => 'Roztropek'
                        ], [
                    'token' => $token,
                    'nick' => $extractedUser['nick'],
                    'avatar' => $extractedUser['avatar']['path']['normal']
                        ]
                );

                $response = new JsonModel([
                    'success' => true
                ]);
                return $response;
            }
        }
        $response = new JsonModel([
            'success' => false,
            'messages' => ['user' => 'user not found']
        ]);
        return $response;
    }

    public function changePasswordAction() {
        return $this->getIdentity() ? $this->changePasswordAuthenticated() : $this->changePasswordUnauthenticated();
    }

    public function changeBankAccountAction() {
        $data = $this->getFilteredData(['bankAccount', 'password']);
        $account = ArrayHelper::getValue('bankAccount', $data, null, 'string');
        $password = ArrayHelper::getValue('password', $data, '', 'string');

        if ($account === null) {
            throw new \Exception('Nieprawidłowa wartość parametru bankAccount');
        }

        $idUser = $this->getIdentity()['idUser'];
        $userEntity = $this->getService('User')->findOneBy([
            'id' => $idUser,
            'password' => sha1($password)
        ]);

        if (!$userEntity) {
            throw new ActionFailedException('Podane hasło jest nieprawidłowe');
        }

        $userExchanger = new UserExchanger();
        try {
            $dataToExchange = $userExchanger->getExchangeData(['bankAccount' => $account]);
        } catch (IsNotValidException $ex) {
            throw new ActionFailedException($ex->getMessagesAsString());
        }

        $this->getService('User')->edit($idUser, $dataToExchange);

        return $this->finalize(new JsonModel(['success' => true]));
    }

    protected function changePasswordAuthenticated() {
        $data = $this->getFilteredData(['oldPassword', 'password']);
        $oldPassword = ArrayHelper::getKey('oldPassword', $data, null, 'string');
        $password = ArrayHelper::getKey('password', $data, null, 'string');
        $userExchanger = new UserExchanger();

        if ($oldPassword && $password) {
            $idUser = $this->getIdentity()['idUser'];
            $userEntity = $this->getService('User')->findOneBy([
                'id' => $idUser,
                'password' => sha1($oldPassword)
            ]);
            if (!$userEntity) {
                throw new ActionFailedException('Stare hasło jest nieprawidłowe');
            }

            try {
                $userExchanger->getExchangeData(['password' => $password]);
            } catch (IsNotValidException $ex) {
                throw new ActionFailedException($ex->getMessagesAsString());
            }

            $this->getService('User')->changePassword($userEntity, $password);
            return $this->finalize(new JsonModel(['success' => true]));
        }
        return $this->finalize(new JsonModel(['success' => false]));
    }

    protected function changePasswordUnauthenticated() {
        $data = $this->getFilteredData(['token', 'password']);
        $token = ArrayHelper::getKey('token', $data, null, 'string');
        $password = ArrayHelper::getKey('password', $data, null, 'string');
        if ($token && $password) {
            $redisClient = $this->getService('Redis')->getClient();
            $userId = $redisClient->get(sprintf('reset_password_request_%s', $token));
            if ($userId) {
                $userExchanger = new UserExchanger();
                try {
                    $userExchanger->getExchangeData(['password' => $password]);
                } catch (IsNotValidException $ex) {
                    throw new ActionFailedException($ex->getMessagesAsString());
                }
                $userEntity = $this->getService('User')->findOneBy(['id' => $userId]);
                if ($userEntity) {
                    $this->getService('User')->changePassword($userEntity, $password);
                    $redisClient->del(sprintf('reset_password_request_%s', $token));
                    return $this->finalize(new JsonModel(['success' => true]));
                }
            }
        }
        return $this->finalize(new JsonModel(['success' => false]));
    }

}
