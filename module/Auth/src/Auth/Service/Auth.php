<?php

namespace Auth\Service;

use Zend\Validator\EmailAddress;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\AuthenticationService as AuthenticationService;
use \Roztropek\Service\Base;

class Auth extends Base implements FactoryInterface, \Zend\Authentication\AuthenticationServiceInterface {

    const TOKEN_TTL = 900;

    public $serviceLocator;

    /**
     * @var \Zend\Authentication\AuthenticationService 
     */
    public $authService;

    /**
     *
     * @var string
     */
    protected $tokenHeaderName = 'x-token';

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        $this->authService = new AuthenticationService(new \Zend\Authentication\Storage\NonPersistent(), new \Auth\Adapter\Adapter($this->getTokenKey($this->getXToken()), $this->getService('Redis')->getClient()));
        return $this;
    }

    public function getRequest() {
        return $this->serviceLocator->get('Request');
    }

    public function createToken($entity) {
        $redis = $this->getService('Redis')->getClient();
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $role = 'guest';
        if ($entity instanceof \User\Entity\User) {
            $role = 'player';
        } else if ($entity instanceof \Admin\Entity\Administrator) {
            $role = 'administrator';
        }
        $idUser = $entity->getId();
        $login = $entity->getLogin();

        $identity = [
            'role' => $role,
            'login' => $login,
            'idUser' => $idUser
        ];

        $tokenKey = $this->getTokenKey($token);

        $redis->set($tokenKey, json_encode($identity));
        $redis->expire($tokenKey, self::TOKEN_TTL);
        return [
            'token' => $token,
            'identity' => $identity
        ];
    }

    public function logout() {
        if ($this->clearIdentity()) {
            $this->getService('Redis')->getClient()->del($this->getTokenKey($this->getXToken()));
            return true;
        }
        return false;
    }

    public function clearIdentity() {
        if ($this->hasIdentity()) {
            $this->authService->clearIdentity();
            return true;
        }
        return false;
    }

    public function authenticate() {
        $result = $this->authService->authenticate();
        if ($result->isValid()) {
            $this->getService('Redis')->getClient()->expire($this->getTokenKey($this->getXToken()), self::TOKEN_TTL);
        }
    }

    public function getIdentity() {
        $identity = $this->authService->getIdentity();
        return $identity;
    }

    public function hasIdentity() {
        return $this->authService->hasIdentity();
    }

    public function getRole() {
        return !$this->hasIdentity() ? 'guest' : $this->getIdentity()['role'];
    }

    public function getFreshIdentity() {
        if ($this->hasIdentity()) {
            $entityManager = $this->serviceLocator->get('doctrine.entitymanager.orm_default');
            $idUser = $this->getIdentity()['idUser'];
            switch ($this->getRole()) {
                case 'administrator':
                    return $entityManager->getRepository('\Admin\Entity\Administrator')->find($idUser);
                    break;
                case 'player':
                    return $entityManager->getRepository('\User\Entity\User')->getUserWithWallet($idUser);
                    break;
                default:
                    return false;
                    break;
            }
        }
        return false;
    }

    public function getAuthenticatedUser() {
        return $this->getFreshIdentity();
    }

    /**
     * @return string
     */
    protected function getXToken() {
        $header = $this->getRequest()->getHeader($this->getTokenHeaderName(), false);
        return $header ? $header->getFieldValue() : false;
    }

    protected function getTokenKey($token) {
        return sprintf('token_%s', $token);
    }

    /**
     * @param string $tokenHeaderName
     * @return \Auth\Service\Auth
     */
    public function setTokenHeaderName($tokenHeaderName) {
        $this->tokenHeaderName = $tokenHeaderName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTokenHeaderName() {
        return $this->tokenHeaderName;
    }

}

?>
