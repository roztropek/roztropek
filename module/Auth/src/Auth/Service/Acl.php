<?php

namespace Auth\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Acl implements FactoryInterface {

    public $serviceLocator;

    /**
     * @var \Zend\Authentication\AuthenticationService 
     */
    public $authService;

    /**
     * @var \Zend\Permissions\Acl\Acl
     */
    protected $acl;

    public function createService(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
        $this->acl = new \Zend\Permissions\Acl\Acl();
        $config = $this->getApplicaitonConfig();
        $this->makeResources();
        $this->makeRoles();
        $this->makePrivilages();
        return $this;
    }

    /**
     * @return \Zend\Permissions\Acl\Acl
     */
    protected function getAcl() {
        return $this->acl;
    }

    /**
     * @param \Zend\Permissions\Acl\Acl $acl
     */
    protected function setAcl(\Zend\Permissions\Acl\Acl $acl) {
        $this->acl = $acl;
    }

    /**
     * @return \Auth\Service\Auth
     */
    protected function getAuthService() {
        return $this->serviceLocator->get('Auth');
    }

    /**
     * @return \Auth\Service\Acl
     */
    protected function makeResources() {
        $config = $this->getApplicaitonConfig();
        foreach ($config['controllers']['invokables'] as $resource => $controllerName) {
            $aclResource = self::getResourceFromControllername($resource);
            $this->getAcl()->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($aclResource));
        }
        return $this;
    }

    /**
     * @return \Auth\Service\Acl
     */
    protected function makeRoles() {
        $config = $this->getApplicaitonConfig();
        foreach ($config['roles'] as $role => $parents) {
            $this->getAcl()->addRole(new \Zend\Permissions\Acl\Role\GenericRole($role), $parents);
        }
        return $this;
    }

    /**
     * @return \Auth\Service\Acl
     */
    protected function makePrivilages() {
        $config = $this->getApplicaitonConfig();
        $acl = $this->getAcl();
        $acl->deny();
        foreach ($config['permissions'] as $role => $permissions) {
            $aclPermissions = array();
            if (array_key_exists('allow', $permissions) && is_array($permissions['allow'])) {
                $aclPermissions['allow'] = $permissions['allow'];
            }
            if (array_key_exists('deny', $permissions) && is_array($permissions['deny'])) {
                $aclPermissions['deny'] = $permissions['deny'];
            }
            foreach ($aclPermissions as $action => $aclPrivilages) {
                foreach ($aclPrivilages as $resource => $privilages) {
                    if ($acl->hasResource($resource)) {
                        $acl->$action($role, $resource, $privilages === '*' ? null : $privilages);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    protected function getApplicaitonConfig() {
        return $this->serviceLocator->get('Config');
    }

    /**
     * @param string $resource
     * @param string $privilage
     * @return \Auth\Authorization\Result(\Auth\Authorization\Result
     */
    public function isAllowed($resource, $privilage = null) {

        $request = $this->serviceLocator->get('Request');
        if ($request->isOptions()) {
            return new \Auth\Authorization\Result(\Auth\Authorization\Result::SUCCESS);
        }

        if (!$this->getAcl()->hasResource($resource)) {
            return new \Auth\Authorization\Result(\Auth\Authorization\Result::RESOURCE_NOT_FOUND);
        }

        $role = $this->getAuthService()->getRole();

        $isAllowed = $this->getAcl()->isAllowed($this->getAuthService()->getRole(), $resource, $privilage);

        if ($isAllowed === true) {
            return new \Auth\Authorization\Result(\Auth\Authorization\Result::SUCCESS);
        }

        if ($this->getAuthService()->hasIdentity()) {
            return new \Auth\Authorization\Result(\Auth\Authorization\Result::FORBIDDEN);
        }
        return new \Auth\Authorization\Result(\Auth\Authorization\Result::UNAUTHORIZED);
    }

    /**
     * @param string $controllerName
     * @return string
     */
    public static function getResourceFromControllername($controllerName) {
        return strtolower(str_ireplace('\Controller', '', $controllerName));
    }

}

?>
