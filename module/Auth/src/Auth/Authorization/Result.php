<?php

namespace Auth\Authorization;

class Result {

    const FORBIDDEN = 403;
    const UNAUTHORIZED = 401;
    const RESOURCE_NOT_FOUND = 404;
    const SUCCESS = 200;

    /**
     * @var int 
     */
    protected $result;

    /**
     * @var mixed
     */
    protected $messages;

    public function __construct($result, $messages = null) {
        $this->setResult($result);
        $this->setMessages($messages);
    }

    /**
     * @return boolean
     */
    public function isAuthorized() {
        return $this->result === self::SUCCESS;
    }

    /**
     * @return int
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * @return mixed
     */
    public function getMessages() {
        return $this->messages;
    }

    /**
     * @param int $result
     * @return \Auth\Authorization\Result
     */
    public function setResult($result) {
        $this->result = (int) $result;
        return $this;
    }

    /**
     * @param mixed $messages
     * @return \Auth\Authorization\Result
     */
    public function setMessages($messages) {
        $this->messages = $messages;
        return $this;
    }

}
