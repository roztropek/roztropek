<?php

namespace Auth\Adapter;

use Zend\Authentication\Result;

class Adapter implements \Zend\Authentication\Adapter\AdapterInterface {

    /**
     * @var \Predis\Client
     */
    protected $redisClient;

    /**
     * @var strint
     */
    protected $token;

    /**
     * @param \Zend\Http\Request | null $request
     * @param \Predis\Client $client
     */
    public function __construct($token = null, $client = null) {
        if ($token) {
            $this->setToken($token);
        }
        $this->setRedisClient($client);
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate() {
        $token = $this->getToken();
        if ($token) {
            $tokenRow = $this->getRedisClient()->get($token);
            if ($tokenRow) {
                return new Result(Result::SUCCESS, json_decode($tokenRow, true));
            }
        }

        return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null);
    }

    /**
     * @return string | null
     */
    public function getToken() {
        return $this->token;
    }

    /**
     * 
     * @param string $token
     * @return \Auth\Adapter\Adapter
     */
    public function setToken($token) {
        $this->token = $token;
        return $this;
    }
    
    public function getRedisClient() {
        return $this->redisClient;
    }

    public function setRedisClient(\Predis\Client $redisClient) {
        $this->redisClient = $redisClient;
    }
}
