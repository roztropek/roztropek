<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Roztropek\Exception\ActionFailedException;
use User\Extractor\User as UserExtractor;
use Admin\Extractor\Administrator as AdministratorExtractor;

class AuthController extends ApiController {

    public function loginAction() {
        $data = $this->getData();
        if ($data && array_key_exists('login', $data) && array_key_exists('password', $data)) {
            $type = $this->params()->fromRoute('type', 'player');
            $repository = $this->getIdentityRepository($type);

            $criteria = $this->getLoginCriteria();
            if (!$criteria) {
                return new JsonModel(array('success' => false));
            }
            $entity = $repository->findOneBy($criteria);
            if (!$entity) {
                throw new ActionFailedException('Nieprawidłowy login lub hasło');
            }

            $authService = $this->getService('Auth');
            $token = $authService->createToken($entity);
            switch (true) {
                case $entity instanceof \User\Entity\User:
                    $extractedUser = UserExtractor::extract($entity, 'full');
                    break;
                case $entity instanceof \Admin\Entity\Administrator:
                    $extractedUser = AdministratorExtractor::extract($entity);
                    break;
            }


            $token['identity'] = array_merge($token['identity'], $extractedUser);
            $response = new JsonModel(array(
                'success' => true,
                'token' => $token
            ));
            return $this->finalize($response);
        }
    }

    public function logoutAction() {
        $authService = $this->getServiceLocator()->get('Auth');
        $result = $authService->logout();
        return new JsonModel(array('success' => $result));
    }

    public function getIdentityAction() {
        $authService = $this->getServiceLocator()->get('Auth');
        $identity = $authService->getIdentity();
        if ($identity && $identity['role'] === 'player') {
            $freshIdentity = $authService->getFreshIdentity();
            $extractedUser = UserExtractor::extract($freshIdentity, 'full');
            $identity = array_merge($identity, $extractedUser);
        }

        return new JsonModel(array(
            'success' => true,
            'identity' => $identity
        ));
    }

    protected function getIdentityRepository($type) {
        switch ($type) {
            case 'player':
                $class = '\User\Entity\User';
                break;
            case 'administrator':
                $class = '\Admin\Entity\Administrator';
                break;
            default:
                $class = '\User\Entity\User';
                break;
        }

        return $this->getEntityManager()->getRepository($class);
    }

    protected function getLoginCriteria() {
        $data = $this->getData();
        if (array_key_exists('login', $data) && array_key_exists('password', $data)) {
            $login = (string) $data['login'];
            $password = sha1((string) $data['password']);
            $type = $this->params()->fromRoute('type', 'player');

            $emailValidator = new \Zend\Validator\EmailAddress();

            switch ($type) {
                case 'player':
                    $criteria = $emailValidator->isValid($login) ? array('email' => $login) : array('nick' => $login);
                    $criteria['status'] = 1;
                    break;
                case 'administrator':
                    $criteria = array('login' => $login);
                    break;
                default:
                    $criteria = ['bad' => 'criteria'];
            }

            if ($criteria !== false) {
                $criteria['password'] = $password;
            }
            return $criteria;
        }
        return false;
    }

}
