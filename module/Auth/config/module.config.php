<?php

namespace Auth;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'auth' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/auth',
                    'defaults' => array(
                        '__NAMESPACE__' => __NAMESPACE__ . '\Controller',
                        'controller' => __NAMESPACE__,
                        'action' => 'login',
                        'type' => 'player'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'action' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/:action',
                            'constraints' => array(
//                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
//                                'controller' => 'Question'
                            ),
                        ),
                    ),
//                    'controllerAction' => array(
//                        'type' => 'Segment',
//                        'options' => array(
//                            'route' => '/:controller/:action',
//                            'constraints' => array(
//                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                            ),
//                            'defaults' => array(
//                            ),
//                        ),
//                    ),
                    'admin' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/admin/:action',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                'type' => 'administrator'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            __NAMESPACE__ . '\Controller\\' . __NAMESPACE__ => __NAMESPACE__ . '\Controller\\' . __NAMESPACE__ . 'Controller'
        ),
    ),
    'view_manager' => array(
        'view_manager' => array(
//        'template_path_stack' => array(
//            'question' => __DIR__ . '/../view'
//        ),
            'strategies' => array(
                'ViewJsonStrategy',
            ),
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
//            __NAMESPACE__ . '_orm_driver' => array(
//                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
//                'cache' => 'array',
//                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
//            ),
//            'orm_default' => array(
//                'drivers' => array(
//                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_orm_driver'
//                )
//            ),
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'Auth' => 'Auth\Service\Auth',
            'Acl' => 'Auth\Service\Acl'
        ),
    ),
    'roles' => array(
        'guest' => array(),
        'member' => array('guest'),
        'player' => array('member'),
        'moderator' => array('member'),
        'administrator' => array('moderator'),
    ),
    'permissions' => array(
        'guest' => array(
            'allow' => array(
                'auth\auth' => array('login', 'getidentity'),
                'user\user' => array('register', 'activate', 'resetPasswordRequest', 'changePassword'),
                'tournament\tournament' => ['simulate', 'list', 'details', 'test'],
                'statistic\ranking' => '*',
                'payment\payin' => ['tpay'],
                'application\contact' => ['us']
            )
        ),
        'member' => array(
            'allow' => array(
                'auth\auth' => ['logout', 'getidentity'],
                'user\user' => ['get', 'edit', 'getDetails', 'changeBankAccount'],
                'tournament\tournament' => ['list', 'get', 'simulate', 'join', 'details', 'history'],
                'tournament\quiz' => ['start', 'nextQuestion', 'answer', 'details', 'history'],
                'payment\payin' => ['create'],
                'payment\payout' => ['order'],
                'payment\payment' => ['getList'],
                'notification\notification' => 'getList',
                'statistic\my' => '*'
            )
        ),
        'moderator' => array(
            'allow' => array(
                'admin\admin' => '*',
                'question\question' => '*',
                'question\category' => '*',
                'tournament\tournament' => ['simulate', 'create', 'list', 'details', 'history'],
                'payment\payment' => ['getList'],
                'payment\payout' => ['accept']
            )
        )
    ),
);
