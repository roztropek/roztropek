<?php

namespace Question\Exchanger;

use Roztropek\Exchanger\Base;

class FakeAnswer extends Base {

    private $inputFilterConfig = [
        'text' => [
            'required' => true,
            'validators' => [
                ['name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 50
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'Stringtrim']
            ]
        ],
        'question' => [
            'validators' => [],
            'filters' => []
        ]
    ];
    
    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }
}
