<?php

namespace Question\Exchanger;

use Roztropek\Exchanger\Base;

class Category extends Base {

    private $inputFilterConfig = [
        'name' => [
            'required' => true,
            'validators' => [
                ['name' => 'stringlength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 255
                    ]
                ]
            ],
            'filters' => [
                ['name' => 'striptags'],
                ['name' => 'stringtrim']
            ]
        ],
        'active' => [
            'validators' => [],
            'filters' => [
                ['name' => 'boolean']
            ]
        ]
    ];
    
    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
