<?php

namespace Question\Exchanger;

use Roztropek\Exchanger\Base;

class Question extends Base {

    private $inputFilterConfig = [
        'text' => [
            'required' => true,
            'validators' => [
                    ['name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 100
                    ]
                ]
            ],
            'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'Stringtrim']
            ]
        ],
        'correctAnswer' => [
            'required' => true,
            'validators' => [
                    ['name' => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 50
                    ]
                ]
            ],
            'filters' => [
                    ['name' => 'StripTags'],
                    ['name' => 'Stringtrim']
            ]
        ],
        'categories' => [
            'required' => true,
            'validators' => [
                    [
                    'name' => '\Roztropek\Validator\Count',
                    'options' => [
                        'min' => 0
                    ]
                ]
            ]
        ],
        'fakeAnswers' => [
            'required' => true,
            'validators' => [
                    [
                    'name' => '\Roztropek\Validator\Count',
                    'options' => [
                        'equals' => 3
                    ]
                ]
            ]
        ],
        'administrator' => [
            'required' => true,
            'validators' => [
                    [
                    'name' => 'IsInstanceof',
                    'options' => [
                        'className' => '\Admin\Entity\Administrator'
                    ]
                ]
            ]
        ],
        'type' => [
            'required' => false,
            'validators' => [
                    ['name' => 'InArray',
                    'options' => [
                        'haystack' => [1, 2],
                        'strict' => true
                    ]]
            ]
        ],
        'isActive' => [
            'required' => false,
            'continue_if_empty' => true,
            'allow_empty' => true,
            'filters' => [
                    ['name' => 'boolean']
            ],
            'validators' => [
                    ['name' => 'InArray',
                    'options' => [
                        'haystack' => [true, false],
                        'strict' => true
                    ]]
            ]
        ],
    ];

    public function getInputFilterConfig() {
        return array_merge($this->inputFilterConfig, parent::getInputFilterConfig());
    }

}
