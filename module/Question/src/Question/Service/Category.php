<?php

namespace Question\Service;

use \Zend\ServiceManager\FactoryInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;
use Roztropek\Service\CrudEntity;
use Roztropek\Utils\Result\Result;

class Category extends CrudEntity {

    protected $entityClass = '\Question\Entity\Category';

    public function getList(array $params = array()) {
        return $this->createResult(Result::OK, $this->getEntityRepository()->findAll());
    }

}

?>
