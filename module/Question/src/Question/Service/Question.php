<?php

namespace Question\Service;

use \Zend\ServiceManager\FactoryInterface;
use \Zend\ServiceManager\ServiceLocatorInterface;
use \Roztropek\Service\CrudEntity;
use \Roztropek\Utils\Helper\ArrayHelper;
use \Roztropek\Utils\Result\Result;
use \Doctrine\Common\Collections\ArrayCollection;
use Question\Entity\FakeAnswer as FakeAnswerEntity;
use Question\Entity\Question as QuestionEntity;

class Question extends CrudEntity {

    protected $entityClass = '\Question\Entity\Question';

    public function edit($entity, array $data, array $fieldsToValidate = []) {
        return $this->getEntityRepository()->update($entity, $data, $fieldsToValidate);
    }

    public function create(array $data) {
        $identity = $this->getService('Auth')->getIdentity();
        $data['administrator'] = $this->getEntityManager()->getReference('\Admin\Entity\Administrator', $identity['idUser']);
        $categoriesCollection = new ArrayCollection();
        $fakeAnswersCollection = new ArrayCollection();

        if (array_key_exists('categories', $data) && is_array($data['categories'])) {
            foreach ($data['categories'] as $category) {
                $category = $this->getEntityManager()->find('\Question\Entity\Category', $category);
                if (!$category) {
                    throw new \Exception('Nie ma takiej kategorii');
                }
                $categoriesCollection->add($category);
            }
        }

        $data['categories'] = $categoriesCollection;

        if (array_key_exists('fakeAnswers', $data) && is_array($data['fakeAnswers'])) {
            foreach ($data['fakeAnswers'] as $fakeAnswer) {
                $fakeEntity = new FakeAnswerEntity($fakeAnswer);
                $this->getEntityManager()->persist($fakeEntity);
                $fakeAnswersCollection->add(new FakeAnswerEntity($fakeAnswer));
            }
        }

        $data['fakeAnswers'] = $fakeAnswersCollection;
        $questionEntity = new QuestionEntity($data);
        $this->getEntityManager()->persist($questionEntity);
        return $questionEntity;
    }

    public function getList(array $params = array()) {
        $paginator = $this->getEntityRepository()->getList($params);
        return $this->createResult(Result::OK, $paginator);
    }

}

?>
