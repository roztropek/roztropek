<?php

namespace Question\Extractor;

class Category {

    /**
     * @param \Question\Entity\Category $categoryEntity
     * @return array
     */
    public static function extract(\Question\Entity\Category $categoryEntity) {
        return [
            'id' => $categoryEntity->getId(),
            'active' => $categoryEntity->getActive(),
            'name' => $categoryEntity->getName()
        ];
    }

}
