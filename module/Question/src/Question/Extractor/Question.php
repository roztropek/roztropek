<?php

namespace Question\Extractor;

class Question {

    /**
     * @param \Question\Entity\Question $questionEntity
     * @return array
     */
    public static function extract(\Question\Entity\Question $questionEntity) {
        $categories = [];
        foreach ($questionEntity->getCategories() as $category) {
            $categories[] = [
                'id' => $category->getId(),
                'name' => $category->getName()
            ];
        }
        $fakeAnswers = [];
        foreach ($questionEntity->getFakeAnswers() as $answer) {
            $fakeAnswers[] = [
                'id' => $answer->getId(),
                'text' => $answer->getText()];
        }

        return [
            'id' => $questionEntity->getId(),
            'isActive' => $questionEntity->isActive(),
            'text' => $questionEntity->getText(),
            'categories' => $categories,
            'correctAnswer' => $questionEntity->getCorrectAnswer(),
            'administrator' => $questionEntity->getAdministrator()->getLogin(),
            'fakeAnswers' => $fakeAnswers
        ];
    }

}
