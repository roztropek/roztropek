<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Question\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use \Question\Extractor\Category as CategoryExtractor;

class CategoryController extends ApiController {

    public function createAction() {
        $data = $this->getFilteredData(['name', 'active']);
        $entity = $this->getService('category')->create($data);
        $response = new JsonModel(['success' => $success, 'id' => $entity->getId()]);
        return $this->finalize($response);
    }

    public function listAction() {
        $result = $this->getService('category')->getList();
        $categoryEntitiesList = $result->getResult();
        $categoryEntities = [];
        foreach ($categoryEntitiesList as $categoryEntity) {
            $categoryEntities[] = CategoryExtractor::extract($categoryEntity);
        }

        $response = new JsonModel(['success' => true, 'result' => $categoryEntities]);
        return $this->finalize($response);
    }

    public function getAction() {
        $id = $this->params('id', false);
        if (!$id) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['success' => false]);
        }
        $categoryResult = $this->getService('category')->get($id);


        if (!$categoryResult->isValid()) {
            $this->getResponse()->setStatusCode(404);
        }
        $categoryEntity = $categoryResult->getResult();
        return new JsonModel(['success' => true, 'result' => CategoryExtractor::extract($categoryEntity)]);
    }

    public function editAction() {
        $data = $this->getFilteredData(['name']);
        $id = $this->params('id', false);
        if (!$id) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['success' => false]);
        }

        $result = $this->getService('category')->edit($id, $data);
        $this->getResponse()->setStatusCode($result->getSuccess());
        $success = $result->isValid();
        if ($success == true) {
            $response = new JsonModel(['success' => $success]);
            return $this->finalize($response);
        }
        return new JsonModel(['success' => $success, 'messages' => $result->getMessages()]);
    }

    public function deleteAction() {
        $id = $this->params('id', false);
        if (!$id) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['success' => false]);
        }
        $result = $this->getEntityManager()->getService('category')->delete($id);
        $this->getResponse()->setStatusCode($result->getSuccess());
        $response = new JsonModel(['success' => true]);
        return $this->finalize($response);
    }

}
