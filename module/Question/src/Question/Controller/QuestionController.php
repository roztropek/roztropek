<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Question\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;
use Question\Extractor\Question as QuestionExtractor;

class QuestionController extends ApiController {

    public function createAction() {
        $data = $this->getFilteredData(['text', 'categories', 'correctAnswer', 'type', 'fakeAnswers']);
        $questionEntity = $this->getService('question')->create($data);

        $response = new JsonModel(['success' => true, 'id' => $questionEntity->getId()]);
        return $this->finalize($response);
    }

    public function editAction() {
        $data = $this->getFilteredData(['text', 'categories', 'correctAnswer', 'type', 'fakeAnswers','isActive']);
        $id = $this->params('id', false);
        if (!$id) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['success' => false]);
        }
        $result = $this->getService('question')->edit($id, $data);
        $this->getResponse()->setStatusCode($result->getSuccess());
        $success = $result->isValid();
        if ($success == true) {
            $response = new JsonModel(['success' => $success]);
            return $this->finalize($response);
        }
        return new JsonModel(['success' => $success, 'messages' => $result->getMessages()]);
    }

    public function getAction() {
        $id = $this->params('id', false);
        if (!$id) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['success' => false]);
        }
        $questionResult = $this->getService('question')->get($id);
        if (!$questionResult->isValid()) {
            $this->getResponse()->setStatusCode(404);
        }
        $questionEntity = $questionResult->getResult();
        return new JsonModel(['success' => true, 'result' => QuestionExtractor::extract($questionEntity)]);
    }

    public function listAction() {
        $params = $this->params()->fromQuery();
        $result = $this->getService('question')->getList($params);
        $paginator = $result->getResult();
        $questions = [];
        foreach ($paginator as $questionEntity) {
            $questions[] = QuestionExtractor::extract($questionEntity);
        }
        return new JsonModel(['success' => true, 'result' => $questions, 'totalCount' => count($paginator)]);
    }

    public function deleteAction() {
        $id = $this->params('id', false);
        if (!$id) {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(['success' => false]);
        }
        $result = $this->getService('question')->delete($id);
        $this->getResponse()->setStatusCode($result->getSuccess());
        $response = new JsonModel(['success' => true]);
        return $this->finalize($response);
    }

}
