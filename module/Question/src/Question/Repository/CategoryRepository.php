<?php

namespace Question\Repository;

use Roztropek\Utils\Result\Result;
use Roztropek\Db\Repository\ORM\BaseRepository;
use Doctrine\ORM\AbstractQuery;

class CategoryRepository extends BaseRepository {

    /**
     * @return array
     */
    public function getAll() {
        $tableName = $this->getTableName();
        $query = $this->getEntityManager()->createQuery(
                'SELECT ' . $tableName . ' FROM ' . $this->getEntityName() . ' ' . $tableName
        );
        return $query->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

}
