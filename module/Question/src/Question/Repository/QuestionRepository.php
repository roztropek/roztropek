<?php

namespace Question\Repository;

use Roztropek\Db\Repository\ORM\BaseRepository;
use Question\Entity\Question;
use Question\Entity\FakeAnswer;
use \Roztropek\Utils\Result\Result;
use Roztropek\Utils\Helper\ArrayHelper;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;

class QuestionRepository extends BaseRepository {

    /**
     * @param int | \Question\Entity\Question $questionEntity
     * @param array $data
     * @return Result
     */
    public function update($questionEntity, array $data) {
        $className = $this->getClassName();
        if (!$questionEntity instanceof $className) {
            $questionEntity = $this->find($questionEntity);
        }

        $result = new Result();
        if ($questionEntity) {
            $data['categories'] = $this->makeCategoriesCollection($data);

            if (array_key_exists('fakeAnswers', $data) && is_array($data['fakeAnswers'])) {
                if (count($data['fakeAnswers']) > 3) {
                    $data['fakeAnswers'] = array_slice($data['fakeAnswers'], 0, 3);
                }
                foreach ($data['fakeAnswers'] as $fakeAnswerKey => $fakeAnswer) {
                    if (is_array($fakeAnswer) && array_key_exists('id', $fakeAnswer) && array_key_exists('text', $fakeAnswer) && is_scalar($fakeAnswer['text'])) {
                        $answerEntity = $questionEntity->getFakeAnswer($fakeAnswer['id']);
                        if ($answerEntity) {
                            $answerEntity->exchangeData(['text' => $fakeAnswer['text']]);
//                            $answerEntity->setText($fakeAnswer['text']);
                            if (false) {
                                $result->setSuccess(Result::IS_NOT_VALID);
                                $errors = $result->getMessages();
                                $errors['fakeAnswers'][$fakeAnswerKey] = $answerEntity->getMessages();
                                $result->setMessages($errors);
                                continue;
                            }
                        }
                    }
                }
            }
            unset($data['fakeAnswers']);
            $questionEntity->exchangeData($data);
            $result->setResult($questionEntity);
        } else {
            $result->setSuccess(Result::RESOURCE_NOT_FOUND);
        }
        return $result;
    }

    public function getList($params) {
        $limit = ArrayHelper::getKey('limit', $params, 10, 'natural');
        if ($limit > 100) {
            $limit = 100;
        }
        $offset = ArrayHelper::getKey('start', $params, 0, 'natural');
        $page = ArrayHelper::getKey('page', $params, 0, 'natural');
        if ($page > 0) {
            $offset = ($page * $limit) - $limit;
        }

        $orderBy = ArrayHelper::getKey('order', $params, [], 'array');
        $text = ArrayHelper::getKey('text', $params, null, 'scalar');
        $categories = array_filter(explode(',', ArrayHelper::getKey('categories', $params, '', 'string')));
        $isActive = ArrayHelper::getKey('isActive', $params, null, 'scalar');
        $administrators = ArrayHelper::filterByType(explode(',', ArrayHelper::getKey('administrators', $params, '', 'string')), 'natural');

        $qb = $this->createQueryBuilder('q');
        $qb->join('q.fakeAnswers', 'fa');
        $qb->join('q.categories', 'c');
        $qb->join('q.administrator', 'a');

        if ($text) {
            $qb->andWhere($qb->expr()->like('q.text', $qb->expr()->literal('%' . $text . '%')));
        }

        if (!empty($categories)) {
            $qb->andWhere($qb->expr()->in('c.id', $categories));
        }

        if (!empty($administrators)) {
            $qb->andWhere($qb->expr()->in('q.administrator', $administrators));
        }

        if (!empty($isActive)) {
            $qb->andWhere('q.isActive = :isActive');
            $qb->setParameter('isActive', $isActive === 'true' ? true : false);
        }

        $qb->setMaxResults($limit);
        $qb->setFirstResult($offset);

        return $paginator = new Paginator($qb);
    }

    /**
     * @param Question $questionEntity
     * @param array $data
     */
    protected function makeCategoriesCollection(array $data) {
        $categoriesCollection = new \Doctrine\Common\Collections\ArrayCollection();
        if (array_key_exists('categories', $data) && is_array($data['categories'])) {
            foreach ($data['categories'] as $category) {
                $categoriesCollection->add($this->getEntityManager()->getReference('\Question\Entity\Category', $category));
            }
        }
        return $categoriesCollection;
    }

    public function getRandomQuestion(array $notIn = [], $type = 1) {

        $qb = $this->createQueryBuilder('q');
        if (count($notIn) > 0) {
            $qb->andWhere($qb->expr()->notIn('q.id', $notIn));
            $qb->andWhere($qb->expr()->orX('q.isActive = TRUE', 'q.isActive IS NULL'));
        }
        $qb->andWhere('q.type = :type');
        $qb->setParameter('type', $type);
        
        $qb->select('COUNT(q) as counter');
        $count = $qb->getQuery()->getScalarResult()[0]['counter'];
        if ($count === 0) {
            throw new \Exception('Brak pytań!');
        }
        $qb->select('q');
        $qb->setMaxResults(1);
        $firstResult = rand(0, $count-1);
        $qb->setFirstResult($firstResult);
        $result = $qb->getQuery()->getSingleResult();

        return $result;
    }

}
