<?php

namespace Question\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;

/**
 * @ORM\Entity
 * @ORM\Table(name="FakeAnswer")
 * */
class FakeAnswer extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="string") */
    protected $text;

    /**
     * @ORM\ManyToOne(targetEntity="Question\Entity\Question", inversedBy="fakeAnswers")
     * @ORM\JoinColumn(name="idQuestion", referencedColumnName="id")
     * 
     */
    protected $question;
    private $settersMapping = [
        'question' => 'setQuestion',
        'text' => 'setText'
    ];
    
    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function getId() {
        return $this->id;
    }

    public function getText() {
        return $this->text;
    }

    public function getQuestion() {
        return $this->question;
    }

    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @param \Question\Entity\Question $question
     */
    public function setQuestion(\Question\Entity\Question $question) {
        $this->question = $question;
        $question->addFakeAnswer($this);
    }

}
