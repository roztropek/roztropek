<?php

namespace Question\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Roztropek\Db\Entity\Base;

/**
 * @ORM\Entity(repositoryClass="Question\Repository\CategoryRepository")
 * @ORM\Table(name="Category")
 * */
class Category extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="string") * */
    protected $name;

    /** @ORM\Column(type="boolean") * */
    protected $active = true;

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function setName($name) {
        $this->name = $name;
    }

}
