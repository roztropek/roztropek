<?php

namespace Question\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;
use Doctrine\Common\Collections\ArrayCollection;
use Admin\Entity\Administrator;

/**
 * @ORM\Entity(repositoryClass="Question\Repository\QuestionRepository")
 * @ORM\Table(name="Question")
 * @ORM\HasLifecycleCallbacks
 * */
class Question extends Base {

    use \Roztropek\Db\Entity\Traits\Creatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="string") */
    protected $text;

    /**
     * @ORM\ManyToOne(targetEntity="Admin\Entity\Administrator")
     * @ORM\JoinColumn(name="idAdministrator", referencedColumnName="id")
     */
    protected $administrator;

    /**
     * @ORM\ManyToMany(targetEntity="Question\Entity\Category")
     * @ORM\JoinTable(name="QuestionCategory",
     * joinColumns={@ORM\JoinColumn(name="idQuestion", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="idCategory", referencedColumnName="id")}
     * )
     */
    protected $categories;

    /**
     * @ORM\OneToMany(targetEntity="\Question\Entity\FakeAnswer", mappedBy="question", cascade={"persist", "remove"})
     * */
    protected $fakeAnswers;

    /** @ORM\Column(type="string") */
    protected $correctAnswer;

    /** @ORM\Column(type="integer") */
    protected $type = 1;

    /** @ORM\Column(type="boolean") */
    protected $isActive = true;
    private $settersMapping = [
        'administrator' => 'setAdministrator',
        'categories' => 'setCategories',
        'fakeAnswers' => 'setFakeAnswers',
        'text' => 'setText',
        'correctAnswer' => 'setCorrectAnswer',
        'type' => 'setType',
        'isActive' => 'setIsActive'
    ];

    protected function getSettersMapping() {
        return array_merge($this->settersMapping, parent::getSettersMapping());
    }

    public function isActive() {
        
        return $this->isActive;
    }

    public function getId() {
        return $this->id;
    }

    public function getText() {
        return $this->text;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function getIdAdministrator() {
        return $this->idAdministrator;
    }

    public function getAdministrator() {
        return $this->administrator;
    }

    public function activate() {
        $this->setIsActive(true);
    }

    public function deactivate() {
        $this->setIsActive(false);
    }

    /**
     * @return ArrayCollection
     */
    public function getCategories() {
        if (!$this->categories) {
            $this->setCategories(new ArrayCollection());
        }
        return $this->categories;
    }

    /**
     * @return ArrayCollection
     */
    public function getFakeAnswers() {
        if (!$this->fakeAnswers) {
            $this->setFakeAnswers(new ArrayCollection());
        }
        return $this->fakeAnswers;
    }

    /**
     * @param ArrayCollection $answers
     */
    protected function setFakeAnswers(ArrayCollection $answers) {
        $this->fakeAnswers = $answers;
        foreach ($answers as $answerEntity) {
            $this->addFakeAnswer($answerEntity);
        }
    }

    /**
     * @param string $correctAnswer
     */
    protected function setCorrectAnswer($correctAnswer) {
        $this->correctAnswer = $correctAnswer;
    }

    /**
     * @param Administrator $administrator
     */
    protected function setAdministrator(Administrator $administrator) {
        $this->administrator = $administrator;
    }

    /**
     * @param \Question\Entity\Answer $answerEntity
     * @return \Question\Entity\Question
     */
    public function addFakeAnswer(FakeAnswer $answerEntity) {
        if ($this->getFakeAnswers()->count() < 4) {
            if (!$this->getFakeAnswers()->contains($answerEntity)) {
                $this->getFakeAnswers()->add($answerEntity);
            }
            if ($answerEntity->getQuestion() !== $this) {
                $answerEntity->setQuestion($this);
            }
        }
    }

    public function getCategory($id) {
        foreach ($this->getCategories() as $category) {
            if ($category->getId() == $id) {
                return $category;
            }
        }
        return null;
    }

    public function getType() {
        return $this->type;
    }

    public function addCategory(Category $category) {
        $existedCategory = $this->getCategory($category->getId());
        if (!$existedCategory) {
            $this->addCategory($category);
        }
    }

    public function setCategories(ArrayCollection $categories) {
        $this->categories = $categories;
        foreach ($categories as $categoryEntity) {
            $this->addCategory($categoryEntity);
        }
    }

    protected function setText($text) {
        $this->text = $text;
    }

    protected function setIsActive($isActive) {
        $this->isActive = $isActive;
    }

    public function getCorrectAnswer() {
        return $this->correctAnswer;
    }

    public function getFakeAnswer($id) {
        foreach ($this->getFakeAnswers() as $answerEntity) {
            if ($answerEntity->getId() == $id) {
                return $answerEntity;
            }
        }
        return null;
    }

    protected function setType($type) {
        $this->type = $type;
    }

}
