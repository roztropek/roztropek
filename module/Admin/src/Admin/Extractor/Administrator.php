<?php

namespace Admin\Extractor;

use Admin\Entity\Administrator as AdministratorEntity;

class Administrator {

    /**
     * @param \Admin\Entity\Administrator $administratorEntity
     * @return array
     */
    public static function extract(AdministratorEntity $administratorEntity) {
        return [
            'id' => $administratorEntity->getId(),
            'nick' => $administratorEntity->getLogin()
        ];
    }

}
