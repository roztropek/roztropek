<?php

namespace Admin\Entity;

use Doctrine\ORM\Mapping as ORM;
use Roztropek\Db\Entity\Base;

/**
 * @ORM\Entity 
 * @ORM\Table(name="Administrator")
 * */
class Administrator extends Base {

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /** @ORM\Column(type="string") * */
    protected $login;

    /** @ORM\Column(type="string") * */
    protected $password;

    /** @ORM\Column(type="integer") * */
    protected $type;

    public function getId() {
        return $this->id;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getType() {
        return $this->type;
    }

    public function getLogin() {
        return $this->login;
    }

    public function setLogin($login) {
        $this->login = $login;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function setType($type) {
        $this->type = $type;
    }

}
