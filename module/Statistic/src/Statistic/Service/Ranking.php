<?php

namespace Statistic\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Helper\ArrayHelper;
use User\Extractor\User as UserExtractor;

class Ranking extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    public function getWinners(array $params) {
        $type = ArrayHelper::getKey('type', $params, 'monthly', 'string');

        if (!in_array($type, ['monthly', 'weekly', 'daily', 'lastWeek', 'last30days'])) {
            throw new \InvalidArgumentException('Nieprawidłowy typ rankingu!');
        }

        $rankingResult = $this->getRepository('Payment\Entity\Transaction')->getWinnersRanking($type);
        $players = $this->getRepository('\User\Entity\User')->findBy(['id' => array_column($rankingResult, 'userId')]);

        $result = [];
        foreach ($rankingResult as $resultItem) {
            $player = array_values(array_filter($players, function($player) use ($resultItem) {
                                return $player->getId() === $resultItem['userId'];
                            }))[0];

            if ($player) {
                $result[] = [
                    'value' => $resultItem['value'],
                    'player' => UserExtractor::extract($player)
                ];
            }
        }

        return $result;
    }

}
