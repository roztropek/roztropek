<?php

namespace Statistic\Service;

use Roztropek\Service\Base;
use Roztropek\Utils\Result\Result;
use Roztropek\Utils\Helper\ArrayHelper;

class PlayerStatistics extends Base {

    use \Roztropek\Service\Traits\Doctrinable;

    public function getGames(array $params) {
        $identity = $this->getService('Auth')->getIdentity();
        $values = $this->parseParams($params);

        $type = $values['type'];
        $dateStart = $values['dateStart'];
        $dateStop = $values['dateStop'];
        $format = $values['format'];

        $dateFormat = sprintf("date_format(q.endDate, '%s')", $format);

        $dql = sprintf('SELECT 
            %s as statKey,
                COUNT(q.id) as quizCount,
                SUM(q.points) as sumPoints,
                MAX(q.points) as maxPoints,
                ROUND(AVG(q.points),2) as averangePoints,
                ROUND(AVG(q.endDate - q.startDate), 2) as averangeQuizTime,
                MIN(q.endDate - q.startDate) as shortestQuiz,
                MAX(q.endDate - q.startDate) as longestQuiz
            FROM \Tournament\Entity\Quiz q
            WHERE q.player = :user 
            AND q.endDate IS NOT NULL
            AND q.endDate > :dateStart 
            AND q.endDate <= :dateStop
            GROUP BY statKey', $dateFormat, $dateFormat);
        $query = $this->getEntityManager()->createQuery($dql)
                ->setParameters([
            'user' => $identity['idUser'],
            'dateStart' => $dateStart,
            'dateStop' => $dateStop
        ]);
        $queryResults = $query->getScalarResult();
        $result = array_fill_keys($values['keys'], [
            'quizCount' => 0,
            'sumPoints' => 0,
            'maxPoints' => 0,
            'averangePoints' => 0,
            'averangeQuizTime' => 0,
            'shortestQuiz' => 0,
            'longestQuiz' => 0
        ]);
        foreach ($result as $resultKey => $resultItem) {
            $result[$resultKey]['statKey'] = $resultKey;
        }

        foreach ($queryResults as $queryResult) {
            $result[$queryResult['statKey']] = $queryResult;
        }

        return array_values($result);
    }

    public function getWinnings(array $params) {
        $identity = $this->getService('Auth')->getIdentity();
        $values = $this->parseParams($params);

        $type = $values['type'];
        $dateStart = $values['dateStart'];
        $dateStop = $values['dateStop'];
        $format = $values['format'];

        $dateFormat = sprintf("date_format(t.createdAt, '%s')", $format);

        $dql = sprintf("SELECT 
            %s as statKey,
            SUM(t.value) as sumWinnings,
            ROUND(AVG(t.value), 2) as averangeWinnings,
            COUNT(t.id) AS countWinnigns,
            MAX(t.value) as maxPrize
            FROM \Payment\Entity\Transaction t
            WHERE 
            t.user = :user AND
            t.type = 'credit' AND
            t.currency = 'playCoin' AND
            t.description = 'winInTournament' AND
            t.createdAt > :dateStart AND
            t.createdAt <= :dateStop
            GROUP BY statKey
            ORDER BY statKey
            ", $dateFormat);

        $query = $this->getEntityManager()->createQuery($dql)
                ->setParameters([
            'user' => $identity['idUser'],
            'dateStart' => $dateStart,
            'dateStop' => $dateStop
        ]);

        $queryResults = $query->getScalarResult();

        $result = array_fill_keys($values['keys'], [
            'sumWinnings' => 0,
            'averangeWinnings' => 0,
            'countWinnigns' => 0
        ]);
        foreach ($result as $resultKey => $resultItem) {
            $result[$resultKey]['statKey'] = $resultKey;
        }

        foreach ($queryResults as $queryResult) {
            $result[$queryResult['statKey']] = $queryResult;
        }

        return array_values($result);
    }

    public function getTotalGames() {
        $identity = $this->getService('Auth')->getIdentity();

        $dql = 'SELECT 
                COUNT(q.id) as quizCount,
                SUM(q.points) as sumPoints,
                MAX(q.points) as maxPoints,
                MIN(q.endDate - q.startDate) as shortestQuiz,
                MAX(q.endDate - q.startDate) as longestQuiz
            FROM \Tournament\Entity\Quiz q
            WHERE q.player = :user 
            AND q.endDate IS NOT NULL';

        $query = $this->getEntityManager()->createQuery($dql)
                ->setParameters([
            'user' => $identity['idUser']
        ]);
        return $query->getScalarResult();
    }

    public function getTotalWinnings() {
        $identity = $this->getService('Auth')->getIdentity();
        $dql = "SELECT 
            SUM(t.value) as totalPrize,
            COUNT(t.id) AS countWinnigns,
            MAX(t.value) as maxPrize
            FROM \Payment\Entity\Transaction t
            WHERE 
            t.user = :user AND
            t.type = 'credit' AND
            t.currency = 'playCoin' AND
            t.description = 'winInTournament'
            ";

        $query = $this->getEntityManager()->createQuery($dql)
                ->setParameters([
            'user' => $identity['idUser']
        ]);
        return $query->getScalarResult();
    }

    protected function parseParams(array $params) {
        $type = ArrayHelper::getKey('type', $params, 'daily', 'string');
        $dateStart = ArrayHelper::getKey('dateStart', $params, date('Y-m-d', strtotime('first day of this month')), 'string');
        $dateStart = date('Y-m-d', strtotime($dateStart));
        $dateStop = ArrayHelper::getKey('dateStop', $params, date('Y-m-d', strtotime('last day of this month')), 'string');
        $format = '';
        $keys = [];
        switch ($type) {
            case 'monthly':
                $format = '%m-%y';
                $currentTime = strtotime($dateStart);
                $dateStop = strtotime($dateStop) - $currentTime > 184 * 24 * 3600 ? date('Y-m-d', strtotime('+6 months', $currentTime)) : $dateStop;
                while (strtotime($dateStop) >= $currentTime) {
                    $keys[] = date('m-y', $currentTime);
                    $currentTime = strtotime('+1 month', $currentTime);
                }
                break;
            case 'weekly':
                $format = '%u-%y';
                $currentTime = strtotime($dateStart);
                $dateStop = strtotime($dateStop) - $currentTime > 8 * 7 * 24 * 3600 ? date('Y-m-d', strtotime('+8 weeks', $currentTime)) : $dateStop;
                while (strtotime($dateStop) >= $currentTime) {
                    $keys[] = date('W-y', $currentTime);
                    $currentTime = strtotime('+1 week', $currentTime);
                }
                break;
            default:
            case 'daily':
                $currentTime = strtotime($dateStart);
                $dateStop = strtotime($dateStop) - $currentTime > 31 * 24 * 3600 ? date('Y-m-d', strtotime('+31 days', $currentTime)) : $dateStop;
                $format = '%d-%m-%y';
                while (strtotime($dateStop) >= $currentTime) {
                    $keys[] = date('d-m-y', $currentTime);
                    $currentTime = strtotime('+1 day', $currentTime);
                }
                break;
        }
        $dateStop = date('Y-m-d H:i:s', strtotime('today 23:59:59', strtotime($dateStop)));
        return [
            'type' => $type,
            'dateStart' => $dateStart,
            'dateStop' => $dateStop,
            'format' => $format,
            'keys' => $keys
        ];
    }

}
