<?php

namespace Statistic\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;

class MyController extends ApiController {

    public function gamesAction() {
        $result = $this->getService('PlayerStatistics')->getGames($this->params()->fromQuery());
        return new JsonModel(['success' => true, 'result' => $result]);
    }

    public function winningsAction() {
        $result = $this->getService('PlayerStatistics')->getWinnings($this->params()->fromQuery());
        return new JsonModel(['success' => true, 'result' => $result]);
    }

    public function totalsAction() {
        $params = $this->params()->fromQuery();
        $stats = $params['stats'];
        $explodedStats = explode(',', $stats);
        $correctStats = array_filter($explodedStats, function($stat) {
           return in_array($stat, ['games', 'winnings']);
        });
        $result = [];
        
        foreach($correctStats as $statName) {
            $methodName = sprintf('getTotal%s', ucfirst(strtolower($statName)));
            $result[$statName] = $this->getService('PlayerStatistics')->$methodName();
        }
        return new JsonModel(['success' => true, 'result' => $result]);
    }

}
