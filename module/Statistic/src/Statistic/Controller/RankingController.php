<?php

namespace Statistic\Controller;

use Roztropek\Controller\ApiController;
use Zend\View\Model\JsonModel;

class RankingController extends ApiController {

    public function winnersAction() {
        $result = $this->getService('Ranking')->getWinners($this->params()->fromQuery());
        return new JsonModel(['success' => true, 'result' => $result]);
    }

}
